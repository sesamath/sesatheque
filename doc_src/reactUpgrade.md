Upgrade React
=============

React et sa collection de dépendances / compagnons n'est pas simple à mettre à jour…

On part de
```json
{
  "dependencies": {
    "connected-react-router": "5.0.1",
    "react": "16.6.3",
    "react-ace": "^9.5.0",
    "react-dom": "16.6.3",
    "react-hot-loader": "4.3.12",
    "react-paginate": "5.3.1",
    "react-redux": "5.1.1",
    "react-router": "4.3.1",
    "react-router-dom": "4.3.1",
    "react-select": "2.1.2",
    "recompose": "0.30.0",
    "redux": "4.0.1",
    "redux-form": "7.4.2",
    "redux-thunk": "2.3.0"
  }
}
```

Et on essaie de passer à la dernière version de react16 (16.14.0), mais ça casse les tests (par test/react/src/Description.test.js qui plante parce que <NavLink> doit être dans un <Router>, il est bien dans un <MemoryRouter> qui étend <Router> mais ça plante quand même).

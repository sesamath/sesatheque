Notes pour les développeurs
===========================

Dans toute la doc, on utilise pnpm (`npm -g i pnpm` pour l'installer globalement), mais vous pouvez utiliser npm ou yarn si vous préférez.

La motivation principale est la rapidité et le gain d'espace disque :
- https://pnpm.js.org/docs/en/motivation.html
- https://www.kochan.io/nodejs/why-should-we-use-pnpm.html

Installation
------------

Cf ../INSTALL.md

Développement
-------------

Back :
- `pnpm start:devBack` va lancer le serveur node avec reload à chaque modif (sans rebuild du code client)
- Pour éviter un reload à chaque modif, mais le déclencher manuellement, ça peut être pratique de lancer un `while true; do pnpm run start; done` et relancer avec un ctrl+c 

Front:
- lancer webpack-dev-server sur le port 3001 avec `pnpm run start:devFront`   
  (pour tous les js compilés par webpack) avec proxy vers le back pour le reste)
- lancer le back avec `pnpm run start:dev` pour qu'il se lance sur le bon port

Pour lancer deux Sésathèques (pour un usage avec un sesalab à coté), vous pouvez utiliser `pnpm run start:both` (qui va lancer les deux avec _private/config.js et _private/commun.js)

Développement d'un plugin
-------------------------

Ce qui suit est aussi valable pour d'autres dépendances (sesajstools & co).

Pour pouvoir développer une dépendance de sésathèque et avoir le résultat en live dans la sesathèque locale, il faut utiliser `pnpm link` (Attention, ça marche pas avec `npm link`, un très vieux bug de npm qui remonte à npm3)

Par ex, pour lier le plugin arbre à la version locale que l'on a 

- On récupère localement le dépôt du plugin arbre 

```bash
# depuis le dossier local sesatheque
cd ..
# (l'url est dans package.json, dans les peerDependencies, 
# mais ici on clone via ssh et pas http pour pouvoir faire des push ultérieurement)
git clone gitlab@git.sesamath.net:sesamath/sesatheque-plugin-arbre.git

# 1) On donne le dossier à utiliser pour ce module
cd sesatheque/app/plugins
pnpm link ../../../sesatheque-plugin-arbre
cd ../..

2) Autre solution, passer par une déclaration globale (vers notre dépôt local des modules globaux) 
# on déclare ce plugin comme link global
cd sesatheque-plugin-arbre
pnpm link --global

# On retourne dans la sesatheque pour l'utiliser
cd ../sesatheque/app/plugins
pnpm link --global @sesatheque-plugins/arbre
```

Pour revenir à une situation "normale" avec des dépendances installées par pnpm (et plus les links vers nos dossiers locaux)
```
# depuis le dossier de la sésathèque
cd app/plugins
pnpm unlink @sesatheque-plugins/arbre

# depuis le dossier sesatheque-plugin-arbre
pnpm unlink
```

TODO : upgrade react
--------------------
v16 => v17
Cf https://legacy.reactjs.org/blog/2020/10/20/react-v17.html et https://legacy.reactjs.org/blog/2020/08/10/react-v17-rc.html#other-breaking-changes

v17 => v18
Cf https://react.dev/blog/2022/03/08/react-18-upgrade-guide

Docs react
----------

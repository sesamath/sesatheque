/* global db print */

// corrige les fautes typographiques fréquentes (espace après parenthèse ouvrante, etc.)
const rectifs = [
  // triple point d'abord, pour ne pas virer un éventuel espace avant
  [/\.\.\./g, '…'],
  // toujours un tableau avec la regex en 1er et le remplacement en 2e
  [/([([«]) /g, '$1'],
  [/ ([)\],.])/g, '$1'],
  [/([^ ])([;:?!»])/g, '$1 $2'],
  [/([;:?!«])([^ ])/g, '$1 $2'],
  // les <br> en \n
  [/<br( *\/)?>/ig, '\n']
]
const res = rectifs.map(([regex]) => regex)
const needChange = (text) => res.some(re => re.test(text))
const clean = (text) => rectifs.reduce((str, [re, repl]) => str.replace(re, repl), text)

const cursor = db.EntityRessource.find()
while (cursor.hasNext()) {
  const doc = cursor.next()
  try {
    const { _data: { titre, resume, commentaires, description, rid } } = doc
    if ([titre, resume, commentaires, description].some(needChange)) {
      print(`\n* modification de ${rid}`)
      for (const prop of ['titre', 'resume', 'commentaires', 'description']) {
        const src = doc._data[prop]
        const dst = clean(src)
        if (dst !== src) {
          print(`  - pour ${prop}\n    ${src}\n    =>\n    ${dst}`)
          doc._data[prop] = dst
        }
      }
      db.EntityGroupe.save(doc)
    }
  } catch (error) {
    console.error(error, `Sur la ressource ${doc._id}`)
  }
}

// script à utiliser pour lister toutes les ressources de l'arbre IEP, avec leur niveaux

const idMasterIep = '5bcc89dc129e5b1076e56616'
// from sesatheque-client:src/config.js
const niveaux = {
  mtps: 'très petite section',
  mps: 'petite section',
  mms: 'moyenne section',
  mgs: 'grande section',
  11: 'CP',
  10: 'CE1',
  9: 'CE2',
  8: 'CM1',
  7: 'CM2',
  6: 'sixième',
  5: 'cinquième',
  4: 'quatrième',
  3: 'troisième',
  2: 'seconde',
  '2p': 'seconde pro',
  1: 'première',
  '1p': 'première pro',
  12: 'terminale',
  '12p': 'terminale pro',
  cap1: 'CAP 1re année',
  cap2: 'CAP 2e année',
  bep1: 'BEP 1re année',
  bep2: 'BEP 2e année'
}

/* global db print */
const logErr = (message) => print(`Erreur : ${message}`)

function parse (enfants, indent = 0) {
  enfants.forEach(({ aliasOf, titre, type, enfants }) => {
    if (aliasOf) {
      const ress = load(aliasOf)
      if (!ress) return logErr(`pas trouvé ${aliasOf} (titre : ${titre})`)
      display(ress, indent)
      enfants = ress.enfants
    } else if (type === 'arbre') {
      display({ titre, type }, indent)
    }
    if (type === 'arbre') parse(enfants, indent + 1)
  })
}

function display (ress, indent = 0) {
  if (['iep', 'arbre'].includes(ress.type)) {
    const isIep = ress.type === 'iep'
    const prefix = ' '.repeat(indent * 2) + (isIep ? '* ' : '# ')
    const suffix = isIep ? ` (${ress.niveaux.map(n => niveaux[n]).join(', ')})` : ''
    print(prefix + ress.titre + suffix)
  } else {
    print('Error : une ressource ni arbre ni iep')
  }
}

function load (rid) {
  // on cherche par rid ou id
  const cursor = /\//.test(rid) ? db.EntityRessource.find({ rid }) : db.EntityRessource.find({ _id: rid })
  const r = cursor.next()
  if (!r) return print(`Erreur : ${rid} n’existe pas`)
  if (!r._data) return print(`Erreur : pas de _data pour ${rid}`)
  if (!r.publie || r.restriction) return print(`Erreur : ${rid} n’est pas public`)
  return r._data
}

const master = load(idMasterIep)
print(`Arbre iep chargé, id ${idMasterIep}, titre : ${master.titre}`)
parse(master.enfants)

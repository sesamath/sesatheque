// script à utiliser pour retrouver les arbres contenant des graphes avec exo_mep dedans
// à lancer avec par ex
// ./scripts/mongoApp -f scripts-mongo/findJ3pSectionsExoMep.js | tee J3pSectionsExoMep.list

/* global db print printjson */

const exosMep = new Set()
const arbres = new Set()

function parse (enfants, ridParent) {
  arbres.add(ridParent)
  enfants.forEach(({ aliasOf, type, enfants }) => {
    if (type === 'arbre') {
      if (aliasOf) return load(aliasOf)
      if (enfants && enfants.length) return parse(enfants, ridParent)
    } else if (type === 'j3p' && exosMep.has(aliasOf)) {
      print(`L’arbre ${ridParent} contient la ressource j3p exo_mep ${aliasOf}`)
    }
  })
}

function load (rid) {
  // on cherche par rid ou id
  const cursor = /\//.test(rid) ? db.EntityRessource.find({ rid }) : db.EntityRessource.find({ _id: rid })
  const r = cursor.next()
  if (!r) return print(`Erreur : ${rid} n’existe pas`)
  if (!r._data) return printjson(r)
  if (r._data.type !== 'arbre') return print(`Erreur : ${rid} n’est pas un arbre`)
  if (!r.publie || r.restriction) return print(`Erreur : ${rid} n’est pas public`)
  if (!Array.isArray(r._data.enfants) || !r._data.enfants.length) return print(`Erreur : ${rid} n’a pas d'enfants`)
  return parse(r._data.enfants, rid)
}

db.EntityRessource.find({ '_data.type': 'j3p' }).sort({ dateCreation: 1 }).forEach(r => {
  const { g } = r._data.parametres
  if (g && g.length) {
    g.forEach((node) => {
      if (Array.isArray(node) && /exo_mep$/.test(node[1])) exosMep.add(r._data.oid)
    })
  }
})

print('Liste des ressources avec exo_mep (ou ancienexo_mep) :\n', Array.from(exosMep).join('\n'))
print('Parsing des arbres Sésamath')
load('50035')
print(`On a parsé l’arbre 50035 et ses ${arbres.size} enfants :\n`, Array.from(arbres).join(' '))

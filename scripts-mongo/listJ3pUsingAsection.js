// script à utiliser pour retrouver les sections utilisées dans les graphes de la bibliotheque
// ./scripts/mongoApp -f scripts-mongo/listJ3pSections.js -q > j3p.list

/* global db print section */
const sec = (typeof section === 'number')
  ? String(section)
  : section
if (typeof sec !== 'string') {
  throw Error('Vous devez appeler ce script en lui passant une section, avec mongoApp -e \'const section="laSection"\' -f …')
}

const cursor = db.EntityRessource.find({ '_data.type': 'j3p' }).sort({ dateCreation: 1 })

while (cursor.hasNext()) {
  const doc = cursor.next()
  try {
    const { _id, _data: { parametres: { g } } } = doc
    if (g.some(nodeParams => Array.isArray(nodeParams) && nodeParams[1] === sec)) {
      // séparateur
      print(_id)
    }
  } catch (error) {
    // rien
  }
}

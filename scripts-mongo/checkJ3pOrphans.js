// script qui parse un arbre pour lister tous les oids qu'il contient
// sort la liste des arbres contenant chaque oid

// ./scripts/mongoApp -f scripts-mongo/listOidInArbres.js -q > arbre.list

/* global db print printjson */
const parent = '50035'
const rids = {}
const auteursJ3p = [
  'sesasso/17', // Alexis
  'sesasso/74', // Rémi
  'sesasso/14813', // Daniel
  'sesasso/42052', // Yves
  'sesasso/112551' // Tommy
]

function add (rid, parent) {
  if (!rids[rid]) rids[rid] = []
  rids[rid].push(parent)
}
// ./scripts/mongoApp -f scripts-mongo/listJ3pSections.js -q > j3p.list

function parse (enfants, ridParent) {
  enfants.forEach(({ aliasOf, type, enfants }) => {
    if (type === 'arbre') {
      if (aliasOf) return load(aliasOf)
      if (enfants && enfants.length) return parse(enfants, ridParent)
    } else if (type === 'j3p') {
      add(aliasOf, ridParent)
    }
  })
}

function load (rid) {
  const cursor = /\//.test(rid) ? db.EntityRessource.find({ rid }) : db.EntityRessource.find({ _id: rid })
  const r = cursor.next()
  if (!r) return print(`Erreur : ${rid} n’existe pas`)
  if (!r._data) return printjson(r)
  if (r._data.type !== 'arbre') return print(`Erreur : ${rid} n’est pas un arbre`)
  if (!r.publie || r.restriction) return print(`Erreur : ${rid} n’est pas public`)
  if (!Array.isArray(r._data.enfants) || !r._data.enfants.length) return print(`Erreur : ${rid} n’a pas d'enfants`)
  return parse(r._data.enfants, rid)
}

function checkAll () {
  const format = (str) => str.replace(/\n/g, '\n        ')
  // cf https://docs.mongodb.com/manual/reference/method/db.collection.find/#db.collection.find
  // https://docs.mongodb.com/manual/reference/method/db.collection.find/#find-projection
  let nbOk = 0; let nbKo = 0
  const orphans = []
  // la ligne suivante ne remonte que les ressources avec un des auteurs étant dans notre liste
  // const cursor = db.EntityRessource.find({'_data.type': 'j3p', publie: true, restriction: 0, '_data.auteurs': {$in: auteursJ3p}}).sort({dateCreation: 1})
  // mais on les prend toutes et on filtre ensuite, car on veut soit auteurs vide (un import), soit tous les auteurs dans les auteurs "officiels" j3p
  const cursor = db.EntityRessource.find({ '_data.type': 'j3p', publie: true, restriction: 0 }).sort({ dateCreation: 1 })
  while (cursor.hasNext()) {
    const r = cursor.next()
    const { rid, _data: { oid, auteurs, titre, resume, description, commentaires } } = r
    // on vire les clones, où un des auteurs n'est pas dans notre liste
    if (auteurs.length && !auteurs.every(a => auteursJ3p.includes(a))) continue
    if (rids[rid]) {
      nbOk++
      // printjson({ [rid]: rids[rid] })
    } else {
      nbKo++
      orphans.push(rid)
      // print(`Erreur: la ressource j3p ${rid} n’est référencée dans aucun des arbres Sésamath (${r._data.titre})`)
      let msg = `* https://bibliotheque.sesamath.net/ressource/apercevoir/${oid} ${titre}`
      if (resume) msg += `\n    resume : ${format(resume)}`
      if (description) msg += `\n    description : ${format(description)}`
      if (commentaires) msg += `\n    commentaires : ${format(commentaires)}`
      print(msg)
    }
  }
  print(`On a vérifié ${nbOk + nbKo} ressources j3p "officielles", dont ${nbKo} orphelines :`)
  // printjson(orphans)
}

load(parent)
print(`On a trouvé ${Object.keys(rids).length} ressources j3p différentes dans les arbres Sésamath.`)
checkAll()

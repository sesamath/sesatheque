Quelques exemples mongosh
=========================

Renommer une baseId (après récup d'une base de prod par ex)
-----------------------------------------------------------

exemple pour remplacer sesacommun par communlocal3002
```js
let nb = 0
let cursor = db.EntityRessource.find({rid: /sesacommun\//})
while (cursor.hasNext()) { const r = cursor.next(); if (r._data.origine === 'sesacommun') r._data.origine = 'communlocal3002'; const rid = r.rid.replace('sesacommun', 'communlocal3002'); r.rid = rid; r._data.rid = rid; db.EntityRessource.replaceOne({_id: r._id}, r); nb++; console.log(`ressource ${r._id} modifiée`) }
console.log(`${nb} ressources modifiées sur ${db.EntityRessource.count()}`)
```

On devrait pouvoir faire plus performant avec un pipeline, cf https://www.mongodb.com/docs/manual/tutorial/update-documents-with-aggregation-pipeline/#updatemany-with--set

à tester (ATTENTION, ne surtout pas lancer ce genre d'update en prod, une petite faute de frappe pouvant avoir des effets catastrophiques)
```js
db.EntityRessource.updateMany({rid: /sesacommun\//}, { rid: {'$concat': ['communlocal3002/', '$_id']}, '_data.rid': {'$concat': ['communlocal3002/', '$_id']}, '_data.origine': {$cond: {if: {$eq: ['$_data.origine', 'sesacommun']}, then: 'communlocal3002', else: '$_data.origine' }} })
```

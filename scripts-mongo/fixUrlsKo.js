// script à utiliser pour corriger les anciennes urls avec du ressources.sesamath.net/coll_docs/…/voir….php?…

// ./scripts/mongoApp -f scripts-mongo/fixCollDocs.js

/* global db print */
let nbPb = 0
let nbFixed = 0
let nbKo = 0
db.EntityRessource
  .find({ '_data.type': 'url' })
  .forEach((ressource) => {
    const { adresse } = ressource._data.parametres
    const _id = ressource._id
    let url
    let needMaj = false
    let motifMaj

    // #0 url + adresse
    if (adresse && ressource._data.parametres.url) {
      if (adresse !== ressource._data.parametres.url) {
        // on sauvegarde la valeur d'url dans les commentaires
        if (ressource._data.commentaires) ressource._data.commentaires += '\n'
        // else ressource._data.commentaires = '' // au cas où la prop n'existerait pas
        ressource._data.commentaires += `param url était ${ressource._data.parametres.url}`
      } else {
        motifMaj = 'adresse + url (identiques), url virée'
      }
      delete ressource._data.parametres.url
      needMaj = true
    }

    // #1 pas d'adresse
    if (!adresse) {
      // on regarde si on a une prop url plutôt que adresse (visiblement ça arrive)
      if (ressource._data.parametres.url) {
        url = ressource._data.parametres.url
        delete ressource._data.parametres.url
      } else {
        // on tente de la retrouver dans le titre
        const chunks = /.*(https?:\/\/[^ )\]]+)/.exec(ressource._data.titre)
        if (chunks) {
          url = chunks[1]
        } else {
          nbPb++
          nbKo++
          print(`ERROR (${_id}) sans adresse ni url trouvée ailleurs`)
          return // on arrête là
        }
      }
    }

    if (adresse.includes('ressources.sesamath.net')) {
      // #2a ressources avec un voir_xxx.php dans coll_docs
      if (/coll_docs.*voir.*php/.test(adresse)) {
        if (adresse.includes('swf')) {
          url = adresse.replace(/https:\/\/ressources\.sesamath\.net\/coll_docs\/(.+)\/voir_swf\.php\?fichier=(.+)/, 'https://manuel.sesamath.net/lecteur/voir_flash.php?dossier=$1&fichier=$2')
        } else if (adresse.includes('iep')) {
          url = adresse.replace(/https:\/\/ressources.sesamath.net\/coll_docs\/(.+)\/voir_iepv2\.php\?script=(.+)/, 'https://manuel.sesamath.net/lecteur/voir_iep.php?dossier=$1&script=$2')
        }

      // #2b ressources avec le lecteur iep (=> manuel)
      } else if (adresse.includes('/coll/lecteur/voir_iep.php')) {
        url = adresse.replace(/https:\/\/ressources.sesamath.net\/coll\/lecteur\/voir_iep\.php\?(.+)/, 'https://manuel.sesamath.net/lecteur/voir_iep.php?$1')
      }
    }

    // #3 urls http://matoumatheux.ac-rennes.fr/
    if (adresse.includes('http://matoumatheux.ac-rennes.fr/')) {
      url = adresse.replace('http://matoumatheux.ac-rennes.fr/', 'https://ressources.sesamath.net/matoumatheux/www/')
    }

    // #4 des accomp restreints à l'auteur, pas normal
    if (ressource._data.origine === 'accomp' && ressource.restriction > 1) {
      motifMaj = `restriction accomp ${ressource.restriction} => 0`
      ressource.restriction = 0
      ressource._data.restriction = 0
      needMaj = true
    }

    // conclusion…
    if (needMaj || (url && url !== adresse)) {
      if (url) ressource._data.parametres.adresse = url
      db.EntityRessource.replaceOne({ _id }, ressource)
      nbPb++
      nbFixed++
      if (!motifMaj) motifMaj = `${adresse} => ${url}`
      print(`fixed (${_id}) ${motifMaj}`)
    }
  })
print(`Fin du script\n${nbPb} urls ayant un pb\n${nbFixed} corrigées\n${nbKo} en erreur`)

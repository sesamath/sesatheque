'use strict'

/* global describe,it */

const assert = require('assert')
const CounterMulti = require('../../../app/server/lib/CounterMulti')

describe('CounterMulti', function () {
  const cm = new CounterMulti()
  it('construct retourne un objet avec une propriété length de 0', function () {
    assert.strictEqual(0, cm.length)
  })
  it('inc incrémente ', function () {
    cm.inc('foo')
    cm.inc('foo')
    cm.inc('bar')
    assert.strictEqual(2, cm.length)
    assert.strictEqual(2, cm.foo)
    assert.strictEqual(1, cm.bar)
  })
  it('dec décrémente ', function () {
    cm.dec('foo')
    cm.dec('baz')
    assert.strictEqual(3, cm.length)
    assert.strictEqual(1, cm.foo)
    assert.strictEqual(-1, cm.baz)
  })
  it('delete efface ', function () {
    cm.delete('foo')
    assert.strictEqual(2, cm.length)
    assert.strictEqual(undefined, cm.foo)
  })
  it('resetLength recalcule la longueur si on ajoute manuellement des compteurs', function () {
    cm.foo = 4
    cm.resetLength()
    assert.strictEqual(3, cm.length)
  })
  it('total additionne tout', function () {
    assert.strictEqual(4, cm.total())
    cm.delete('baz')
    assert.strictEqual(5, cm.total())
  })
})

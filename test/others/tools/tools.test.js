'use strict'

/* global describe,it */

const assert = require('assert')
const tools = require('../../../app/server/lib/tools')
global.log = require('sesajstools/utils/log')

describe('tools', function () {
  describe('encadre', function () {
    it("retourne la valeur fournie si dans l'intervalle", function () {
      assert.strictEqual(42, tools.encadre(42, -2, 48))
    })
    it('retourne la borne inf si trop petit', function () {
      assert.strictEqual(42, tools.encadre(-2, 42, 48))
    })
    it('retourne la borne sup si trop grand', function () {
      assert.strictEqual(42, tools.encadre(52, 2, 42))
    })
  })
})

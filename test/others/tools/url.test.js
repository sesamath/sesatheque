'use strict'

/* global describe,it */

const assert = require('assert')
const url = require('../../../app/server/lib/url')

// @todo tester toutes les méthodes de url

describe('tools.url', function () {
  it('update', function () {
    const { update } = url
    const urlWithoutAnchor = 'https://user:pass@sub.domain.tld:123/path/foo/bar/url.html'
    const anchor = '#anchor'
    let urlStr = urlWithoutAnchor + anchor
    assert.strictEqual(urlStr, update(urlStr))
    assert.strictEqual(urlStr, update(urlStr, null))
    assert.strictEqual(urlStr, update(urlStr, {}))
    assert.strictEqual(urlStr, update(urlStr, {}, { replace: true }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=bar${anchor}`, update(urlStr, { foo: 'bar' }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=true${anchor}`, update(urlStr, { foo: true }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=${anchor}`, update(urlStr, { foo: null }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=false${anchor}`, update(urlStr, { foo: false }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=${anchor}`, update(urlStr, { foo: undefined }))
    const qs = '?foo=bar&baz&titi=42'
    urlStr = urlWithoutAnchor + qs + anchor
    assert.strictEqual(urlStr, update(urlStr))
    assert.strictEqual(urlStr, update(urlStr, null))
    assert.strictEqual(urlStr, update(urlStr, {}))
    assert.strictEqual(`${urlWithoutAnchor}?foo=&baz=&titi=42${anchor}`, update(urlStr, { foo: undefined }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=${anchor}`, update(urlStr, { foo: undefined }, { replace: true }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=bar&baz=&titi=42${anchor}`, update(urlStr, { foo: 'bar' }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=bar${anchor}`, update(urlStr, { foo: 'bar' }, { replace: true }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=barbar&baz=3&titi=42${anchor}`, update(urlStr, { foo: 'barbar', baz: 3 }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=barbar${anchor}`, update(urlStr, { foo: 'barbar' }, { replace: true }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=barbar&baz=3&titi=42${anchor}`, update(urlStr, { foo: 'barbar', baz: 3 }))
    assert.strictEqual(`${urlWithoutAnchor}?foo=barbar&baz=3${anchor}`, update(urlStr, { foo: 'barbar', baz: 3 }, { replace: true }))
  })
})

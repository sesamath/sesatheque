import { expect } from 'chai'
const { decrypt, decryptObj, encrypt, encryptObj } = require('../../../app/utils/crypto')

describe('crypto', () => {
  const samples = [
    ['toto', 'titi'],
    ['', 'nb>p(vj'],
    ['42', 'baasiebnépa'],
    ['\tbaasiebné pas çà.\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'consectetur adipiscing elit']
  ]
  const encrypted = []
  it('chiffre', () => {
    samples.forEach(([msg, key]) => {
      const crypted = encrypt(msg, key)
      expect(crypted).to.be.a('string')
      expect(crypted).to.have.length.at.least(1)
      encrypted.push(crypted)
    })
  })
  it('déchiffre', () => {
    samples.forEach(([msg, key], i) => {
      const decrypted = decrypt(encrypted[i], key)
      expect(decrypted).to.equals(msg, `pb avec déchiffrement de ${msg} avec la clé ${key}`)
    })
  })
  it('chiffre et déchiffre un objet', () => {
    const obj = {
      foo: 'bar',
      baz: 42,
      other: {
        n: null,
        f: false,
        t: true
      }
    }
    const encrypted = encryptObj(obj, 'foo')
    expect(encrypted).to.be.a('string')
    expect(encrypted).to.have.length.at.least(1)
    const decrypted = decryptObj(encrypted, 'foo')
    expect(decrypted).to.deep.equals(obj)
  })
})

/**
 * Ce test crée une ressource puis la supprime
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev
 * ou --token pour lui passer un token
 *
 */

'use strict'
/* eslint-env mocha */
import { expect } from 'chai'
import sinon from 'sinon'
// si on a pas de link vers le module on peut pas aller dans src (mocha aime pas les import que babel ne traite pas because node_modules)
// import {addSesatheque, exists, getBaseId, getBaseUrl} from 'sesatheque-client/src/sesatheques'
import { addSesatheque, exists, getBaseId, getBaseUrl } from 'sesatheque-client/dist/server/sesatheques'
import boot from '../../boot'

const expected = {
  sesabibli: 'https://bibliotheque.sesamath.net/',
  sesacommun: 'https://commun.sesamath.net/'
}

describe('sesatheques', () => {
  let consoleErrorStub
  const baseId = 'stbidon1'
  const baseUrl = 'http://localhost/'
  const baseUrl2 = 'http://localhost:1234/'
  const baseId3 = 'stbidon3'
  let baseUrl3 = 'http://localhost:3'
  let $settings

  before(() => boot()
    .then(({ lassi, testsDone }) => {
      after(testsDone)
      $settings = lassi.service('$settings')
      return Promise.resolve()
    })
  )

  beforeEach(() => {
    consoleErrorStub = sinon.stub(console, 'error')
  })
  afterEach(() => {
    // consoleErrorStub.reset()
    consoleErrorStub.restore()
  })

  it('les 2 sesatheques de prod existent avec les urls attendues', function (done) {
    Object.keys(expected).forEach((baseId) => {
      expect(exists(baseId)).to.be.ok
      expect(getBaseUrl(baseId)).to.equal(expected[baseId])
      expect(getBaseId(expected[baseId])).to.equal(baseId)
    })
    // expect(consoleErrorStub).to.not.have.been.called
    expect(consoleErrorStub.callCount).to.equals(0)

    done()
  })

  it('get de notre sesathèque de test mise au boot', function (done) {
    const { baseId, baseUrl } = $settings.get('application')
    expect(exists(baseId)).to.be.true
    expect(getBaseUrl(baseId)).to.equal(baseUrl)
    expect(getBaseId(baseUrl)).to.equal(baseId)
    // expect(consoleErrorStub).to.not.have.been.called
    expect(consoleErrorStub.callCount).to.equals(0)
    done()
  })

  it('addSesatheque puis get', function (done) {
    expect(addSesatheque(baseId, baseUrl)).to.be.true
    // expect(consoleErrorStub).to.not.have.been.called
    expect(consoleErrorStub.callCount).to.equals(0)
    expect(exists(baseId)).to.be.true
    expect(getBaseUrl(baseId)).to.equal(baseUrl)
    expect(getBaseId(baseUrl)).to.equal(baseId)
    done()
  })
  it('2e add identique renvoie false sans râler', function (done) {
    // on recommence, ça doit renvoyer false
    expect(addSesatheque(baseId, baseUrl)).to.be.false
    // expect(consoleErrorStub).to.not.have.been.called
    expect(consoleErrorStub.callCount).to.equals(0)
    expect(exists(baseId)).to.be.true
    expect(getBaseUrl(baseId)).to.equal(baseUrl)
    expect(getBaseId(baseUrl)).to.equal(baseId)
    done()
  })
  it('add avec même baseId mais baseUrl ≠ => log une erreur', () => {
    // on recommence avec baseUrl ≠, ça doit râler en console
    addSesatheque(baseId, baseUrl2)
    // expect(consoleErrorStub).to.have.been.calledOnce
    expect(consoleErrorStub.callCount).to.equals(1)
    const { firstArg } = consoleErrorStub.getCall(0)
    expect(firstArg instanceof Error).to.be.true
    expect(firstArg.message).to.match(/déjà définie avec une autre base/)
  })
  it('add avec nouvelle baseId mais baseUrl existante => log une erreur', () => {
    addSesatheque(baseId3, baseUrl)
    // expect(consoleErrorStub).to.have.been.calledWithMatch(/était déjà enregistré avec/)
    expect(consoleErrorStub.callCount).to.equals(1)
    const { firstArg } = consoleErrorStub.getCall(0)
    expect(firstArg instanceof Error).to.be.true
    expect(firstArg.message).to.match(/était déjà enregistré avec/)
  })
  it('nouvel add sans slash de fin dans baseUrl, ça l’ȧjoute en le disant', function (done) {
    // on recommence sans le slash de fin
    expect(addSesatheque(baseId3, baseUrl3)).to.be.true
    // expect(consoleErrorStub).to.have.been.calledWithMatch(/doit avoir un slash de fin/)
    expect(consoleErrorStub.callCount).to.equals(1)
    const { firstArg } = consoleErrorStub.getCall(0)
    expect(firstArg).to.match(/doit avoir un slash de fin/)
    baseUrl3 += '/'
    expect(getBaseUrl(baseId3)).to.equal(baseUrl3)
    expect(getBaseId(baseUrl3)).to.equal(baseId3)
    done()
  })

  it('getBaseId throw Error ou console.error si baseUrl inconnue', function (done) {
    const getBaseIdThrow = () => getBaseId('foo')
    const reMsg = /pas l’url d’une sesathèque connue/
    expect(getBaseIdThrow).to.throw(Error, reMsg)
    getBaseId('foo', false)
    // expect(consoleErrorStub).to.have.been.calledWithMatch(reMsg)
    expect(consoleErrorStub.callCount).to.equals(1)
    const { firstArg } = consoleErrorStub.getCall(0)
    expect(firstArg).to.match(reMsg)
    done()
  })
})

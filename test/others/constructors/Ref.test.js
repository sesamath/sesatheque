'use strict'
/* global describe, it */

import Ref from '../../../app/constructors/Ref'
import fakeRef from '../../fixtures/fakeRef'
import fakeRessource from '../../fixtures/fakeRessource'
// cf http://chaijs.com/api/bdd/
import { expect } from 'chai'
import configRessource from '../../../app/server/ressource/config'

const types = Object.keys(configRessource.listes.type)
// vérif que deux objets sont identiques
const check = (data, expected) => {
  // malgré des objets qui semblent vraiment égaux, ce truc passe pas
  // expect(ref).to.deep.equal(refOrig)
  // mais en prenant les clés une par une ça passe…
  const keysData = Object.keys(data).sort()
  const keysExpected = Object.keys(expected).sort()
  expect(keysData).to.deep.equal(keysExpected, 'liste de propriétés')
  keysExpected.forEach(k => expect(data[k]).to.deep.equal(expected[k], `propriété ${k}`))
}

describe('Ref', () => {
  it('Converti une ressource', () => {
    types
      .map(type => fakeRessource({ type }))
      .forEach(ressource => {
        const ref = new Ref(ressource)
        const expectedFields = ['aliasOf', 'type', 'titre', 'resume', 'commentaires', 'description', 'public', 'categories']
        // on a jamais d'enfants sur une ref venant d'une ressource (car y'a un aliasOf)
        // @todo tirer ça au clair
        // en attendant on suit le constructeur Ref
        if (ressource.type === 'arbre' && Array.isArray(ressource.enfants)) expectedFields.push('enfants')
        else if (ressource.type === 'sequenceModele' && ressource.parametres) expectedFields.push('parametres')
        expect(Object.keys(ref).sort()).to.deep.equal(expectedFields.sort(), 'pb sur la liste des champs')
        expectedFields.forEach(p => {
          if (p === 'aliasOf') expect(ref[p]).to.equal(ressource.rid, 'Pb rid => aliasOf')
          else if (p === 'public') expect(ref[p]).to.equal(ressource.publie && !ressource.restriction, 'Pb rid => aliasOf')
          else expect(ref[p]).to.deep.equal(ressource[p], `Pb ${p}`)
        })
      })
  })

  it('Laisse inchangé une ref', () => {
    const refOrig = fakeRef()
    const ref = new Ref(refOrig)
    check(ref, refOrig)
  })

  it('conserve les enfants d’une ref de type arbre sans aliasOf (un dossier d’arbre)', () => {
    const data = fakeRef({ type: 'arbre' })
    data.enfants = [fakeRef()]
    data.enfants.push(fakeRef())
    delete data.aliasOf
    check(new Ref(data), data)
  })
})

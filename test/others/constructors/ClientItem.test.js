'use strict'
/* global describe, it */
import Ref from 'sesatheque-client/src/constructors/Ref'
// si on a pas de link vers le module on peut pas aller dans src
// import ClientItem from 'sesatheque-client/src/constructors/ClientItem'
import ClientItem from 'sesatheque-client/src/constructors/ClientItem'
import { addSesatheque, exists } from 'sesatheque-client/src/sesatheques'

import fakeRef from '../../fixtures/fakeRef'
import fakeRessource from '../../fixtures/fakeRessource'
// cf http://chaijs.com/api/bdd/
import { expect } from 'chai'
import config from '../../../app/server/config'
import configRessource from '../../../app/server/ressource/config'

const myBaseId = config.application.baseId
const myBaseUrl = config.application.baseUrl
const types = Object.keys(configRessource.listes.type)

describe('ClientItem', () => {
  const getRessourcesCollection = () => types.map(type => fakeRessource({ type }))
  const getRefCollection = () => types.map(type => fakeRef({ type }))
  const { describe, display } = configRessource.constantes.routes

  before(() => {
    if (!exists(myBaseId)) addSesatheque(myBaseId, myBaseUrl)
  })

  it('Converti une ressource', () => {
    // ressource publiques
    getRessourcesCollection()
      .forEach(ressource => {
        ressource.$droits = 'R'
        const item = new ClientItem(ressource)
        const expectedFields = ['aliasOf', 'type', 'titre', 'resume', 'commentaires', 'description', 'public', 'categories']
        expectedFields.forEach(p => {
          if (p === 'public') expect(item.public).to.equal(ressource.publie && !ressource.restriction, `Pb restriction => public\n${JSON.stringify(ressource)}\n=>\n${JSON.stringify(item)}`)
          else expect(item[p]).to.deep.equal(ressource[p], `Pb ${p}\n${JSON.stringify(ressource)}\n=>\n${JSON.stringify(item)})`)
        })
        // url
        expect(item.$displayUrl).to.equal(`${myBaseUrl}public/${display}/${ressource.oid}`, 'pb $displayUrl')
        expect(item.$describeUrl).to.equal(`${myBaseUrl}ressource/${describe}/${ressource.oid}`, 'pb $describeUrl')
        expect(item.$dataUrl).to.equal(`${myBaseUrl}api/public/${ressource.oid}`, 'pb $dataUrl')
        expect(item, 'pb $editUrl').not.to.have.property('$editUrl')
        expect(item).not.to.have.property('$deleteUrlApi')
      })
  })
  it('Converti une ref', () => {
    // ressource publiques
    getRefCollection()
      .forEach(ref => {
        ref.$droits = 'R'
        const item = new ClientItem(ref)
        const expectedFields = ['type', 'titre', 'resume', 'commentaires', 'description', 'public', 'categories']
        expect(item.rid).to.equal(ref.aliasOf, 'Pb aliasOf => rid')
        if (!ref.aliasOf && ref.type === 'arbre') expectedFields.push('enfants')
        expectedFields.forEach(p => expect(item[p]).to.deep.equal(ref[p], `Pb ${p}`))
        // url
        const oid = ref.aliasOf.substring(myBaseId.length + 1)
        if (ref.public) {
          expect(item.$displayUrl).to.equal(`${myBaseUrl}public/${display}/${oid}`, 'pb $displayUrl')
          expect(item.$describeUrl).to.equal(`${myBaseUrl}ressource/${describe}/${oid}`, 'pb $describeUrl')
          expect(item.$dataUrl).to.equal(`${myBaseUrl}api/public/${oid}`, 'pb $dataUrl')
        } else {
          expect(item.$displayUrl).to.equal(`${myBaseUrl}public/${display}/cle/${ref.cle}`, 'pb $displayUrl')
          expect(item.$describeUrl).to.equal(`${myBaseUrl}ressource/${describe}/${oid}`, 'pb $describeUrl')
          expect(item.$dataUrl).to.equal(`${myBaseUrl}api/ressource/${oid}`, 'pb $dataUrl')
        }
        expect(item, 'pb $editUrl').not.to.have.property('$editUrl')
        expect(item).not.to.have.property('$deleteUrlApi')
      })
  })

  it('Laisse inchangé un clientItem', () => {
    const itemOrig = fakeRef()
    const item = new Ref(itemOrig)
    // malgré des objets qui semblent vraiment égaux, ce truc passe pas
    // expect(item).to.deep.equal(itemOrig)
    // mais en prenant les clés une par une ça passe…
    const keysOrig = Object.keys(itemOrig).sort()
    const keys = Object.keys(item).sort()
    expect(keys).to.deep.equal(keysOrig, 'liste de propriétés')
    keys.forEach(k => expect(item[k]).to.deep.equal(itemOrig[k], `propriété ${k}`))
  })
})

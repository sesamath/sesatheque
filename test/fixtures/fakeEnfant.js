'use strict'

import faker from 'faker/locale/fr'
import { fakeOid } from './fakePersonne'

import config from '../../app/server/config'

const myBaseId = config.application.baseId
const configRessource = config.components.ressource
const types = Object.keys(configRessource.listes.type)
const categoriesIds = Object.keys(configRessource.listes.categories)

function getFakeEnfant (options) {
  if (!options) options = {}
  const fakeEnfant = {
    aliasOf: options.aliasOf || myBaseId + '/' + fakeOid(),
    type: options.type || faker.random.arrayElement(types),
    titre: options.titre || faker.lorem.words()
  }
  // on prend les catégories imposées par le type, ou une au pif
  fakeEnfant.categories = config.components.ressource.typeToCategories[fakeEnfant.type] || [faker.random.arrayElement(categoriesIds)]

  return fakeEnfant
}

export default getFakeEnfant

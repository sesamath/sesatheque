'use strict'

import faker from 'faker/locale/fr'
import config from '../../app/server/config'
const myBaseId = config.application.baseId

// faut virer les - des pid car ça passe pas la regex de vérif du constructeur Ressource
export const fakeOid = () => faker.random.uuid().replace(/-/g, '')

/**
 * Retourne une ressource avec du contenu aléatoire (avec publié, restriction à 0 et sans auteurs ni contributeur si on les impose pas)
 * @param {object} options ajouter des propriétés imposées, noOid, noRid, noOrigin, noIdOrigin
 * @return {Ressource}
 */
function getFakePersonne (options) {
  if (typeof options !== 'object') options = {}
  /**
   * Un plain object avec les valeurs d'une personne
   * @type {Personne}
   */
  const fakePersonne = {}
  if (!options.nooid) fakePersonne.oid = options.oid || fakeOid()
  if (!options.nopid) fakePersonne.pid = options.pid || myBaseId + '/' + fakePersonne.oid
  if (!options.notitre) fakePersonne.nom = options.nom || faker.name.lastName()
  if (!options.noresume) fakePersonne.prenom = options.prenom || faker.name.firstName()
  // cf app/config.js : faker.random.arrayElement(['formateur', 'admin', 'editeur', …])
  if (!options.nodescription) fakePersonne.roles = options.roles || { formateur: true }
  if (!options.nocommentaires) fakePersonne.email = options.email || faker.internet.email()
  // @todo ajout groupesMembre et groupesSuivis

  return fakePersonne
}

export default getFakePersonne

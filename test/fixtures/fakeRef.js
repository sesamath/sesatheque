'use strict'

import faker from 'faker/locale/fr'
import { fakeOid } from './fakePersonne'

import config from '../../app/server/config'

const myBaseId = config.application.baseId
const configRessource = config.components.ressource
const types = Object.keys(configRessource.listes.type)
const categoriesIds = Object.keys(configRessource.listes.categories)

function getFakeRef (options) {
  if (!options) options = {}
  const fakeRef = {
    aliasOf: options.aliasOf || myBaseId + '/' + fakeOid(),
    type: options.type || faker.random.arrayElement(types),
    titre: options.titre || faker.lorem.words(),
    resume: options.resume || faker.lorem.sentence(),
    description: options.description || faker.lorem.paragraphs(),
    commentaires: options.commentaires || faker.lorem.paragraphs(),
    categories: options.categories || faker.random.arrayElement(types),
    public: options.public || faker.random.arrayElement([true, true, true, false])
  }
  // on prend les catégories imposées par le type, ou une au pif
  fakeRef.categories = config.components.ressource.typeToCategories[fakeRef.type] || [faker.random.arrayElement(categoriesIds)]
  // pour les ressources privées on génère une clé
  if (!fakeRef.public) fakeRef.cle = fakeOid()

  return fakeRef
}

export default getFakeRef

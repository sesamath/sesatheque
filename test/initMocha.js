'use strict'

const fs = require('node:fs')
const path = require('node:path')
/* eslint-env mocha */
// on peut mettre dans mocha.opts un
// --require babel-core/register
// mais il faut lui passer des options, on le fait ici et remplace la ligne précédente par
// --require ./test/initMocha

// on veut exclure les node_modules sauf sesatheque-client/src
// faut retourner true pour que babel ignore le fichier
const babelIgnoreFilter = (file) => {
  // en cas de pb "Unexpected token import", décommenter la ligne suivante pour trouver qui pose pb
  // console.log(`babelize ${file} ?`)
  if (/\/node_modules\//.test(file)) {
    if (/\/app\/plugins/.test(file)) return false
    return !/\/sesatheque-client\/src\//.test(file)
  }
  return false
}

require('ignore-styles')
require('@babel/register')({
  // faut pas qu'il lise les preset du package.json, sinon ça donne du
  // Error: Options {"loose":true} passed to  /home/sesamath/projets/git/sesatheque/node_modules/babel-preset-env/lib/index.js which does not accept options. (While processing preset: …
  babelrc: false,
  // notre filtre sur les fichiers à traiter
  ignore: [babelIgnoreFilter],
  presets: [
    ['@babel/preset-env', { targets: 'node 14' }],
    ['@babel/preset-react']
  ],
  plugins: [
    ['module-resolver', {
      root: ['./app'],
      alias: {
        plugins: path.join(__dirname, '../test/react/plugins')
      }
    }]
  ]
})

// si on lance les tests react, faut aider require à trouver prop-types
// (il n'y arrive pas si les modules ont été installés avec pnpm)
// en passant ici, on ne sait pas si on passe tous les tests (argv avec 'test/**/*.test.js')
// ou seulement react (argv avec 'test/react/**/*.test.js', donc on l'inclus tout le temps
try {
  require('prop-types')
} catch (error) {
  // on tente de le chercher dans les node_modules de react
  const reactDir = path.dirname(require.resolve('react'))
  const reactModules = path.resolve(reactDir, '..', '..', 'node_modules')
  if (fs.existsSync(reactModules)) {
    process.env.NODE_PATH = reactModules + (process.env.NODE_PATH ? path.delimiter + process.env.NODE_PATH : '')
    // reste à regénérer les paths dans lesquels require fouine
    require('module')._initPaths()
  } else {
    throw Error(`require ne trouvera pas le module prop-types de react (ni node_modules/prop-types ni ${reactModules}/prop-types)`)
  }
}

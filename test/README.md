==== Tests mocha

On utilise mocha pour les tests, avec l'interface par défaut (bdd, cf http://mochajs.org/#interfaces)

Cf scripts/test pour les lancer

On purge la db en before mais pas en after (ça permet de pouvoir aller voir ce qu'il y a dedans si un test échoue).
Cf commit bf823a56 pour une version avec `after(purge)` généralisé.

/**
 * Ce test crée une ressource puis la supprime, options possibles :
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev,
 * (sinon c'est la bibli locale)
 * ou --token pour lui passer un token
 */

'use strict'
/* eslint-env mocha */

import { expect } from 'chai'
import boot from '../../boot'
import { purge } from '../populate'

describe('EntityGroupe', () => {
  // une erreur toute prête
  // const errAbort = new Error('pas la peine de tester ça tant que ça plante avant')
  const groupeData = {
    nom: 'Groupe ACCENTUÉ',
    description: 'Un groupe ouvert de test.\nAvec un peu de bla bla.',
    ouvert: true,
    public: true,
    gestionnaires: ['oidXX'] // il en faut un
  }

  // @todo ajouter les groupes au populate et gérer des tests indépendants, avec une vérif faite par un client mongo
  // une entity globale,
  let groupe
  let EntityGroupe

  const checkGroupe = (groupe) => {
    ;['nom', 'description', 'ouvert', 'public'].forEach(p => {
      expect(groupe[p]).to.equal(groupeData[p])
    })
    expect(groupe.gestionnaires).to.deep.equal(groupeData.gestionnaires)
  }

  // boot + récup des services et config nécessaires à nos tests
  before(async () => {
    const { lassi, testsDone } = await boot()
    after(testsDone)
    if (!lassi) throw Error('boot KO lassi')
    EntityGroupe = lassi.service('EntityGroupe')
    // on démarre sur une base vide
    await purge()
  })

  it('create', function () {
    checkGroupe(EntityGroupe.create(groupeData))
  })

  it('store', function (done) {
    EntityGroupe.create(groupeData).store((error, _groupe) => {
      if (error) return done(error)
      groupe = _groupe
      checkGroupe(groupe)
      done()
    })
  })

  it('grab', function (done) {
    EntityGroupe
      .match('nom').equals(groupeData.nom)
      .grab((error, groupes) => {
        if (error) return done(error)
        expect(groupes.length).to.equals(1)
        checkGroupe(groupes[0])
        done()
      })
  })

  it('grabOne', function (done) {
    EntityGroupe
      .match('nom').equals(groupeData.nom)
      .grabOne((error, groupe) => {
        if (error) return done(error)
        checkGroupe(groupe)
        done()
      })
  })

  it('delete', function (done) {
    groupe.delete(function (error) {
      if (error) return done(error)
      EntityGroupe
        .match('nom').equals(groupeData.nom)
        .grab(function (error, groupes) {
          if (error) return done(error)
          expect(groupes).to.have.length(0)
          done()
        })
    })
  })

  it('purge', function (done) {
    EntityGroupe.create(groupeData).store((error, groupe) => {
      if (error) return done(error)
      checkGroupe(groupe)
      EntityGroupe.match().purge((error, nb) => {
        if (error) return done(error)
        expect(nb).to.equal(1, 'pb de nb d’entities purgées')
        EntityGroupe.match().grab((error, groupes) => {
          if (error) return done(error)
          expect(groupes.length).to.equal(0, 'il reste des groupes')
          done()
        })
      })
    })
  })
})

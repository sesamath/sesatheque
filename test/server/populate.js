/**
 * Ce test crée une ressource puis la supprime, options possibles :
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev,
 * (sinon c'est la bibli locale)
 * ou --token pour lui passer un token
 */

'use strict'
/* eslint-env mocha */
import fakeRessource from '../fixtures/fakeRessource'
import fakePersonne from '../fixtures/fakePersonne'
import flow from 'an-flow'

import boot from '../boot'
import config from '../../app/server/config'

const myBaseId = config.application.baseId
// pour nos 1ers tests
const nbPersonnesDefault = 10
const nbRessourcesDefault = 20

const personnes = new Map()
const ressources = new Map()
let pOids = []
let rOids = []

/**
 * Ajoute une ressource particulière (et file l'entity à next)
 * @param ressource
 * @param next
 */
export function addRessource (ressource, next) {
  boot().then(({ lassi }) => {
    lassi.service('EntityRessource').create(ressource).store(next)
  }).catch(next)
}

/**
 * Retourne une des personnes générées au hasard
 * @param {boolean} [pidOnly] passer true pour ne récupérer que le pid
 * @param {string[]} [except] Passer une liste de pid que l'on ne veut pas
 * @return {Personne}
 */
export function getRandomPersonne (pidOnly, except) {
  if (!personnes.size) throw new Error('Les personnes n’ont pas encore été générées (il faut appeler populate)')
  const i = Math.floor(Math.random() * pOids.length)
  const oid = pOids[i]
  const pid = `${myBaseId}/${oid}`
  if (except && except.includes(pid)) return getRandomPersonne(pidOnly, except)
  if (pidOnly) return pid
  return personnes.get(oid)
}

/**
 * Retourne une des ressources générées au hasard
 * @param {string[]} [oidsExcluded] Passer une liste d'oids que l'on ne voudrait pas
 * @return {Ressource}
 */
export function getRandomRessource (oidsExcluded) {
  if (!ressources.size) throw new Error('Les ressources n’ont pas encore été générées (il faut appeler populate)')
  const oidsList = oidsExcluded?.length
    ? Array.from(ressources.keys()).filter(oid => !oidsExcluded.includes(oid))
    : Array.from(ressources.keys())
  const randomOid = oidsList[Math.floor(Math.random() * oidsList.length)]
  return ressources.get(randomOid)
}

/**
 * Retourne les personnes générées
 * @return {Object[]}
 */
export const getPersonnes = () => Array.from(personnes.values())
/**
 * Retourne les ressources générées
 * @return {Object[]}
 */
export const getRessources = () => Array.from(ressources.values())

/**
 * Génère des personnes et des ressources avec des données aléatoires mais cohérentes,
 * et les enregistre en base (via les entités lassi)
 * @param {object} [options]
 * @param {boolean} [options.purge=false] Passer true pour purger la db avant de créer les entities
 * @param {number} [options.nbPersonnes=10] Nb de personnes à générer
 * @param {number} [options.nbRessources=20] Nb de ressources à générer
 * @return {Promise<{ressources: Object[], personnes: Object[]}>}
 */
export async function populate (options) {
  // init avec nos valeurs par défaut
  if (!options) options = {}
  if (options.purge) await purge()
  const nbPersonnes = options.nbPersonnes || nbPersonnesDefault
  const nbRessources = options.nbRessources || nbRessourcesDefault
  // p'tet rien à faire
  if (nbPersonnes <= personnes.size && nbRessources <= ressources.size) return { personnes: getPersonnes(), ressources: getRessources() }

  // faut booter (si personne l'a fait avant) et récupérer lassi
  const { lassi } = await boot()
  const EntityPersonne = lassi.service('EntityPersonne')
  const EntityRessource = lassi.service('EntityRessource')
  return new Promise((resolve, reject) => {
    let i
    let hasArbre = false
    // console.log(`start populate`)
    flow(new Array(nbPersonnes)).seqEach(function () {
      const nextPersonne = this
      EntityPersonne.create(fakePersonne()).store((error, personne) => {
        if (error) return nextPersonne(error)
        personnes.set(personne.oid, personne)
        pOids.push(personne.oid)
        nextPersonne()
      })
    }).seq(function () {
    // console.log(`${personnes.size} personnes générées`)
      this(null, new Array(nbRessources))
    }).seqEach(function () {
      const nextRessource = this
      const options = {
        nooid: true,
        norid: true
      }
      // on veut au moins un arbre dans le tas
      if (!hasArbre) {
        options.type = 'arbre'
        hasArbre = true
      }
      let pid1
      if (nbPersonnes > 0) {
      // par défaut un auteur
        pid1 = getRandomPersonne(true)
        options.auteurs = [pid1]
        if (i % 10 === 1 && nbPersonnes > 1) {
        // dans 10% des cas un 2e auteur
          options.auteurs.push(getRandomPersonne(true, [pid1]))
        } else if (i % 10 === 2 && nbPersonnes > 1) {
        // dans 10% des cas un contributeur
          options.contributeurs = [getRandomPersonne(true, [pid1])]
        } else if (i % 10 === 3 && nbPersonnes > 2) {
        // dans 10% des cas deux contributeurs
          const pid2 = getRandomPersonne(true, [pid1])
          const pid3 = getRandomPersonne(true, [pid1, pid2])
          options.contributeurs = [pid2, pid3]
        } else if (i % 10 === 4 && nbPersonnes > 4) {
        // dans 10% des cas un 2e auteur et 3 contributeurs
          const pid2 = getRandomPersonne(true, [pid1])
          const pid3 = getRandomPersonne(true, [pid1, pid2])
          const pid4 = getRandomPersonne(true, [pid1, pid2, pid3])
          const pid5 = getRandomPersonne(true, [pid1, pid2, pid3, pid4])
          options.auteurs.push(pid2)
          options.contributeurs = [pid3, pid4, pid5]
        }
      }
      const ressource = fakeRessource(options)
      EntityRessource.create(ressource).store((error, ressourceDb) => {
        if (error) return nextRessource(error)
        ressources.set(ressourceDb.oid, ressourceDb)
        rOids.push(ressourceDb.oid)
        nextRessource()
      })
    }).seq(function () {
      // console.log(`${personnes.size} personnes et ${ressources.size} ressources générées`)
      resolve({ personnes: getPersonnes(), ressources: getRessources() })
    }).catch(reject)
  })
}

/**
 * Efface toutes les collections de la base (sauf EntityUpdate)
 * @return {Promise<void>}
 */
export async function purge () {
  const purgeEntity = (def) => def.match().purge()
  const { lassi } = await boot()
  await Promise.all([
    lassi.service('EntityPersonne'),
    lassi.service('EntityRessource'),
    lassi.service('EntityGroupe'),
    lassi.service('EntityArchive')
    // on ne purge pas EntityUpdate pour conserver dans la base de test
    // le dernier update et gagner du temps à chaque boot des tests
  ].map(purgeEntity))
  // on flush nos variables internes
  personnes.clear()
  ressources.clear()
  pOids = []
  rOids = []
  // et le cache
  return lassi.service('$cache').purge()
}

/**
 * Purge une collection
 * @param {string} entityName
 * @return {Promise}
 */
export async function purgeOne (entityName) {
  const { lassi } = await boot()
  return lassi.service(entityName).match().purge()
}

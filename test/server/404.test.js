/**
 * Ce test crée une ressource puis la supprime
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev
 * ou --token pour lui passer un token
 *
 */

'use strict'
/* eslint-env mocha */
import { expect } from 'chai'
import boot from '../boot'
import { getHtml } from '../../app/server/main/reactPage'

describe('prend un 404 sur les urls inexistantes', function () {
  const paths = ['/public/foo/bar', '/ressource/foo/bar', '/public/foo', '/ressource/foo', '/foo/bar']
  let _superTestClient
  this.timeout(60000)

  before(() => boot().then(({ superTestClient, testsDone }) => {
    after(testsDone)
    _superTestClient = superTestClient
  }))

  const reactPage = getHtml()

  paths.forEach(path => {
    // on retourne une promesse
    it(`404 sur ${path} avec la page react`, () =>
      _superTestClient
        .get(path)
        .expect(404, reactPage)
        .expect('Content-Type', /text\/html/)
    )
  })
  paths.forEach(path => {
    it(`404 sur /api${path} avec un message KO`, () =>
      _superTestClient
        .get('/api' + path)
        .expect(404)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          expect(res.body.message).to.contain('n’existe pas')
        })
    )
  })
})

/**
 * Ce test crée une ressource puis la supprime, options possibles :
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev,
 * (sinon c'est la bibli locale)
 * ou --token pour lui passer un token
 */

'use strict'
/* eslint-env mocha */

import { expect } from 'chai'
import boot from '../../boot'
import flow from 'an-flow'
import fakePersonne from '../../fixtures/fakePersonne'
import { getRandomPersonne, populate, purge } from '../populate'

describe('EntityPersonne', () => {
  let EntityPersonne, $settings
  const checkPersonne = (personne, expected) => {
    Object.keys(expected).forEach(p => expect(personne[p]).to.deep.equals(expected[p], `Pb avec propriété ${p}`))
  }

  // boot + récup des services et config nécessaires à nos tests
  before(async () => {
    const { lassi, testsDone } = await boot()
    if (!lassi) throw Error('boot KO lassi')
    after(testsDone)
    EntityPersonne = lassi.service('EntityPersonne')
    $settings = lassi.service('$settings')
    await purge()
  })

  afterEach(purge)

  it('create (avec permissions set n’importe comment)', () => {
    // on teste un create avec tous les rôles définis en config, plus un inconnu
    const configRoles = $settings.get('components.personne.roles')
    const rolesList = Object.keys(configRoles)
    rolesList.push('inconnu')
    const unknownPermissions = { foo: true, bar: true }
    rolesList.forEach(role => {
      const isKnownRole = role !== 'inconnu'
      const personneData = fakePersonne()
      personneData.roles = { [role]: true }
      personneData.permissions = unknownPermissions
      const personne = EntityPersonne.create(personneData)
      // ça on le vérifie séparément après
      delete personneData.permissions
      checkPersonne(personne, personneData)
      if (isKnownRole) {
        // les permissions d'un rôle sont fixée par la config, ça écrase tout ce qu'on peut passer au constructeur
        expect(personne.permissions).not.to.have.property('foo')
        expect(personne.permissions).not.to.have.property('bar')
        expect(personne.permissions).to.deep.equal(configRoles[role], `Pb de permissions sur rôle ${role}`)
      } else {
        expect(personne.permissions).to.deep.equal({}, 'Pb de permissions sur rôle inconnu')
      }
    })
  })

  it('store', (done) => {
    const personneData = fakePersonne()
    EntityPersonne.create(personneData).store((error, personne) => {
      if (error) return done(error)
      checkPersonne(personne, personneData)
      done()
    })
  })

  it('grab', async () => {
    await populate({ nbRessources: 0, nbPersonnes: 3 })
    const personne = getRandomPersonne()
    const matchers = [
      ['oid', personne.oid],
      ['pid', personne.pid],
      ['nom', personne.nom] // lui pourrait remonter des homonymes, mais avec 3 noms aléatoires…
    ]
    return new Promise((resolve, reject) => {
      flow(matchers).seqEach(function ([prop, value]) {
        EntityPersonne.match(prop).equals(value).grab(this)
      }).seqEach(function (personnes) {
        expect(personnes, 'Pb sur le nb de personnes remontées').to.have.lengthOf(1)
        checkPersonne(personnes[0], personne)
        resolve()
      }).catch(reject)
    })
  })

  it('grabOne', async () => {
    await populate({ nbRessources: 0, nbPersonnes: 3 })
    const personne = getRandomPersonne()
    const matchers = [
      ['oid', personne.oid],
      ['pid', personne.pid],
      ['nom', personne.nom] // lui pourrait remonter des homonymes, mais avec 3 noms aléatoires…
    ]
    return new Promise((resolve, reject) => {
      flow(matchers).seqEach(function ([prop, value]) {
        EntityPersonne.match(prop).equals(value).grabOne(this)
      }).seqEach(function (personneFound) {
        checkPersonne(personneFound, personne)
        resolve()
      }).catch(reject)
    })
  })

  it('Delete ok', async () => {
    await populate({ nbRessources: 0, nbPersonnes: 3 })
    return new Promise((resolve, reject) => {
      getRandomPersonne().delete((error) => {
        if (error) return reject(error)
        EntityPersonne.match().grab((error, personnes) => {
          if (error) return reject(error)
          expect(personnes).to.have.lengthOf(2)
          resolve()
        })
      })
    })
  })
})

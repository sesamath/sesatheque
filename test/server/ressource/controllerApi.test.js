/**
 * Ce test crée une ressource puis la supprime, options possibles :
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev,
 * (sinon c'est la bibli locale)
 * ou --token pour lui passer un token
 */

'use strict'
/* eslint-env mocha */
import { expect } from 'chai'
import faker from 'faker/locale/fr'
import { getRandomPersonne, getRandomRessource, getRessources, populate, purge } from '../populate'
import boot from '../../boot'
import configRessource from '../../../app/server/ressource/config'
import config from '../../../app/server/config'
import helpersFactory from './helpers'

const { application: { baseId: myBaseId } } = config

describe('controller api ressource', function () {
  this.timeout(10000)

  // pour les appels authentifiés via token
  let apiTokenEncoded
  // le client express instancié en before
  let _superTestClient
  let $settings
  // nos helpers
  let checkDb
  let checkHttp
  let cleanVolatileProperties
  let getTestRessources
  let purgeOnError
  let _testsDone
  // pour du debug (l'utiliser dans un test)
  // let _lassi
  //
  // const resumeRessource = r => `${r.oid} ${r.rid} (${r.origine} - ${r.idOrigine})`
  // const printDbRessources = async () => {
  //   const dbRessources = await _lassi.service('EntityRessource').match().grab()
  //   const traces = dbRessources.map(resumeRessource)
  //   console.log('en db on a les ressources\n', traces.join('\n'))
  // }
  // fin debug

  // boot + récup des services et config nécessaires à nos tests
  before(async () => {
    const { lassi, superTestClient, testsDone } = await boot()
    // _lassi = lassi // debug
    if (!superTestClient) throw Error('boot KO stc')
    if (!lassi) throw Error('boot KO lassi')
    _testsDone = testsDone
    _superTestClient = superTestClient
    $settings = lassi.service('$settings')
    const apiToken = $settings.get('apiTokens')[0]
    if (!apiToken) throw Error('pas trouvé apiTokens en configuration')
    apiTokenEncoded = encodeURIComponent(apiToken)
    const h = helpersFactory(lassi, superTestClient)
    checkDb = h.checkDb
    checkHttp = h.checkHttp
    cleanVolatileProperties = h.cleanVolatileProperties
    getTestRessources = h.getTestRessources
    purgeOnError = h.purgeOnError
    // on démarre sur une base vide
    return purge()
  })

  // chaque test doit gérer sa purge, on en veut pas forcément après chaque it
  // car on peut vouloir conserver ses ressources au sein d'un même describe
  // afterEach(purge)

  after(async () => {
    if (_testsDone) _testsDone()
  })

  it('POST enregistre une ressource et retourne son oid', function () {
    const getPostPromise = (ressource) => _superTestClient
      .post('/api/ressource')
      .set('Content-Type', 'application/json')
      .set('X-ApiToken', apiTokenEncoded)
      .send(ressource)
      .expect(200)
      .then(({ body: { message, data } }) => {
        expect(message).to.equal('OK')
        const { oid } = data
        expect(!!oid).to.be.true
        ressource.oid = oid
        ressource.rid = `${myBaseId}/${oid}`
        cleanVolatileProperties(ressource)
        // la ressource n'existait pas donc version a été incrémenté
        delete ressource.version
        return checkDb(ressource)
      })

    const ressources = getTestRessources().map(r => {
      // si on crée une ressource avec oid ou version l'api va vouloir archiver
      // et ça plantera car y'a pas de version antérieure
      delete r.oid
      delete r.version
      return r
    })

    // purge puis poste et vérifie puis purge
    return purge()
      .then(() => Promise.all(ressources.map(getPostPromise)))
  })

  it('POST /api/ressource met à jour une ressource si modif du résumé', async () => {
    function checkOne (ressource) {
      const { oid } = ressource
      cleanVolatileProperties(ressource)
      ressource.resume += faker.lorem.words()
      const postData = {
        oid,
        resume: ressource.resume
      }
      return _superTestClient
        .post('/api/ressource')
        .set('Content-Type', 'application/json')
        .set('X-ApiToken', apiTokenEncoded)
        .send(postData)
        .expect(200)
        .then(({ body: { message, data } }) => {
          expect(message).to.equal('OK')
          expect(data.oid).to.equal(oid)
          return checkDb(ressource)
        })
    }

    const { ressources } = await populate({ nbRessources: 10, nbPersonnes: 6 })
    while (ressources.length) {
      const ressource = ressources.pop()
      try {
        await checkOne(ressource)
      } catch (error) {
        console.error(`Pb sur le POST /api/ressource avec résumé modifié pour ${ressource.oid}`, error)
        throw error
      }
    }
  })

  it('POST /api/ressource met à jour une ressource et incrémente version si modif des auteurs', async () => {
    function checkOne (ressource) {
      const { oid } = ressource
      cleanVolatileProperties(ressource)
      // on ajoute un auteur
      const excludedPids = ressource.auteurs.concat(ressource.contributeurs)
      ressource.auteurs.push(getRandomPersonne(true, excludedPids))
      // on incrémente version
      ressource.version++
      // mais on ne poste que oid & auteurs
      const postData = {
        oid,
        auteurs: ressource.auteurs
      }
      return _superTestClient
        .post('/api/ressource')
        .set('Content-Type', 'application/json')
        .set('X-ApiToken', apiTokenEncoded)
        .send(postData)
        .expect(200)
        .then(({ body: { message, data } }) => {
          expect(message).to.equal('OK')
          expect(data.oid).to.equal(oid)
          checkDb(ressource)
        })
    }
    const { ressources } = await populate({ nbRessources: 10, nbPersonnes: 6 })
    while (ressources.length) {
      const ressource = ressources.pop()
      try {
        await checkOne(ressource)
      } catch (error) {
        console.error(`Pb sur le POST /api/ressource avec modif auteurs pour ${ressource.oid}`, error)
        throw error
      }
    }
  })

  it('POST /api/ressource met à jour une ressource sans incrémenter la version avec une modif de relations', async () => {
    // ajoute une relation à ressource, poste et vérifie le résultat retourné + db
    function checkOne (ressource) {
      const { oid } = ressource
      cleanVolatileProperties(ressource)
      // on ajoute une relation
      const linkedRess = getRandomRessource([ressource.oid])
      ressource.relations.push([configRessource.constantes.relations.assocA, linkedRess.rid])
      if (!ressource.relationsTitres) ressource.relationsTitres = {}
      ressource.relationsTitres[linkedRess.rid] = linkedRess.titre
      const postData = {
        oid,
        relations: ressource.relations,
        relationsTitres: ressource.relationsTitres
      }
      return _superTestClient
        .post('/api/ressource')
        .set('Content-Type', 'application/json')
        .set('X-ApiToken', apiTokenEncoded)
        .send(postData)
        .expect(200)
        .then(() => checkDb(ressource))
    }

    const { ressources } = await populate({ nbRessources: 2, nbPersonnes: 6 })
    while (ressources.length) {
      const ressource = ressources.pop()
      try {
        await checkOne(ressource)
      } catch (error) {
        console.error(`Pb sur le POST /api/ressource avec modif relations pour ${ressource.oid}`, error)
        throw error
      }
    }
  })

  it('DELETE prend un 401 si on veut effacer sans token ni session', function () {
    return populate({ nbRessources: 1, nbPersonnes: 1 })
      .then(() => {
        const ressource = getRandomRessource()
        return _superTestClient
          .delete(`/api/ressource/${ressource.oid}`)
          .expect(401)
          .expect('Content-type', /application\/json/)
      })
      .then(({ body }) => {
        expect(body).to.have.property('message')
        expect(body.message).to.contains('pas de droits suffisants')
        expect(Object.keys(body)).to.have.lengthOf(1, 'Pb sur le nb de propriétés de result')
        return Promise.resolve()
      })
      .catch(purgeOnError)
      .then(purge)
  })

  it('DELETE vire une ressource', function () {
    let ressource
    return populate({ nbRessources: 1, nbPersonnes: 1 })
      .then(() => {
        ressource = getRandomRessource()
        return _superTestClient
          .delete(`/api/ressource/${ressource.oid}`)
          .set('X-ApiToken', apiTokenEncoded)
          .expect(200)
          .expect('Content-type', /application\/json/)
      })
      .then(({ body: result }) => {
        expect(result).to.have.property('message', 'OK', 'Pb sur result.message')
        expect(result).to.have.property('data')
        expect(result.data).to.have.property('deleted', ressource.oid, 'Pb sur result.deleted')
        expect(Object.keys(result)).to.have.lengthOf(2, 'Pb sur le nb de propriétés de result')
      })
      .catch(purgeOnError)
      .then(purge)
  })

  describe('GET /api/ressource/… sur ressource publique', () => {
    let ressources
    const checkAll = async (urlConstructor) => {
      for (const r of ressources) {
        const url = urlConstructor(r)
        try {
          // console.log(`test sur ${url} avec`, resumeRessource(r))
          await checkHttp(url, r)
        } catch (error) {
          console.error(`Pb avec ${url} pour la ressource ${r.oid} : ${error.message}`)
          throw error
        }
      }
    }

    before(async () => {
      await purge()
      await populate({ nbRessources: 6, nbPersonnes: 6 })
      ressources = getRessources().map(cleanVolatileProperties)
    })

    it('GET /api/ressource/:oid', () => checkAll(r => `/api/ressource/${r.oid}`))
    it('GET /api/ressource/:baseId/:oid', () => checkAll(r => `/api/ressource/${r.rid}`))
    it('GET /api/ressource/:origine/:idOrigine', () => checkAll(r => `/api/ressource/${r.origine}/${r.idOrigine}`))
    it('GET /api/public/:oid', () => checkAll(r => `/api/public/${r.oid}`))
    it('GET /api/public/:baseId/:oid', () => checkAll(r => `/api/public/${r.rid}`))
    it('GET /api/public/:origine/:idOrigine', () => checkAll(r => `/api/public/${r.origine}/${r.idOrigine}`))
  })
})

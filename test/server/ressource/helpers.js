/**
 * Ce test crée une ressource puis la supprime, options possibles :
 * l'appeler directement en lui passant --prod ou --dev pour tester la sésathèque de prod ou dev,
 * (sinon c'est la bibli locale)
 * ou --token pour lui passer un token
 */

'use strict'
/* eslint-env mocha */
import { expect } from 'chai'
import { fakeOid } from '../../fixtures/fakePersonne'
import fakeRessource from '../../fixtures/fakeRessource'
import { purge } from '../populate'

import config from '../../../app/server/config'
const { application: { baseId: myBaseId } } = config

export default function helpersFactory (lassi, superTestClient) {
  const EntityRessource = lassi.service('EntityRessource')

  /**
   * Une fonction à passer à un promise.catch() pour purger avant de renvoyer l'erreur
   * @type {function}
   * @return {Promise} promesse rejetée
   */
  const purgeOnError = (error) => purge().then(() => Promise.reject(error))

  /**
   * Vérifie que ressourceExpected est bien en db
   * @param ressourceExpected
   * @return {Promise}
   */
  const checkDb = (ressourceExpected) => new Promise((resolve, reject) => {
    if (!ressourceExpected.oid) return reject(new Error('ressource attendue sans oid'))
    const { oid } = ressourceExpected
    EntityRessource.match('oid').equals(oid).grabOne((error, ressource) => {
      if (error) return reject(error)
      if (!ressource) return reject(new Error(`aucune resssource ${oid} en base`))
      // console.log('ress db', ressource.relations, ressource.relationsTitres)
      // Object.keys(ressourceExpected).forEach(p => expect(ressource[p]).to.deep.equals(ressourceExpected[p], `pb avec ${p} pour ${oid} : ${JSON.stringify(ressource[p])} ≠ ${JSON.stringify(ressourceExpected[p])}`))
      for (const [p, expected] of Object.entries(ressourceExpected)) {
        const got = ressource[p]
        const msg = `Pb avec ${p} sur ${oid} en db`
        if (['commentaires', 'description', 'resume'].includes(p)) {
          expect(got.replace(/\\\\n/g, '\n')).to.equals(expected, msg)
        } else if (typeof got === 'object') {
          expect(got).to.deep.equals(expected, msg)
        } else {
          expect(got).to.equals(expected, msg)
        }
      }
      resolve()
    })
  })

  /**
   * Appelle url en get et vérifie qu'on récupère expected
   * @param url
   * @param expected
   * @return {Promise}
   */
  const checkHttp = (url, expected) => checkHttpResult(superTestClient.get(url), expected)

  /**
   * Vérifie qu'un supertestClient ayant envoyé sa requête récupère bien la ressource attendue
   * @param stc
   * @param expected
   * @return {Promise}
   */
  const checkHttpResult = (stc, expected) => stc
    .expect(200)
    .expect('Content-type', /application\/json/)
    .then(({ body: { message, data: ressource } }) => {
      expect(message).to.equal('OK')
      cleanVolatileProperties(expected)
      expect(ressource).to.have.property('oid', expected.oid)
      expect(ressource).not.to.have.property('error')
      expect(ressource).not.to.have.property('errors')
      expect(ressource).not.to.have.property('warnings')
      for (const [k, v] of Object.entries(expected)) {
        // if (expected[k] instanceof Date) expect(ressource[k]).to.equals(expected[k].toISOString(), `pb sur propriété ${k}`)
        // else expect(ressource[k]).to.deep.equal(expected[k], `pb sur propriété ${k}`)
        expect(JSON.stringify(v)).to.equal(JSON.stringify(expected[k]), `pb sur propriété ${k}`)
      }
    })

  /**
   * Nettoie une entity pour en faire un objet presque plain sans ses propriétés volatiles
   * ($original ou dateMiseAJour) ou susceptible d'être modifiées par un post (typeDoc & typePeda)
   * @param {EntityRessource} ressource
   * @return {Ressource}
   */
  const cleanVolatileProperties = (ressource) => {
    // typeDoc et typePeda peuvent avoir été re-générés par le post, on les vérifie pas
    delete ressource.typeDocumentaires
    delete ressource.typePedagogiques
    // pour dateMiseAJour idem, c'est normal que ça change
    delete ressource.dateMiseAJour
    // on vire aussi toutes les méthodes et propriétés $
    Object.keys(ressource).forEach(p => {
      if (/^\$/.test(p) || typeof ressource[p] === 'function') delete ressource[p]
    })
    return ressource
  }

  /**
   * arbre public avec oid et rid
   * @return {Ressource}
   */
  const getArbrePublic = () => fakeRessource({ type: 'arbre' })

  /**
   * ressource publique avec oid et rid
   * @return {Ressource}
   */
  const getRessPublic = () => fakeRessource()

  /**
   * ressource publique sans oid ni rid
   * @return {Ressource}
   */
  const getRessPublicAnonymous = () => fakeRessource({
    nooid: true,
    norid: true,
    // on met une baseId connue, mais pas la notre sinon l'api va virer ces relations vers des ressources qui n'existent pas chez elle
    relations: [[1, 'sesabibli/1'], [14, 'sesabibli/2']],
    relationsTitres: { 'sesabibli/1': 'titre1', 'sesabibli/2': 'titre2' }
  })

  /**
   * ressource publique sans oid avec rid
   * @return {Ressource}
   */
  const getRessPublicSsOid = () => fakeRessource({
    nooid: true,
    rid: `${myBaseId}/${fakeOid()}`
  })

  /**
   * ressource privée sans oid ni rid
   * @return {Ressource}
   */
  const getRessPrivateAnonymous = () => fakeRessource({
    nooid: true,
    norid: true,
    restriction: 1,
    // on met une baseId connue, mais pas la notre sinon l'api va virer ces relations vers des ressources qui n'existent pas chez elle
    relations: [[1, 'sesabibli/1'], [14, 'sesabibli/2']],
    relationsTitres: { 'sesabibli/1': 'titre1', 'sesabibli/2': 'titre2' }
  })

  /**
   * ressource privée sans oid ni rid
   * @return {Ressource}
   */
  const getRessUnpublishedAnonymous = () => fakeRessource({
    nooid: true,
    norid: true,
    publie: false,
    // on met une baseId connue, mais pas la notre sinon l'api va virer ces relations vers des ressources qui n'existent pas chez elle
    relations: [[1, 'sesabibli/1'], [14, 'sesabibli/2']],
    relationsTitres: { 'sesabibli/1': 'titre1', 'sesabibli/2': 'titre2' }
  })

  /**
   * Retourne un assortiment de ressources
   * @return {Ressource[]}
   */
  const getTestRessources = () => [
    getRessPublicAnonymous(),
    getRessPublicSsOid(),
    getRessPublic(),
    getArbrePublic(),
    getRessPrivateAnonymous(),
    getRessUnpublishedAnonymous()
  ]

  return {
    purgeOnError,
    checkDb,
    checkHttp,
    checkHttpResult,
    cleanVolatileProperties,
    getArbrePublic,
    getRessPublic,
    getRessPublicAnonymous,
    getRessPrivateAnonymous,
    getRessUnpublishedAnonymous,
    getTestRessources
  }
}

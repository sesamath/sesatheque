/**
 * Test qui lance l'appli sur un autre port avec une config de test (d'après la conf _private/test.js)
 * S'il y a de nouveaux updates depuis le dernier lancement avec cette conf, relancer l'appli avec
 *    env SESATHEQUE_CONF=test node --stack_trace_limit=100 --stack-size=2048 app/index.js
 * avant de relancer les tests (pour appliquer les updates sur la base de test)
 */

'use strict'
/* eslint-env mocha */

// Ce jeu de test de l'appli est lancé avec les paramètres de _private/test.js
// (c'est app/config.js qui détecte mocha et utilise ce fichier)

import app from '../app/server'
import config from '../app/server/config'
import anLog from 'an-log'
// si on a pas de link vers le module on peut pas aller dans src
import sesatheques from 'sesatheque-client/dist/server/sesatheques'
import log from 'sesajstools/utils/log'

/**
 * client supertest
 * @see https://github.com/visionmedia/supertest
 * @typedef supertestClient
 * @type {object}
 */
/**
 * Agent supertest
 * @see https://visionmedia.github.io/superagent/
 * @typedef supertestAgent
 * @type {object}
 */
import supertest from 'supertest'
const { addSesatheque } = sesatheques

log.setLogLevel('error')

let isBooted = false

// pour que mocha rende la main, il faut shutdown lassi
// on gère ça avec un timeout après chaque boot, et un
// testsDone qui raccourci ce timeout (qui sera réinitialisé
// si un autre describe appelle boot juste après)

/**
 * Objet de la promesse résolue après le boot
 * Chaque describe devra appeler le boot en before et testsDone en after
 * @type {{lassi, superTestClient, supertestAgent, testsDone}}
 */
let resolvedValue = {}
let timerId, bootPromise

const resetTimer = (delay) => {
  if (timerId) clearTimeout(timerId)
  timerId = setTimeout(shutdown, delay)
}

const shutdown = (done) => {
  // console.log('launch shutdown')
  if (!resolvedValue.lassi) {
    if (!resolvedValue.shutdownAsked) throw new Error('impossible de fermer l’appli avant de l’avoir démarrée')
    // on avait déjà été appelé, on arrête là silencieusement
    return
  }
  resolvedValue.lassi.on('shutdown', () => {
    // console.log('shutdown done')
    bootPromise = null
    resolvedValue = { shutdownAsked: true }
    if (done) done()
  })
  resolvedValue.lassi.shutdown()
}

/**
 * Démarre l'appli et résoud avec un objet {lassi, superTestClient, superTestAgent, testsDone}
 * @return {Promise} qui sera résolue en passant un objet {lassi, superTestClient, superTestAgent, testsDone}
 */
export default function boot () {
  // à chaque appel de boot on supprime l'éventuel timer sur un shutdown mis par la fin du test précédent
  if (timerId) {
    clearTimeout(timerId)
    timerId = null
  }
  if (!bootPromise) {
    bootPromise = new Promise((resolve) => {
      const finish = () => {
        // on supprime le timer de shutdown
        // Il faut un shutdown pour que mocha rende la main, même si on l'appelle sur un seul fichier
        // mais on veut pas de boot & shutdown à chaque test, trop long, donc à chaque fin de test
        if (timerId) clearTimeout(timerId)
        if (resolvedValue.shutdownAsked) delete resolvedValue.shutdownAsked
        return resolve(resolvedValue)
      }
      // sera appelé sur l'événement startup de lassi
      const afterBootCallback = () => {
        if (!resolvedValue.lassi) throw new Error('Il y a eu un pb dans la méthode boot, lassi n’est pas disponible')
        /** @type supertestClient */
        resolvedValue.superTestClient = supertest(resolvedValue.lassi.express)
        /** @type supertestAgent */
        resolvedValue.superTestAgent = supertest.agent(resolvedValue.lassi.express)
        // à mettre en after pour lancer un shutdown si personne ne demande de boot dans les 100ms qui suivent
        resolvedValue.testsDone = () => resetTimer(100)
        isBooted = true
        finish()
      }

      if (isBooted) return finish()

      // si le boot a démarré mais que l'événement startup n'est pas encore arrivé,
      // (afterBootCallback pas encore appelée), faut l'attendre
      if (resolvedValue.lassi) {
        log('boot already started, waiting for startup event')
        resolvedValue.lassi.on('startup', finish)
        return
      }

      // on enregistre notre sesatheque de test
      addSesatheque(config.application.baseId, config.application.baseUrl)
      // on configure an-log d'après la conf
      // les logs de l'appli sont dans le dossier configuré dans _private/test.js
      // possibilité de modifier le logLevel là-bas
      anLog.config(config.lassiLogger)
      // boot
      app({ noCheckLocalOnRemote: true, test: true }, afterBootCallback).then((lassiInstance) => {
        // le boot retourne lassi en synchrone et afterBootCallback est appelée sur l'event startup
        // donc on est dans le then mais afterBootCallback n'a pas encore été appelée

        // anLog est pénible…
        anLog.config(config.lassiLogger)
        resolvedValue.lassi = lassiInstance
        // finish sera appelé dans afterBootCallback
      }).catch(log.error)
    })
  }
  return bootPromise
}

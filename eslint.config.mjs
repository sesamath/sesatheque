// Cf https://eslint.org/docs/latest/use/configure/
// et https://eslint.org/docs/latest/use/configure/migration-guide

// pour forcer les imports sur une seule ligne (pour rechercher avec regex tous les fichiers qui utilisent une fct)
// cf https://github.com/SeinopSys/eslint-plugin-import-newlines
import importNewlines from 'eslint-plugin-import-newlines'
import react from 'eslint-plugin-react'
import globals from 'globals'
import neostandard from 'neostandard'

export default [
  {
    settings: {
      react: {
        version: '16.6.3',
      }
    }
  },
  {
    plugins: { importNewlines },
    rules: {
      // https://github.com/SeinopSys/eslint-plugin-import-newlines#readme
      'importNewlines/enforce': [
        'error',
        {
          items: 1000,
          forceSingleLine: true,
          semi: false
        }
      ]
    },
  },

  // la conf standard, cf https://github.com/neostandard/neostandard?tab=readme-ov-file#configuration-options
  ...neostandard({
    globals: {
      ...globals.browser,
    },
    ignores: [
      '**/node_modules',
      'app/assets',
      '_private',
      'build',
      'build.*',
      'doc_src',
      'documentation'
    ]
  }),

  // react
  react.configs.flat.recommended,

  // options communes
  {
    files: ['**/*.js'],
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
    },
    // avec qq ajouts
    rules: {
      // un alert() doit lancer une erreur
      'no-alert': 'error',
      // et un console.log aussi (on autorise console.error, warn, …)
      'no-console': ['error', { allow: ['error', 'warn', 'info', 'debug'] }]
    }
  },

  // fichiers en es5
  {
    files: [
      'app/client*/**/*.preLoad.js'
    ],
    languageOptions: {
      ecmaVersion: 5,
      sourceType: 'script'
    },
    rules: {
      'no-var': 'off',
      'prefer-const': 'off'
    }
  },

  // la partie node
  {
    files: ['app/server/**/*.js', 'scripts*/**/*.js', 'webpack*.js'],
    languageOptions: {
      globals: {
        ...globals.node,
        lassi: true,
        log: true
      }
    },
    rules: {
      'no-console': 'off'
    }
  },

  // les tests
  {
    files: ['test/**/*.js'],
    rules: {
      // ça c’est pour les trucs du genre `expect(value).to.be.true`
      'no-unused-expressions': 'off',
      'no-console': 'off'
    },
    languageOptions: {
      globals: {
        $$: true,
        browser: true,
        ...globals.chai,
        ...globals.mocha,
        ...globals.node,
        sinon: true,
        lassi: true,
      }
    },
  }
]

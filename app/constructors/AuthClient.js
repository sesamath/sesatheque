'use strict'

/**
 * Ce fichier ne sert pas directement dans sesatheque, il est là pour servir de modèle à l'implémentation d'un
 * client d'authentification (et sert aussi pour jsdoc ou l'autocomplétion)
 */

/**
 * Un modèle pour un client SSO qui devra s'inscrire dans la sésathèque via $auth.addClient(authClient)
 * @constructor
 */
function AuthClient () {
  /**
   * La baseId du serveur d'authentification
   * @type {string}
   */
  this.baseId = undefined

  /**
   * Un peu de texte qui sera affiché à l'utilisateur pour lui permettre de choisir son serveur d'authentification si plusieurs sont enregistrés
   * @type {string}
   */
  this.description = undefined
}

/**
 * Pour fournir au client des méthodes de l'appli
 * (appelé à l'enregistrement si ça existe, sinon il s'est débrouillé autrement)
 * @param {{loginCallback, logoutCallback, errorCallback, authServer, prefix}} props
 */
AuthClient.prototype.init = function (props) {

}

/**
 * Renvoie les liens à mettre dans le panneau authentifié d'une personne loggée chez nous
 * @param {string} userId
 * @returns {Link[]} La liste de liens
 */
AuthClient.prototype.getSsoLinks = function (userId) {

}

/**
 * Redirige vers le serveur d'authentification qui devra rappeler urlValidate en GET,
 * en ajoutant des paramètres de son choix que validate devra traiter
 * @param {Context} context
 * @param {string}  urlValidate L'url que le serveur d'authentification devra rappeler après authentification
 * @param {string}  urlLogout   L'url que le serveur d'authentification pourra rappeler si l'utilisateur s'est déconnecté du serveur
 *                              (pour propager le logout ici, sesatheque rappellera alors la méthode logoutFromRemote)
 */
AuthClient.prototype.login = function (context, urlValidate, urlLogout) {

}

/**
 * Redirige vers la déconnexion du serveur d'authentification (elle est déjà faite dans sesatheque)
 * @param {Context} context
 */
AuthClient.prototype.logout = function (context) {

}

module.exports = AuthClient

// cf webpackConfigLoader.js pour les valeurs exportées à un browser
import { baseId, baseUrl, sesatheques } from 'server/config'
import { addSesatheque } from 'sesatheque-client/src/sesatheques'

// ce fichier est compilé par webpack isolément, et ajouté via un tag <script> par sesatheque-plugin-j3p:public/editgraphe.html
// il est aussi importé par app/client-react/components/InitApp.js

// addSesatheque peut planter, on veut pas bloquer le client pour ça
try {
  // init de cette sesatheque et des autres pour sesatheque-client
  addSesatheque(baseId, baseUrl)
  if (sesatheques.length) {
    for (const { baseId, baseUrl } of sesatheques) {
      addSesatheque(baseId, baseUrl)
    }
  }
} catch (error) {
  if (typeof window.bugsnagClient?.notify === 'function') window.bugsnagClient.notify(error)
  else console.error(error)
}

const { pluginsPaths } = require('./plugins')
const { application: { baseUrl }, version } = require('../server/config')

const params = { baseUrl, version }

const allEntries = {}
const allPlugins = []
const allRules = []

pluginsPaths.forEach(plugin => {
  const { entries, plugins, rules } = require(`${plugin}/webpack.config`)(params)
  // console.log('traitement plugin', plugin, 'donne', JSON.stringify({ entries, plugins, rules }, null, 2))
  Object.assign(allEntries, entries)
  allPlugins.push(...plugins)
  allRules.push(...rules)
})

module.exports = {
  entries: allEntries,
  plugins: allPlugins,
  rules: allRules
}

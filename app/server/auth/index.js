'use strict'
module.exports = function authComponentFactory (lassi) {
  const authComponent = lassi.component('auth')
  require('./serviceAuth')(authComponent)
  require('./controllerAuth')(authComponent)
  require('./controllerApi')(authComponent)
}

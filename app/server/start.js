'use strict'

// pour utiliser babel avant node en gardant la possibilité d'utiliser pm2 en cluster,
// il faut décommenter ces requires
// cf http://stackoverflow.com/questions/35436266/how-can-i-use-babel-6-with-pm2-1-0
// et https://babeljs.io/docs/usage/require/ qui indique qu'il faut ajouter babel-polyfill
//
// require('babel-register')
// require('babel-polyfill')

const app = require('./index')
app().catch((error) => {
  console.error('Boot KO', error)
})

'use strict'

const { addSesatheque, getBaseUrl } = require('sesatheque-client/dist/server/sesatheques')
// Cette fonction ne peut pas être dans le module checkConfig, ça causerait des dépendances cycliques
// checkConfig => config => checkConfigSesatheques (ce module)

/**
 * Vérifie les couples baseId/baseUrl fournis et retourne une liste d'erreurs (vide si y'en a pas)
 * @param {Object[]} sesatheques
 * @return {string[]} la liste des erreurs
 * @private
 */
module.exports = function checkConfigSesatheques (sesatheques, addIfUnknown = false) {
  const errors = []
  if (!Array.isArray(sesatheques)) throw new Error('sesatheques doit être un Array')
  if (!sesatheques.length) throw new Error('sesatheques est vide')
  for (const [index, { baseId, baseUrl }] of Object.entries(sesatheques)) {
    if (!baseId) errors.push(`La sésathèque ${index} n’a pas de baseId`)
    if (!baseUrl) errors.push(`La sésathèque ${index} n’a pas de baseUrl`)
    const knownBaseUrl = getBaseUrl(baseId, false)
    if (knownBaseUrl) {
      if (baseUrl !== knownBaseUrl) errors.push(`La sésathèque ${baseId} a la baseUrl ${baseUrl} mais devrait avoir ${knownBaseUrl}`)
    } else if (addIfUnknown) {
      // à priori on a déjà tout vérifié mais au cas où on ajoute un try/catch pour récupérer l'erreur
      try {
        addSesatheque(baseId, baseUrl)
      } catch (error) {
        console.error(error)
        errors.push(error.message)
      }
    } else {
      errors.push(`La sésathèque ${baseId} est inconnue`)
    }
  }
  return errors
}

/**
 * Ce script remplace un pid par un autre
 * Utilisé lorsqu'un compte a été supprimé (pour inactivité par ex) puis recrée.
 * Ça arrive avec un prof ayant eu un compte Sésaprof puis ayant créé un compte ENT
 * sans le rapprocher avec son compte Sésaprof, puis compte Sésaprof supprimé.
 */
'use strict'

const flow = require('an-flow')

const actions = ['testTo', 'changeTo']

function changePid (oldPid, action, newPid, done) {
  /**
   * Cherche les users old & new et les traite
   * @param next
   */
  function findUsers (next) {
    flow().seq(function () {
      $personneRepository.load(oldPid, this)
    }).seq(function (_oldUser) {
      if (_oldUser) {
        oldUser = _oldUser
        console.log(`L’ancien user ${oldPid} est ${oldUser.oid}`)
      } else {
        console.log(`L’ancien user ${oldPid} n’existe plus`)
      }
      $personneRepository.load(newPid, this)
    }).seq(function (_newUser) {
      if (_newUser) {
        newUser = _newUser
        console.log(`Le nouveau user ${newPid} est ${newUser.oid}`)
      } else {
        console.log(`Le nouveau user ${newPid} n’existe pas`)
      }

      // on vérifie qu'on a bien au moins un user
      if (!oldUser && !newUser) {
        return done(Error(`Aucun des utilisateurs ${oldPid} et ${newPid} n’existe, ABANDON`))
      }
      next()
    }).catch(next)
  }

  function grabRess (next) {
    flow().seq(function () {
      // ça marche aussi…
      // $ressourceRepository.grabSearch({ iPids: [oldPid] }, { limit, skip: offset }, this)
      EntityRessource.match('auteurs').equals(oldPid).grab({ limit, offset }, this)
    }).seqEach(function (ress) {
      if (dryRun) printOneRess(ress, this)
      else changeOneRess(ress, this)
    }).seq(function (results) {
      if (results.length === limit) {
        if (dryRun) offset += limit
        // sinon faut pas incrémenter car on a modifié l'auteur,
        // donc le prochain tour prendra les suivantes à modifier à l'offset 0
        process.nextTick(() => grabRess(next))
      } else {
        const plural = nbRess > 1 ? 's' : ''
        let msg = `${nbRess} ressource${plural} contenant ${oldPid}`
        if (action === 'changeTo') msg += ` ont été modifiée${plural} pour y mettre ${newPid} à la place`
        console.log(msg)
        next()
      }
    }).catch(next)
  } // grabRess

  function printOneRess (ress, next) {
    const { oid, type, titre, auteurs, auteursParents, contributeurs } = ress
    let msg = `Ressource ${oid} "${titre}" : type ${type}`
    if (auteurs?.length) msg += `, auteurs ${auteurs.join(', ')}`
    if (contributeurs?.length) msg += `, contributeurs ${contributeurs.join(', ')}`
    if (auteursParents?.length) msg += `, auteursParents ${auteursParents.join(', ')}`
    console.log(msg)
    nbRess++
    next()
  }

  function changeOneRess (ress, next) {
    flow().seq(function () {
      const replacer = (pid) => pid === oldPid ? newPid : pid
      ress.auteurs = ress.auteurs.map(replacer)
      ress.auteursParents = ress.auteursParents.map(replacer)
      ress.contributeurs = ress.contributeurs.map(replacer)
      $ressourceRepository.save(ress, this)
    }).seq(function () {
      console.log(`Ressource ${ress.oid} modifiée (${ress.titre})`)
      nbRess++
      next()
    }).catch(error => {
      console.error(`La modification de la ressource ${ress.oid} a échoué (${ress.titre})`, error)
      next()
    })
  }

  function changeUsers (next) {
    // faut modifier les users
    if (oldUser) {
      if (newUser) {
        // il faut aller éventuellement modifier tout ce qui contient un oid de user
        // ça ne concerne que les gestionnaires des groupes
        changeGroups(next)
      } else {
        // on remplace le pid dans l'objet et le user changera pas d'oid
        oldUser.pid = newPid
        console.log(`Le user ${oldUser.oid} qui avait le pid ${oldPid} a désormais le pid ${newPid}`)
        $personneRepository.save(oldUser, next)
      }
    } else {
      console.warn(`L’ancien user ${oldPid} n’existe plus, on ne connaît donc pas son oid => impossible de mettre à jour les gestionnaires de groupe`)
      next()
    }
  }

  function changeGroups (next) {
    if (!oldUser || !newUser) return next(Error('Erreur interne, user perdu'))
    let nbGroups = 0
    flow().seq(function () {
      $groupeRepository.fetchListManagedBy(oldUser.oid, this)
    }).seqEach(function (groupe) {
      nbGroups++
      console.log(`Groupe ${groupe.nom} ${groupe.oid} modifié (gestionnaire ${oldUser.oid} => ${newUser.oid})`)
      groupe.gestionnaires = groupe.gestionnaires.map(oid => oid === oldUser.oid ? newUser.oid : oid)
      $groupeRepository.save(groupe, this)
    }).seq(function () {
      console.log(`${nbGroups} modifié${nbGroups > 1 ? 's' : 's'}`)
      next()
    }).catch(next)
  }

  function delOldIfNeeded (next) {
    if (!dryRun && oldUser && newUser) {
      $personneRepository.delete(oldUser, next)
    } else {
      next()
    }
  }

  // check préalable & init
  if (typeof done !== 'function') {
    const realDone = typeof oldPid === 'function'
      ? oldPid
      : typeof action === 'function'
        ? action
        : typeof newPid === 'function'
          ? newPid
          : null
    if (realDone) {
      changePid.help()
      return realDone(Error('Il manque des arguments'))
    }
    throw Error('aucune callback fournie')
  }

  let errMsg
  if (!actions.includes(action)) errMsg = `action ${action} invalide (doit être ${actions.join(' ou ')})`
  else if (!/^(labomep|labomepdev|sesasso|sesassodev)\//.test(oldPid)) errMsg = `oldPid doit commencer par labomep/ ou labomepdev/ ou sesasso/ ou sesassodevsesasso/ (${oldPid} fourni)`
  else if (!/^(labomep|labomepdev|sesasso|sesassodev)\//.test(newPid)) errMsg = `newPid doit commencer par labomep/ ou labomepdev/ ou sesasso/ ou sesassodevsesasso/ (${newPid} fourni)`
  if (errMsg) {
    changePid.help()
    return done(Error(errMsg))
  }

  // on préfère tester changeTo pour que ce soit test par défaut
  const dryRun = !(action === 'changeTo')
  let offset = 0
  const limit = 50
  let nbRess = 0
  let oldUser, newUser
  const $ressourceRepository = lassi.service('$ressourceRepository')
  const EntityRessource = lassi.service('EntityRessource')
  const $personneRepository = lassi.service('$personneRepository')
  const $groupeRepository = lassi.service('$groupeRepository')

  // go
  flow().seq(function () {
    findUsers(this)
  }).seq(function () {
    grabRess(this)
  }).seq(function () {
    if (dryRun) return done()
    // faut continuer et appliquer les modifs aux users / groups
    changeUsers(this)
  }).seq(function () {
    delOldIfNeeded(this)
  }).empty()
    .done(done)
}

changePid.help = function changePidHelp () {
  console.log(`La commande changePid remplace dans toutes les ressources un pid par un autre. Elle prend 3 arguments, ATTENTION à l'ordre :
{oldPid}   : le pid à remplacer
testTo|changeTo   : pour voir ce qui serait modifié ou le faire
{newPid} : le pid qui doit remplacer {olPid}`)
}

module.exports = {
  changePid
}

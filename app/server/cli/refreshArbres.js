'use strict'
const flow = require('an-flow')
const { stringify } = require('sesajstools')
const { getBaseIdFromRid } = require('sesatheque-client/dist/server/sesatheques')
const taskLog = require('an-log')('sesatheque-cli')

const appConfig = require('../config')
const configRessource = require('../ressource/config')

const { application: { baseId } } = appConfig
const { constantes: { relations: { contientRefA, estRefPar, estRemplacePar } } } = configRessource

const refreshed = new Set()

function logErrorInDataAndTask (message, obj) {
  taskLog.error(message)
  log.dataError(message, obj)
}

/**
 * Rafraichit les datas d'un arbre (ou tous si on ne précise pas d'oid)
 * @param {string} [oid]
 * @param {errorCallback} done
 */
function refreshArbres (oid, done) {
  function grab (offset, next) {
    let nb = 0
    flow().seq(function () {
      taskLog(`traitement des arbres de ${offset} à ${offset + limit} sur ${nbArbres}`)
      // Attention, il faut un tri qui ne change pas l'ordre lorsque certains sont mis à jour
      EntityRessource.match('type').equals('arbre').sort('oid').grab({ limit, offset }, this)
    }).seqEach(function (arbre) {
      nb++
      refreshOne(arbre, this)
    }).seq(function () {
      if (nb === limit) {
        grab(offset + limit, next)
      } else {
        next()
      }
    }).catch(next)
  } // grab

  /**
   * Met éventuellement à jour les titres des enfants de item
   * @private
   * @param {Ressource} parent l'arbre courant
   * @param {Ref} branche la branche à vérifier
   * @param next appelé avec (error, itemClean, hasChanged)
   */
  function cleanEnfants (parent, branche, next) {
    if (parent.aliasOf) {
      // il faut aller chercher le vrai
      $ressourceFetch.fetchOriginal(parent.aliasOf)
        .then(ressource => {
          if (refreshed.has(ressource.rid)) {
            return next(null, ressource, false)
          }
          // on se rappelle avec l'arbre original
          cleanEnfants(ressource, branche, next)
        })
        .catch(next)
      return
    }
    if (!parent.oid || parent.type !== 'arbre') return next(Error(`parent invalide : ${stringify(parent)}`))
    if (branche.enfants && branche.enfants.length) {
      // boucle sur les enfants
      let hasChanged = false
      flow(branche.enfants).seqEach(function (enfant) {
        const nextEnfant = this
        if (!enfant) {
          log.dataError(`enfant invalide dans ${branche && (branche.rid || branche.aliasOf || branche.titre)}`, branche)
          return this()
        }
        process.nextTick(cleanEnfant, parent, enfant, function (error, enfantCleaned, enfantsHadChanges) {
          if (error) return nextEnfant(error)
          if (enfantsHadChanges) hasChanged = true
          nextEnfant(null, enfantCleaned)
        })
      }).seq(function (enfants) {
        branche.enfants = enfants.filter(e => e)
        next(null, branche, hasChanged)
      }).catch(next)
    } else {
      next(null, branche, false)
    }
  }

  /**
   * Vérifie s'il faut modifier ref
   * @private
   * @param {Ressource} parent l'arbre parent
   * @param {Enfant} enfant la feuille de l'arbre à vérifier
   * @param next appelé avec (error, ref, hasChanged)
   */
  function cleanEnfant (parent, enfant, next) {
    let hasChanged = false
    if (!parent.relationsTitres) parent.relationsTitres = {}

    // cas alias en premier, on fetch et compare
    if (enfant.aliasOf) {
      nbRessources++
      $ressourceFetch.fetchOriginal(enfant.aliasOf)
        .then((ressource) => {
          const changes = [] // pour le log

          // on a une ressource
          flow().seq(function () {
            const nextStep = this
            if (!ressource.relations) return nextStep()
            // on regarde si elle n'est pas obsolète
            const replacedByRelation = ressource.relations.find(([type]) => type === estRemplacePar)
            if (!replacedByRelation) return nextStep()
            // y'a un remplaçant désigné, on le cherche
            $ressourceFetch.fetchOriginal(replacedByRelation[1])
              .then(ress => {
                if (ress) ressource = ress
                // sinon on continue avec la ressource initiale
                nextStep()
              })
              .catch(next)
          }).seq(function () {
          // relations du parent
            const existingRelationInParent = parent.relations.find(([typeRelation, rid]) => typeRelation === contientRefA && rid === ressource.rid)
            if (!existingRelationInParent) {
              hasChanged = true
              changes.push('relations')
              parent.relations.push([contientRefA, ressource.rid])
            }
            if (!parent.relationsTitres[ressource.rid]) {
              parent.relationsTitres[ressource.rid] = ressource.titre
              hasChanged = true
            }
            // relations de la ressource
            const existingRelationInRess = ressource.relations.find(([typeRelation, rid]) => typeRelation === estRefPar && rid === parent.rid)
            const ressHasTreeTitle = parent.rid in ressource.relationsTitres
            if (existingRelationInRess && ressHasTreeTitle) return this()
            // faut màj ressource
            if (!existingRelationInRess) {
              taskLog.debug(`Ajout de la relation estRefPar (${parent.rid}) ajoutée sur la ressource (${ressource.rid})`)
              ressource.relations.push([estRefPar, parent.rid])
            }
            if (!ressHasTreeTitle) {
              ressource.relationsTitres[parent.rid] = parent.titre
            }
            const ressBaseId = getBaseIdFromRid(ressource.rid)
            if (ressBaseId === baseId) {
              $ressourceRepository.save(ressource, this)
            } else {
              $ressourceRemote.update(ressource)
                .then(() => this())
                .catch(this)
            }
          }).seq(function () {
            // check si la ressource a changé de titre/résumé/commentaires
            for (const field of ['titre', 'resume', 'commentaires']) {
              if (enfant[field] !== ressource[field]) {
                hasChanged = true
                enfant[field] = ressource[field]
                changes.push(field)
              }
            }
            // public ?
            const isPublic = ressource.publie && !ressource.restriction
            if (enfant.public !== isPublic) {
              enfant.public = isPublic
              hasChanged = true
              changes.push('public')
            }
            if (enfant.type === 'arbre' && enfant.enfants && enfant.enfants.length) {
              logErrorInDataAndTask(`item arbre avec aliasOf ${enfant.aliasOf} et enfants, on vire les enfants pour rendre le chargement dynamique, dans l’arbre ${currentOid} (${currentTitre})`)
              hasChanged = true
              delete enfant.enfants
            }
            if (changes.length) taskLog.debug(`DEBUG : l'enfant ${ressource.rid} a eu des modifs (${changes.join(', ')})`)
            next(null, enfant, hasChanged)
          }).catch(next)
        })
        .catch((error) => {
          // si c'est une 404 on veut continuer
          if (/(Aucune ressource|existe pas)/.test(error.message)) {
            logErrorInDataAndTask(`la ressource ${enfant.aliasOf} n’existe plus dans l’arbre ${currentOid} (${currentTitre})`)
            enfant.titre = `Cette ressource n’existe plus (${enfant.titre})`
            enfant.type = 'error'
            hasChanged = true
            next(null, enfant, hasChanged)
          } else {
            next(error)
          }
        })

    // error, on fait suivre tel quel
    } else if (enfant.type === 'error') {
      next(null, enfant, hasChanged)

    // dossier
    } else if (enfant.type === 'arbre') {
      // on nettoie les enfants
      if (enfant.enfants && enfant.enfants.length) {
        // si c'est un alias il devient parent
        if (enfant.aliasOf) parent = enfant
        process.nextTick(cleanEnfants, parent, enfant, function (error, refCleaned, needSaveRef) {
          if (error) return next(error)
          if (needSaveRef) hasChanged = true
          next(null, refCleaned, hasChanged)
        })
      // si vide, on fait suivre tel quel
      } else {
        next(null, enfant, hasChanged)
      }

    // cas inconnu, on laisse en le signalant
    } else {
      logErrorInDataAndTask(new Error('enfant sans aliasOf, ni error ni arbre'), enfant)
      next(null, enfant, hasChanged)
    }
  }

  /**
   * Parcours tous les enfants de l'arbre pour éventuellement mettre à jour le titre
   * @private
   * @param arbre
   * @param next
   */
  function refreshOne (arbre, next) {
    if (refreshed.has(arbre.rid)) {
      // rien à faire
      return next()
    }
    currentOid = arbre.oid
    currentTitre = arbre.titre
    if (arbre.enfants && arbre.enfants.length) {
      taskLog.debug(`DEBUG : starting refresh arbre ${currentOid} (${arbre.titre})`)
      process.nextTick(function () {
        cleanEnfants(arbre, arbre, function (error, arbreCleaned, needSave) {
          if (error) return next(error)
          if (needSave) {
            nbArbresModif++
            taskLog.debug(`DEBUG saving arbre ${currentOid} (${arbre.titre})`)
            $ressourceRepository.save(arbreCleaned, (error, arbreSaved) => {
              if (error) return next(error)
              refreshed.add(arbreSaved.rid)
              next(null, arbreSaved)
            })
          } else {
            taskLog.debug(`DEBUG arbre ${currentOid} (${arbre.titre}) non modifié`)
            refreshed.add(arbreCleaned.rid)
            next()
          }
        })
      })
    } else {
      log.dataError(`arbre ${arbre.oid} sans enfants (${arbre.titre})`)
      next()
    }
  }

  if (typeof oid === 'function') {
    done = oid
    oid = undefined
  }
  if (typeof done !== 'function') throw new Error('Erreur interne, pas de callback de commande')
  const limit = 10
  let nbArbres = 0
  let nbArbresModif = 0
  let nbRessources = 0
  let currentOid, currentTitre
  const EntityRessource = lassi.service('EntityRessource')
  const $ressourceRepository = lassi.service('$ressourceRepository')
  /** @type {$ressourceRemote} */
  const $ressourceRemote = lassi.service('$ressourceRemote')
  const $ressourceFetch = lassi.service('$ressourceFetch')

  if (oid) {
    // on rafraîchi un seul arbre
    flow().seq(function () {
      EntityRessource.match('oid').equals(oid).grabOne(this)
    }).seq(function (arbre) {
      if (!arbre) {
        taskLog.error(`L’arbre ${oid} n’existe pas`)
        return done()
      }
      taskLog(`Starting refreshArbres ${oid}`)
      refreshOne(arbre, this)
    }).seq(function () {
      taskLog(`fin du rafraichissement de l’arbre ${oid} (contenant ${nbRessources} ressources), il ${nbArbresModif ? 'a' : 'n’a pas'} été modifié.`)
      done()
    }).catch(done)
  } else {
    // faut tous les rafraîchir
    flow().seq(function () {
      EntityRessource.match('type').equals('arbre').count(this)
    }).seq(function (nb) {
      nbArbres = nb
      taskLog(`Starting refreshArbres avec ${nb} arbres`)
      grab(0, this)
    }).seq(function () {
      taskLog(`fin du rafraichissement de ${nbArbres} arbres (contenant ${nbRessources} ressources), dont ${nbArbresModif} modifiés`)
      done()
    }).catch(done)
  }
}

refreshArbres.help = function refreshArbresHelp () {
  console.info('La commande refreshArbres prend un oid en argument pour mettre à jour l’arbre, sans argument elle lance le rafraîchissement des données de tous les arbres')
}

module.exports = {
  refreshArbres
}

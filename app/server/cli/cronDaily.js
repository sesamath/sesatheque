const { promisify } = require('node:util')

const taskLog = require('an-log')('sesatheque-cli')
const { moodle } = require('../config.js')
const { refreshArbres } = require('./refreshArbres.js')
const { exportMoodle } = require('./exportMoodle.js')

/**
 * Génère les gifts et le html qui les liste
 * @param {errorCallback} done
 */
async function cronDaily (done) {
  if (typeof done !== 'function') throw Error('cronDaily ne prend pas d’arguments')
  const errors = []
  try {
    // avant le 2023-06-15 les tâches étaient lancées en parallèle (plus simple avec du Promise.allSettled)
    // mais c'est pas gentil pour mongo, on passe en séquentiel, sans s'arrêter sur la 1re erreur
    await promisify(refreshArbres)()
  } catch (error) {
    console.error(error)
    errors.push(error.message)
  }
  if (moodle?.staticExports?.length) {
    try {
      await promisify(exportMoodle)(moodle.staticExports.join(','))
    } catch (error) {
      console.error(error)
      errors.push(error.message)
    }
  }
  if (errors.length) {
    done(Error(`Les tâches du cronDaily ont généré ${errors.length} erreur${errors.length > 1 ? 's' : ''} : ${errors.join(' | ')}`))
  } else {
    done()
  }
}

cronDaily.help = function cronDailyHelp () {
  taskLog('La commande cronDaily ne prend pas d’argument, elle devrait être appelé quotidiennement. Elle exécute refreshArbres et exportMoodle avec l’éventuelle liste de rid de config.moodle.staticExports')
}

module.exports = {
  cronDaily
}

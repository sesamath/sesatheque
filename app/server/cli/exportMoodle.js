const { mkdir, writeFile } = require('node:fs/promises')
const { existsSync } = require('node:fs')
const { dirname, join } = require('node:path')

// ce truc marche pas non plus car il importe sesatheques qui a changé de format (nodeClient est cjs et sesatheques esm)
// const { fetchPublicRef } = require('sesatheque-client/src/nodeClient')

const taskLog = require('an-log')('sesatheque-cli')
const { application: { baseUrl }, moodle } = require('../config')
const { fetchPublicRef, exportRefAsGift } = require('../lib/moodle')

const outputDir = join(__dirname, '..', '..', '..', 'local', 'moodle')
const exportFile = join(outputDir, 'staticExports.json')

const getGiftFile = (rid) => join(outputDir, `${rid}.gift.txt`)

/**
 * Génère les gifts et le html qui les liste
 * @param {string} [rids]
 * @param {errorCallback} done
 */
async function exportMoodle (rids, done) {
  try {
    // check args
    if (typeof rids === 'function') {
      done = rids
      if (moodle.staticExports) {
        rids = moodle.staticExports.join(',') // un peu idiot de join pour split ensuite, mais ça simplifie le code
      } else {
        return done('Il faut passer des rid en argument (séparateur virgule) ou bien les configurer dans config.moodle.staticExports')
      }
    }
    if (typeof done !== 'function') {
      console.error('Il faut séparer les rids par des virgules, pas des espaces !')
      // on est sympa on continue quand même en rectifiant
      rids = Array.from(arguments)
      done = rids.pop()
      rids = rids.join(',')
    }
    // on génère tous les gifts
    const menu = new Map()
    for (const rid of rids.split(/[,;]/)) {
      try {
        if (!rid) continue
        const giftFile = getGiftFile(rid)
        // => ça donne du _private/moodle/sesabibli/xxx.gift.txt, faut créer le dossier s'il n'existe pas
        const giftDir = dirname(giftFile)
        if (!existsSync(giftDir)) await mkdir(giftDir, { recursive: true, mode: '755' })
        const ref = await fetchPublicRef(rid)
        menu.set(rid, ref.titre)
        const giftContent = await exportRefAsGift(ref)
        await writeFile(giftFile, giftContent)
      } catch (error) {
        console.error(`L’export de la ressource ${rid} a planté :`, error)
        // faut le virer du menu
        if (menu.has(rid)) menu.delete(rid)
      }
    } // for
    // reste à générer le menu dans _private/moodle/index.html
    await buildJson(menu)
    console.log(`Fin de l’export moodle des ressources :\n${Array.from(menu.entries()).map(([rid, titre]) => `${rid} : ${titre}`).join('\n')}`)
    console.log(`${exportFile} généré, cf ${baseUrl}moodle`)
    done()
  } catch (error) {
    done(error)
  }
}

async function buildJson (menu) {
  const liste = Array.from(menu.entries()).map(([rid, titre]) => ({ href: `/moodle/${rid}.gift.txt`, rid, titre }))
  await writeFile(exportFile, JSON.stringify({ liste }))
}

exportMoodle.help = function exportMoodleHelp () {
  taskLog('La commande exportMoodle génére les gifts de ressources choisies et la page html qui les liste (/moodle/index.html). Elle prend la liste de rid en argument (séparateur virgule) ou dans config.moodle.staticExports')
}

module.exports = {
  exportMoodle
}

/**
 * Ce script passe en revue les ressources pour vérifier que les relations sont cohérentes
 * Ajoute d'éventuels titres manquants, vire d'éventuelles relations existeAussi ou est RefPar (complément de estVersionDe/contientRefA) en trop, ajoute d'éventuelle relations contientRefA|estRefPar et signale le reste.
 * À lancer avec `node app/server/cli.js checkRelations`
 */
const taskLog = require('an-log')('sesatheque-cli')
const flow = require('an-flow')
const { application: { baseId } } = require('../config')
const ressConfig = require('../ressource/config')

const { listes, constantes: { relations: { existeAussi, estVersionDe, estPartieDe, estRefPar, contientRefA, estRequisPar, estUnFormatDe, existeDansFormat, estTraduitAvec, estTraductionDe, aPourCorrige, estLaCorrectionDe, remplace }, relationComplementaire } } = ressConfig

// les relations ajoutées d'office dans la ressource liée si ça lui manque
// (donc si la ressource analysée a le complémentaire)
const autoAddedInRel = [existeAussi, estRefPar, estRequisPar, remplace, estPartieDe, estUnFormatDe, existeDansFormat, estTraduitAvec, estTraductionDe, aPourCorrige, estLaCorrectionDe]
// les types de ressource qui sont des collections
const collType = ['arbre', 'serie', 'sequenceModele']

function checkRelations (oid, opt, done) {
  function logData (msg, data) {
    msg = msgPrefix + msg
    // il faut pas lui passer les nullish sinon il les dump
    if (data) taskLog(msg, data)
    else taskLog(msg)
    log.dataError(msg, data)
  }

  // récupère toutes les ressources ayant une relation
  function grabAll (next) {
    flow().seq(function () {
      const msg = `traitement des ressources de ${offset} à ${offset + limit} (sur ${nbRess} dont ${oidsMods.size} déjà modifiées` +
        (externalMods.size
          ? `, avec également ${externalMods.size} modifs sur la ou les sesatheques externes)`
          : ')'
        )
      taskLog(msg)
      EntityRessource.match('relations').isNotNull().sort('oid').grab({ limit, offset }, this)
    }).seqEach(function (ress) {
      checkOne(ress)
        .then(() => this())
        .catch(this)
    }).seq(function (results) {
      if (results.length === limit) {
        offset += limit
        // on laisse 300ms à mongo pour reprendre son souffle et gérer les affaires courantes
        setTimeout(() => grabAll(next), 300)
      } else {
        next()
      }
    }).catch(next)
  } // grabAll

  /**
   * Sauvegarde la ressource liée ress (suivant local/remote)
   * @private
   * @param ress
   * @returns {Promise<void>}
   */
  async function saveRelated (ress) {
    if (isDryRun) return
    taskLog(`sauvegarde de la ressource liée ${ress.rid}`)
    if (ress.rid.startsWith(baseId + '/')) {
      await saveRess(ress)
    } else {
      await $ressourceRemote.update(ress)
    }
    externalMods.add(ress.rid)
  }

  /**
   * Sauvegarde localement ress
   * @private
   * @param {Ressource} ress
   * @returns {Promise<void>}
   */
  function saveRess (ress) {
    const nbRel = ress.relations.length
    return new Promise((resolve, reject) => {
      $ressourceRepository.save(ress, (error, ressSaved) => {
        if (error) reject(error)
        oidsMods.add(ress.oid)
        // au cas où le save remettrait des relations, on le signale (on a p'tet oublié un truc ici)
        if (ressSaved.relations.length !== nbRel) logData(`le save de ${ress.oid} a changé le nb de relations de ${nbRel} à ${ressSaved.relations.length}`)
        resolve()
      })
    })
  }

  /**
   * Nettoie l'alias (fait juste un save qui s'en charge)
   * @param {Ressource} ress
   * @returns {Promise<void>}
   */
  function cleanAlias (ress) {
    logData(`sauvegarde de ${oid} alias de ${ress.aliasOf} nettoyé de ses relations`)
    return saveRess(ress)
  }

  /**
   * Propage un 404 vers un enfant (aliasOf viré et type transformé en error)
   * @private
   * @param {Ressource} ress
   * @param {string} rid
   */
  function forward404 (ress, rid) {
    if (ress.type === 'arbre') return forward404InChilds(ress.enfants, rid)
    if (ress.type === 'serie') return forward404InChilds(ress.parametres.serie, rid)
    if (ress.type === 'sequenceModele') {
      for (const sseq of ress.parametres.sousSequences) {
        forward404InChilds(sseq.serie, rid)
      }
    }
  }

  function forward404InChilds (enfants, rid) {
    for (const enfant of enfants) {
      if (enfant.aliasOf === rid) {
        delete enfant.aliasOf
        let titre = `Erreur : ${rid}`
        const typeLabel = listes[enfant.type]
        if (typeLabel) titre += ` de type ${typeLabel}`
        titre += ` n’existe pas ou plus (${enfant.titre})`
        enfant.titre = titre
        enfant.type = 'error'
      }
      if (enfant.enfants?.length) forward404InChilds(enfant.enfants, rid)
    }
  }

  /**
   * Vérifie toutes les relations de ress
   * @private
   * @param ress
   * @returns {Promise<void>}
   */
  async function checkOne (ress) {
    if (ress.aliasOf) return cleanAlias(ress)
    const { oid, relations, relationsTitres } = ress
    if (collType.includes(ress.type)) {
      $ressourceRepository.refreshRelationsContientRefA(ress)
    }
    if (!relations.length) return
    const relationsByRid = {}
    const ressByRid = {}
    let hasChanged = false
    // pour ne pas râler sur les titres manquants de rids qu'on a viré
    const ridsRemoved = new Set()

    // helper pour virer une relation
    let nbRelRemoved = 0
    const removeRel = (index) => {
      const realIndex = index - nbRelRemoved
      const [, rid] = relations[realIndex]
      ridsRemoved.add(rid)
      relations.splice(realIndex, 1)
      nbRelRemoved++
      hasChanged = true
    }

    // Pour la limitation à la dernière relation estVersionDe,
    // on ne peut pas mémoriser le dernier index dans la boucle principale,
    // car depuis qu'il aurait été mémorisé on aurait pu virer d'autre relations
    // (et removeRel ne retirerait pas la bonne)
    // => on compte d'abord le nb total de relations et on les vire toutes
    //    sauf la dernière dans la boucle principale
    const nbVersionDe = relations.reduce((nb, [type]) => type === estVersionDe ? nb + 1 : nb, 0)
    let nbVersionDeRemoved = 0

    // boucle sur un clone des relations car relations peut changer dans la boucle
    const originalRelations = [...relations]
    for (const [relIndex, [type, rid]] of originalRelations.entries()) {
      if (type === estVersionDe) {
        if (nbVersionDeRemoved < nbVersionDe - 1) {
          nbVersionDeRemoved++
          removeRel(relIndex)
          continue
        }
        // c'est la dernière, on log une seule fois
        logData(`${oid} avait ${nbVersionDe} relations de type ${type} vers ${rid}, seule la dernière vers ${rid} est conservée`)
      }
      // on note cette relation
      if (Array.isArray(relationsByRid[rid])) {
        // y'en a déjà plusieurs
        if (relationsByRid[rid].includes(type)) {
          logData(`${oid} avait deux relations de type ${type} vers ${rid}`)
          removeRel(relIndex)
          continue
        }
        relationsByRid[rid].push(type)
        logData(`${oid} a plus de deux relations vers ${rid}`, relationsByRid[rid])
      } else if (relationsByRid[rid]) {
        // y'en a déjà une
        if (relationsByRid[rid] === type) {
          logData(`${oid} avait deux relations de type ${type} vers ${rid}`)
          removeRel(relIndex)
          continue
        }
        // pas forcément une erreur mais c'est louche (car vraiment rare,
        // ça pourrait arriver avec traduction|corrigé + requiert, mais pas encore croisé en 2024-05)
        logData(`${oid} a deux relations vers ${rid}`, relationsByRid[rid])
        relationsByRid[rid] = [relationsByRid[rid], type]
      } else {
        relationsByRid[rid] = type
      }

      // on charge la relation
      if (!ressByRid[rid]) {
        ressByRid[rid] = await $ressourceFetch.fetch(rid)
        // si c'est une ressource distante sur une sesatheque qui ne répond pas,
        // ça plante et ne retourne pas undefined
      }
      const relRess = ressByRid[rid]
      if (!relRess) {
        logData(`ressource ${oid} liée à ${rid} qui n’existe pas => relation supprimée`, [type, rid])
        if (type === contientRefA && collType.includes(ress.type)) {
          // faut le propager pour virer les aliasOf foireux dans les enfants, sinon le save va remettre ces relations contientRefA
          forward404(ress, rid)
        }
        removeRel(relIndex)
        continue
      }
      if (relRess.aliasOf) {
        // on regarde s'il y a déjà une relation vers la "vraie" ressource
        let isAlreadyLinked = relationsByRid[relRess.aliasOf] === type ||
          (Array.isArray(relationsByRid[relRess.aliasOf]) && relationsByRid[relRess.aliasOf].includes(type))
        if (!isAlreadyLinked) {
          for (let i = relIndex + 1; i < originalRelations.length; i++) {
            const [t, r] = originalRelations[i]
            if (t === type && r === relRess.aliasOf) {
              isAlreadyLinked = true
              break
            }
          }
        }
        if (isAlreadyLinked) {
          logData(`ressource ${oid} liée à ${rid} qui est un alias (de ${relRess.aliasOf} qui a déjà cette relation) => relation supprimée`, [type, rid])
        } else {
          logData(`ressource ${oid} de type ${ress.type} liée à ${rid} qui est un alias (de ${relRess.aliasOf} vers laquelle cette ressource n'est pas liée) => relation supprimée. TODO: voir s’il faut ajouter une relation de type ${type} vers ${relRess.aliasOf}`, [type, rid])
        }
        // dans tous les cas on vire
        removeRel(relIndex)
        continue
      }

      // si c'est une collection on met à jour tous ses contientRefA
      if (collType.includes(relRess.type)) {
        $ressourceRepository.refreshRelationsContientRefA(relRess)
      }

      // check relation complémentaire
      const typeComplementaire = relationComplementaire[type]
      if (typeComplementaire) {
        if (!relRess.relations.some(([t, r]) => t === typeComplementaire && r === `${baseId}/${oid}`)) {
          // le complémentaire n'existe pas dans la ressource liée
          const toRemove = type === existeAussi || (type === estRefPar && collType.includes(relRess.type))
          let toAdd = autoAddedInRel.includes(typeComplementaire)
          if (!toRemove && !toAdd) {
            // à priori on sait pas, mais si un des deux est em ou am ou ato on ajoute à l'autre si c'est pas une collection
            if (['am', 'ato', 'em', 'url'].includes(ress.type) && !collType.includes(relRess.type)) toAdd = true
            if (['am', 'ato', 'em', 'url'].includes(relRess.type) && !collType.includes(ress.type)) toAdd = true
          }
          if (toRemove) {
            logData(`La relation ${typeComplementaire} vers ${oid} n’existe pas dans ${rid} de type ${relRess.type}, relation [${type}, ${rid}] supprimée dans ${oid}`)
            // viré d'office dans oid
            if (Array.isArray(relationsByRid[rid])) {
              // oid lié à rid par plusieurs relations
              relationsByRid[rid] = relationsByRid[rid].filter(r => r[0] !== type)
            } else {
              delete relationsByRid[rid]
              delete ressByRid[rid]
              delete relationsTitres[rid]
            }
            removeRel(relIndex)
            continue
          } else if (toAdd) {
            // ajouté d'office dans relRess
            relRess.relations.push([typeComplementaire, ress.rid])
            if (!relRess.relationsTitres) relRess.relationsTitres = {}
            if (relRess.relationsTitres[ress.rid] !== ress.titre) {
              relRess.relationsTitres[ress.rid] = ress.titre
            }
            logData(`Relation manquante de type ${typeComplementaire} ajoutée dans ${rid} de type ${relRess.type} (vers la ressource ${oid} de type ${ress.type})`)
            await saveRelated(relRess)
          } else {
            // sinon on peut pas savoir si faut virer dans oid ou ajouter dans rid
            logData(`La ressource ${rid} (${relRess.type}) aurait dû contenir la relation ${typeComplementaire} vers ${oid} (${ress.type}). TODO : il faudra rectifier manuellement (ajouter cette relation de type ${typeComplementaire} de ${rid} vers ${oid} ou supprimer la relation de type ${type} de ${oid} vers ${rid})`)
          }
        }
      }

      // check titre
      if (relationsTitres[rid] !== relRess.titre) {
        if (relationsTitres[rid]) {
          logData(`${oid} avait un titre erroné pour ${rid} « ${relationsTitres[rid]} » qui aurait dû être « ${relRess.titre} »`)
        } else {
          logData(`${oid} n’avait pas de titre pour ${rid} (« ${relRess.titre} »)`)
        }
        relationsTitres[rid] = relRess.titre
        hasChanged = true
      }
    } // boucle relations

    // on vérifie qu'on a pas de titre en trop
    for (const rid of Object.keys(relationsTitres)) {
      if (!relationsByRid[rid]) {
        // on râle pas si on vient de virer la relation
        if (!ridsRemoved.has(rid)) {
          logData(`${oid} avait un titre pour ${rid} sans lien vers cette ressource`)
        }
        delete relationsTitres[rid]
        hasChanged = true
      }
    }

    // sauvegarde éventuelle
    if (hasChanged && !isDryRun) {
      logData(`sauvegarde de la ressource corrigée ${oid} avec ses ${ress.relations.length} relation(s)`)
      await saveRess(ress)
    }
  } // checkOne

  // check préalable & init
  if (typeof oid === 'function') {
    done = oid
    oid = ''
  } else if (typeof opt === 'function') {
    done = opt
    opt = ''
  }
  let isDryRun = opt === 'test'
  if (oid === 'test') {
    isDryRun = true
    // on tolère de passer test d'abord et l'oid ensuite, ce sera une chaîne vide si opt n'est pas fourni
    oid = opt
  }
  const msgPrefix = isDryRun ? '(DRY-RUN) ' : ''
  if (typeof done !== 'function') throw new Error('Erreur interne, pas de callback de commande')

  let offset = 0
  // on fait de petits paquets car pour chaque ressource on va charger toutes ses relations
  const limit = 25
  let nbRess = 0
  const oidsMods = new Set()
  const externalMods = new Set()
  const EntityRessource = lassi.service('EntityRessource')
  const $ressourceFetch = lassi.service('$ressourceFetch')
  const $ressourceRemote = lassi.service('$ressourceRemote')
  const $ressourceRepository = lassi.service('$ressourceRepository')

  // go
  if (oid) {
    taskLog(`Starting checkRelations ${oid}`)
    EntityRessource.match('oid').equals(oid).grabOne((error, ress) => {
      if (error) return done(error)
      if (!ress) return done(Error(`La ressource ${oid} n’existe pas`))
      checkOne(ress)
        .then(() => {
          taskLog(`fin du check de ${oid}`)
          done()
        })
        .catch(done)
    })
  } else {
    flow().seq(function () {
      EntityRessource.match('relations').isNotNull().count(this)
    }).seq(function (nb) {
      nbRess = nb
      taskLog(`Starting checkRelations avec ${nb} ressources avec relations à analyser`)
      grabAll(this)
    }).seq(function () {
      const msg = `fin du check des relations de ${nbRess} ressources dont ${oidsMods.size} modifiées` +
        (externalMods.size
          ? `, avec également ${externalMods.size} modifs sur la ou les sesatheques externes`
          : ''
        )
      taskLog(msg)
      done()
    }).catch(done)
  }
}

checkRelations.help = function checkGraphesHelp () {
  taskLog('La commande checkRelations prend éventuellement un oid en argument pour vérifier les relations de la ressource et celles des ressources liées, sans argument elle lance la vérification de toutes les relations de toutes les ressources (TRÈS LONG ET GOURMAND).\nVous pouvez aussi passer "test" en argument supplémentaire (avec ou sans oid) pour signaler tous les pbs sans faire de modification')
}

module.exports = {
  checkRelations
}

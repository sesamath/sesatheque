#!/usr/bin/env node

'use strict'

/**
 * Pour appeler le cli avec une autre config que celle par défaut, passer par l'environnement :
 * `env SESATHEQUE_CONF=xxx app/cli.js …` pour utiliser la config _private/xxx.js
 */

const glob = require('glob')
const anLog = require('an-log')
const anLogLevels = require('an-log/source/lib/levels.js')
const boot = require('./boot')
const config = require('./config')

function beforeBootsrap (lassi, mainComponent, allComponents) {
  // on ajoute toutes les tasks AVANT le bootstrap
  const commands = {}
  let nbCommands = 0
  // eslint-disable-next-line n/no-path-concat
  glob.sync(`${__dirname}/cli/*.js`).concat(glob.sync(`${__dirname}/tasks/**/index.js`)).forEach(function (fichier) {
    // chaque module peut déclarer un service lassi *-cli et ne rien exporter, ou exporter
    // un objet dont chaque propriété sera une fonction avec une méthode help,
    // cette fonction deviendra alors une commande du nom de la propriété (les autres propriétés seront ignorées)
    // console.log('ajout du module', fichier)
    const module = require(fichier)
    for (const [name, fn] of Object.entries(module)) {
      if (typeof fn === 'function' && typeof fn.help === 'function') {
        // console.log('ajout de la commande ', name)
        commands[name] = fn
        nbCommands++
      }
    }
    if (nbCommands) {
      // console.log('aj service $sesatheque-cli')
      mainComponent.service('$sesatheque-cli', function () {
        // un service est une fct, et un service *-cli doit retourner une fct
        // qui doit retourner un objet avec une propriété commands qui doit être un fct
        // qui doit retourner un objet dont chaque propriété est une commande…
        // C'est lassi qui l'a dit…
        return function () {
          return {
            commands: () => commands
          }
        }
      })
    }
  })
}

try {
  for (const channel of [config.application.name, '$auth', '$cache', 'EntityDefinition', 'lassi']) {
    anLog(channel).setLogLevel(anLogLevels.ERROR)
  }
  // {cli: true} évite de lancer le serveur http et charge les services *.cli
  boot(beforeBootsrap, { cli: true }, () => {
    lassi.service('$cli').run()
  })
} catch (error) {
  console.error(error)
}

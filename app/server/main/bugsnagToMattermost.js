// un service qui forward des post venant de bugsnag (data forwarding, cf https://docs.bugsnag.com/product/integrations/data-forwarding/webhook/)
// vers mattermost (cf https://docs.mattermost.com/developer/webhooks-incoming.html)
// inspiré de https://github.com/marcopolee/hookbot/blob/master/index.js
// (et https://github.com/akleandrov/bugsnag-telegram/blob/master/lib/bot.js et d'autres formatage pour webhook mattermost https://github.com/search?l=JavaScript&q=mattermost+webhook&type=Repositories)
// avec aussi en go https://github.com/chrisjoyce911/bugsnagmattermost/
// et une classe php pour poster vers mattermost https://github.com/ThibaudDauce/mattermost-php

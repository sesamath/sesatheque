'use strict'

const fs = require('node:fs')
const path = require('node:path')

const log = require('sesajstools/utils/log')
const { version } = require('../../../package')
const { application: { name } } = require('../config')

const root = path.resolve(__dirname, '..', '..', '..')

/**
 * Contenu html de la home, pris dans _private/home.inc.html si ça existe.
 * Mis ici en display:none, c'est app/client-react/index.js qui va le récupérer
 * et le mettre dans le bout de DOM qu'il gère.
 * @private
 */
let homeContent

const homeContentFile = path.join(root, '_private', 'home.inc.html')
if (fs.existsSync(homeContentFile)) {
  homeContent = fs.readFileSync(homeContentFile, 'utf8')
  if (!homeContent) {
    log.error(`${homeContentFile} existe mais sans contenu`)
  }
}

if (!homeContent) homeContent = 'Site en construction.'

const html = `<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Médiathèque de ressources pour l'éducation">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" sizes="16x16" href="/favicon.png?${version}">
  <title>${name}</title>
</head>
<body>
<div id="homeContent" style="display: none;">${homeContent}</div>
<div id="root" role="document"></div>
<script
  type="application/javascript"
  src="/react.js?${version}"
></script>
</body>
</html>
`

const getHtml = () => html

/**
 * Ajoute la page react au contenu courant (pour beforeTransport)
 * @param {Context} context
 * @param {string} [contentToAdd] Sera ajouté tel quel dans la page, après le div root (juste avant </body>), à priori du js…
 */
function displayReactPage (context) {
  context.contentType = 'text/html'
  // faut utiliser raw sinon le content-type va imposer le transport html qui veut un template dust
  context.raw(html, {})
  // on peut fixer nos headers directement sur la réponse
  // context.response.append('Content-Length', reactPagelength)
  // mais express ajoute Content-Length lui-même
}

module.exports = {
  displayReactPage,
  getHtml
}

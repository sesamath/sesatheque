'use strict'

const path = require('node:path')
const { application: { staging } } = require('../config')

module.exports = function mainComponentFactory (lassi) {
  /**
   * Component qui défini
   * - le layout et les vues pour le html
   * - les controleurs des pages statiques
   * - des controleurs de debug en dev
   */
  const mainComponent = lassi.component('main')

  /**
   * On ajoute un dust.helper à l'initialisation du framework
   * Cf https://github.com/linkedin/dustjs/wiki/Dust-Tutorial#Writing_a_dust_helper
   *
   * context contient les propriétés stack,global,blocks,templateName,
   *     on peut récupérer les paramètres passés à la vue avec context.get('param')
   * bodies contient block
   * params liste les attributs passé au helper avec {@helper attrName1=...}
   * @see https://github.com/linkedin/dustjs/wiki/Dust-Tutorial#Writing_a_dust_helper
   * this.application.templateEngines.dust existe plus /
   this.application.templateEngines.dust.helper('dump', function (chunk, context, bodies, params) {
  return chunk.write('<pre class='debug'>' + JSON.stringify(params, null, 2) + '</pre>');
}); /**/

  // services
  require('./serviceSession')(mainComponent)
  // pour le statique
  require('./controllerMain')(mainComponent)
  // pour /api/checkSesalab et /api/checkSesatheque
  require('./controllerApi')(mainComponent)
  require('./serviceJson')(mainComponent)

  // pour la doc
  mainComponent.controller(function () {
    this.serve('doc', path.resolve(__dirname, '../../../documentation'))
  })
  // En dev on ajoute des routes pour debug
  if (['dev', 'debug'].includes(staging)) {
    require('./controllerDebug')(mainComponent)
  } else if (staging === 'test') {
    require('./controllerTest')(mainComponent)
  }

  // le listener beforeTransport est dans le composant ressource (il a besoin des services de ressource)
}

'use strict'

const path = require('node:path')
const fetch = require('node-fetch')

const { application: { staticMaxAge }, moodle } = require('../config')
const { displayReactPage } = require('./reactPage')
const { displayObsoletePage } = require('./obsoletePage')

const envSesathequeConf = process.env.SESATHEQUE_CONF

const root = path.resolve(__dirname, '..', '..', '..')

/**
 * Controleur du composant main pour les routes "statiques"
 * @Controller controllerMain
 */
module.exports = function (mainComponent) {
  mainComponent.controller(function ($rail) {
    // l'appli express
    const app = $rail.get()
    // nos ressources statiques générées par webpack
    const expressOptions = {
      fsPath: path.join(root, 'build'),
      maxAge: staticMaxAge || '7d'
    }
    // cf conf webpack, si y'a un SESATHEQUE_CONF ça build dans build.*
    if (envSesathequeConf) expressOptions.fsPath += '.' + envSesathequeConf
    this.serve('/', expressOptions)
    // et les ressources statiques qui bougent pas (CopyWebpackPlugin arrive pas à les copier, y'en a trop)
    expressOptions.fsPath = path.join(root, 'app', 'assets')
    this.serve('/', expressOptions)
    // les ressources statiques générées pour cette instance locale
    this.serve('/', path.join(root, 'local'))

    // le source react pour toutes ses routes
    // (en dev on sera pas appelé car c'est webpack-dev-server qui gère)
    // cf app/client-react/App.js pour ne pas oublier de routes

    // On continue à passer ici par un contrôleur pour toutes les pages statiques, même si à première
    // vue ce serait plus intelligent de construire ça au build avec html-webpack-plugin et le servir
    // en statique (via le serve qui précède), car les routes dynamiques demandent un contrôleur et
    // représentent > 90% des requêtes, pas la peine de doublonner du code pour optimiser un peu les
    // 10% qui restent.
    const reactRoutes = [
      '/',
      '/autocomplete',
      '/compte',
      '/mentionsLegales',
      '/ressource/ajouter',
      '/ressource/modifier/:oid',
      '/ressource/apercevoir/:oid',
      '/ressource/decrire/:oid',
      '/ressource/rechercher',
      '/groupe/ajouter',
      '/groupe/editer/:groupe',
      '/groupes/perso',
      '/groupes/ouverts',
      '/groupes/publics'
    ]
    if (moodle?.staticExports.length) reactRoutes.push('/giftsMoodle') // /moodle est récupéré par le serve static)
    for (const route of reactRoutes) {
      this.get(route, displayReactPage)
    }

    // page destinée aux navigateurs non pris en charge
    this.get('/navigateurObsolete', displayObsoletePage)

    // lassi ne gère pas les requêtes head. nginx en frontal le fait pour nous,
    // mais on veut répondre sur / pour le monitoring local (avec monit le 'protocol http' donne du head)
    app.head('/', (req, res) => res.send())

    // ajout de la route /replication_calculatice qui proxy vers ressources.sesamath.net/replication_calculatice
    // (pour les pbs de CORS dans canvas, cf rmq dans le plugin ecjs)
    // cf doc express http://expressjs.com/en/4x/api.html
    app.get('/replication_calculatice/*', function (req, res, next) {
      // y'a un exemple de reverse proxy sur https://www.w3codelab.com/solved/nodejs/proxy-with-express-js-solved/
      // d'autres qui utilisent request pour faire du request.get(url).pipe(res)
      // on prend une version avec node-fetch
      const url = 'https://ressources.sesamath.net' + req.url
      fetch(url).then(proxyRes => {
        // recopier les headers plante depuis la migration chez bearstech du 13/05/2022 (virer le connection: close suffit pas)
        // proxyRes.headers.forEach((v, n) => res.set(n, v))
        // on impose ça si c'est pas déjà donné
        if (!res.get('Cache-Control')) res.set('Cache-Control', 'public, max-age=259200') // 3j : 259200 = 3600 * 24 * 3
        proxyRes.body.pipe(res)
      }).catch(error => {
        res.writeHead(500)
        res.write(error.message)
      })
    })
  })
}

'use strict'

const { stringify } = require('sesajstools')

module.exports = function (component) {
  // route pour afficher des objets en dev (debug)
  component.controller('debug', function ($cache) {
    const controller = this

    controller.get('session', function (context) {
      context.layout = 'page'
      // on ajoute un compteur pour vérifier que ça s'incrémente de 1 à chaque affichage
      if (context.session.compteur) context.session.compteur++
      else context.session.compteur = 1
      context.plain(stringify(context.session, 2))
    })

    controller.post('post', function (context) {
      context.plain(stringify(context.post, 2))
    })

    controller.get('request', function (context) {
      context.plain(stringify(context.request, 2))
    })

    controller.get('response', function (context) {
      context.plain(stringify(context.response, 2))
    })

    // un controleur tout prêt pour tout et n'importe quoi
    controller.get('test', function (context) {
      context.plain(stringify({ foo: 'bar' }, 2))
    })

    // renvoie en json ce que l'on reçoit en post
    controller.post('ping', function (context) {
      context.json(context.post)
    })

    // une page pour voir le cache
    controller.get('cache/:key', function (context) {
      const key = context.arguments.key
      $cache.get(key, function (error, data) {
        if (error) context.json({ error: error.toString() })
        else if (data) context.json(data)
        else context.json({ success: true, message: 'la clé ' + key + ' n’existait pas en cache' })
      })
    })

    // une autre pour l'effacer
    controller.get('purgeCache/:key', function (context) {
      const key = context.arguments.key
      $cache.delete(key, function (error) {
        if (error) context.json({ error: error.toString() })
        else context.json({ success: true, message: 'clé ' + key + ' effacée' })
      })
    })
  })
}

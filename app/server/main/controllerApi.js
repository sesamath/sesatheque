'use strict'
const { getBaseUrl } = require('sesatheque-client/dist/server/sesatheques')
const config = require('../config')
const { checkSesalab, checkSesatheque } = require('../checkConfig')

module.exports = function mainApiControllersFactory (component) {
  component.controller('api', function mainApiControllers () {
    /**
     * Retourne la baseUrl d'une baseId de sesatheque
     * (connue par configuration ou déclarée ici par un client)
     * @route GET /api/baseId/:id
     */
    this.get('baseId/:id', function (context) {
      const baseId = context.arguments.id
      const baseUrl = getBaseUrl(baseId, false)
      if (baseUrl) context.rest({ baseUrl })
      else context.restKo({ error: `Sésathèque ${baseId} inconnue sur ${config.application.baseUrl}` })
    })

    /**
     * Valide la configuration d'un sesalab
     * Si tout est bon, retournera {success: true, baseId: 'laBaseId assignée au sesalab appelant'}
     * sinon un {success: false, errors: ['error 1', …]}
     * @route POST /api/checkSesalab
     */
    this.post('checkSesalab', function (context) {
      const { baseUrl, sesatheques } = context.post
      const { baseId, errors, warnings } = checkSesalab(baseUrl, sesatheques)
      if (errors.length) return context.restKo({ errors, warnings })
      context.rest({ message: 'Configuration sesalab OK', baseId, warnings })
    })

    /**
     * Valide la configuration d'une sésatheque (qui nous envoie ses sesatheques et sesalabs)
     * @route POST /api/checkSesatheque
     */
    this.post('checkSesatheque', function (context) {
      const { errors, warnings } = checkSesatheque(context.post)
      if (errors.length) context.restKo({ errors, warnings })
      else context.rest({ message: 'Configuration sésathèque OK', warnings })
    })
  })
}

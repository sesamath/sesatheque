'use strict'

module.exports = function personneComponentFactory (lassi) {
  /**
   * Component de gestion des personnes (auteurs) et des groupes
   * On ne peut pas scinder en 2 composants car on aurait des dépendances cycliques
   * avec les services à cheval les deux entités
   * @private
   */
  const personneComponent = lassi.component('personne')

  personneComponent.config(function ($settings) {
    // on vérifie que l'on a un cache avec des valeur acceptables
    const cacheTTL = $settings.get('components.personne.cacheTTL', null)
    if (!cacheTTL) {
      log.error('Il faudrait indiquer un TTL pour le cache de personne' +
        ' (en s, dans components.personne.cacheTTL), on le fixe à 1h')
    }
    if (cacheTTL < 60) throw new Error("Le cache personne doit avoir un TTL d'au moins 60s")
    if (cacheTTL > 24 * 3600) throw new Error('Le cache personne doit avoir un TTL inférieur à 24h (86400s)')
  })

  require('./serviceCachePersonne')(personneComponent)

  require('./EntityPersonne')(personneComponent)

  require('./serviceCacheGroupe')(personneComponent)

  require('./EntityGroupe')(personneComponent)

  require('./serviceGroupeRepository')(personneComponent)
  require('./servicePersonneRepository')(personneComponent)
  require('./serviceAccessControl')(personneComponent)
  require('./serviceGroupe')(personneComponent)
  require('./servicePersonneControl')(personneComponent)

  // controleur, pour api/personne
  require('./controllerApiPersonne')(personneComponent)
  // pour api/groupe et api/groupes
  require('./controllerApiGroupe')(personneComponent)
  require('./controllerApiGroupes')(personneComponent)
}

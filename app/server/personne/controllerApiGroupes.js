'use strict'

const flow = require('an-flow')

/**
 * Répond sur certaines requetes OPTIONS
 * @private
 * @param {Context} context
 */
function optionsOk (context) {
  context.next(null, 'OK') // ne pas renvoyer de chaîne vide sinon 404
}

module.exports = function (component) {
  component.controller('api/groupes', function (EntityGroupe, $groupeRepository, $accessControl, $json, $personneRepository, $groupe) {
    const { addGestionnairesNames } = $groupe

    /**
     * Controleur de la route /api/groupe/
     * @Controller controllerApiGroupe
     */
    const controller = this

    /**
     * Récupère la liste des groupes dont on est admin
     * @route GET /api/groupes/admin
     */
    controller.get('admin', function (context) {
      const groupesAdmin = []
      const oid = $accessControl.getCurrentUserOid(context)
      if (!oid) return $json.denied(context, "Il faut s'authentifier avant pour récupérer ses groupes")
      flow().seq(function () {
        $groupeRepository.fetchListManagedBy(oid, this)
      }).seq(function (groupesManaged) {
        if (groupesManaged && groupesManaged.length) {
          groupesManaged.forEach(function (groupe) {
            groupesAdmin.push({ name: groupe.nom, admin: true })
          })
        }
        $json.sendOk(context, { groupesAdmin })
      }).catch(function (error) {
        console.error(error)
        $json.sendKo(context, 'Une erreur est survenue dans la récupération des groupes')
      })
    })
    controller.options('admin', optionsOk)

    /**
     * Récupère la liste des groupes dont on est membre
     * @route GET /api/groupes/membre
     */
    controller.get('membre', function (context) {
      const groupesMembre = []
      const done = {}
      const oid = $accessControl.getCurrentUserOid(context)
      if (!oid) return $json.denied(context, "Il faut s'authentifier avant pour récupérer ses groupes")
      flow().seq(function () {
        $groupeRepository.fetchListManagedBy(oid, this)
      }).seq(function (groupesManaged) {
        if (groupesManaged && groupesManaged.length) {
          groupesManaged.forEach(function (groupe) {
            groupesMembre.push({ name: groupe.nom, admin: true })
            done[groupe.nom] = true
          })
        }
        $accessControl.getCurrentUserGroupesMembre(context).forEach(function (groupeName) {
          if (!done[groupeName]) {
            groupesMembre.push({ name: groupeName, member: true })
            done[groupeName] = true
          }
        })
        $json.sendOk(context, { groupesMembre })
      }).catch(function (error) {
        console.error(error)
        $json.sendKo(context, 'Une erreur est survenue dans la récupération des groupes')
      })
    })
    controller.options('membre', optionsOk)

    /**
     * Retourne la liste de tous les groupes du user courant, sous la forme d'un objet
     * {groupes: {nom: groupe},groupesAdmin: string[], groupesMembre: string[], groupesSuivis: string[]}
     * @route GET /api/groupes/perso
     */
    controller.get('perso', function (context) {
      const oid = $accessControl.getCurrentUserOid(context)
      if (!oid) return $json.denied(context, 'Il faut être authentifié pour récupérer ses groupes')
      const groupes = {}
      const addGroupe = (groupe) => {
        delete groupe.$loadState
        groupes[groupe.nom] = groupe
      }
      const response = {
        groupes,
        groupesAdmin: []
      }

      flow().seq(function () {
        $groupeRepository.fetchListManagedBy(oid, this)
      }).seq(function (managedGroups) {
        managedGroups.forEach((groupe) => {
          addGroupe(groupe)
          response.groupesAdmin.push(groupe.nom)
        })

        // on peut charger le user (pour avoir ses groupes à jour)
        $personneRepository.load(oid, this)
      }).seq(function (personne) {
        const { groupesMembre, groupesSuivis } = personne
        response.groupesMembre = groupesMembre
        response.groupesSuivis = groupesSuivis

        // les groupes qui manquent
        const missing = new Set()
        groupesMembre.concat(groupesSuivis).forEach(nom => {
          if (!groupes[nom]) missing.add(nom)
        })
        if (!missing.size) return this()
        const next = this
        $groupeRepository.fetchListByNom(Array.from(missing), (error, groupes) => {
          if (error) next(error)
          groupes.forEach(addGroupe)
          next()
        })
      }).seq(function () {
        // on peut traiter la liste de tous les groupes
        this(null, Object.values(groupes))
      }).seqEach(function (groupe) {
        addGestionnairesNames(context, groupe, this)
      }).seq(function () {
        $json.sendOk(context, response)
      }).catch($json.sendKo.bind(null, context))
    })
    controller.options('perso', optionsOk)

    /**
     * Récupère la liste des groupes suivis
     * @route GET /api/groupes/suivis
     */
    controller.get('suivis', function (context) {
      const groupesSuivis = []
      const done = {}
      const oid = $accessControl.getCurrentUserOid(context)
      if (!oid) return $json.denied(context, "Il faut s'authentifier avant pour récupérer ses groupes suivis")

      flow().seq(function () {
        $groupeRepository.fetchListManagedBy(oid, this)
      }).seq(function (groupesManaged) {
        if (groupesManaged && groupesManaged.length) {
          groupesManaged.forEach(function (groupe) {
            groupesSuivis.push({ name: groupe.nom, admin: true })
            done[groupe.nom] = true
          })
        }
        $accessControl.getCurrentUserGroupesMembre(context).forEach(function (groupeName) {
          if (!done[groupeName]) {
            groupesSuivis.push({ name: groupeName, member: true })
            done[groupeName] = true
          }
        })
        $accessControl.getCurrentUserGroupesSuivis(context).forEach(function (groupeName) {
          if (!done[groupeName]) {
            // on ajoute les urls pour ne plus suivre
            groupesSuivis.push({ name: groupeName, follower: true })
            done[groupeName] = true
          }
        })
        $json.sendOk(context, { groupesSuivis })
      }).catch(function (error) {
        console.error(error)
        $json.sendKo(context, 'Une erreur est survenue dans la récupération des groupes')
      })
    })
    controller.options('suivis', optionsOk)

    /**
     * Récupère la liste des groupes ouverts
     * @route GET /api/groupes/ouverts
     */
    controller.get('ouverts', function (context) {
      const pid = $accessControl.getCurrentUserPid(context)
      if (pid) {
        flow().seq(function () {
          $groupeRepository.loadOuvert(this)
        }).seqEach(function (groupe) {
          addGestionnairesNames(context, groupe, this)
        }).seq(function (groupesOuverts) {
          $json.sendOk(context, { groupes: groupesOuverts })
        }).catch(function (error) {
          console.error(error)
          $json.sendKo(context, 'Une erreur est survenue lors de la récupération des groupes ouverts')
        })
      } else {
        $json.denied(context, "Il faut s'authentifier avant pour récupérer les groupes ouverts")
      }
    })
    controller.options('ouverts', optionsOk)

    /**
     * Récupère la liste des groupes publics
     * @route GET /api/groupes/publics
     */
    controller.get('publics', function (context) {
      const pid = $accessControl.getCurrentUserPid(context)
      if (pid) {
        flow().seq(function () {
          $groupeRepository.loadPublic(this)
        }).seqEach(function (groupe) {
          addGestionnairesNames(context, groupe, this)
        }).seq(function (groupesPublics) {
          $json.sendOk(context, { groupes: groupesPublics })
        }).catch(function (error) {
          console.error(error)
          $json.sendKo(context, 'Une erreur est survenue lors de la récupération des groupes publics')
        })
      } else {
        $json.denied(context, "Il faut s'authentifier avant pour récupérer les groupes ouverts")
      }
    })
    controller.options('publics', optionsOk)
  })
}

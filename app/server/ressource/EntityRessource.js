'use strict'
const uuid = require('an-uuid')
const { exists, getBaseUrl, getRidComponents } = require('sesatheque-client/dist/server/sesatheques')
const { hasProp, isArrayNotEmpty, stringify } = require('sesajstools')

const { toDate } = require('../lib/tools')
const { basicArrayIndexer, getNormalizedName } = require('../lib/normalize')
const { getRidEnfants } = require('../lib/ressource')

const Ressource = require('../../constructors/Ressource')
const config = require('../config')
// idem config.component.ressource, mais le require permet une meilleure autocompletion
const configRessource = require('./config')

const schema = require('./EntityRessource.schema')

const myBaseId = config.application.baseId

module.exports = function (component) {
  component.entity('EntityRessource', function () {
    const EntityRessource = this
    /**
     * Notre entité ressource, cf [Entity](lassi/Entity.html)
     * @entity EntityRessource
     * @param {Object} values Un objet ayant des propriétés d'une ressource
     * @extends Entity
     * @extends Ressource
     */
    EntityRessource.construct(function (values) {
      // on initialise avec les propriétés d'un objet Ressource correctement typé et initialisé
      Object.assign(this, new Ressource(values, myBaseId))

      // mais après on ne peut plus ajouter de propriété dans afterStore, pas trouvé pourquoi…
      // => TypeError: Cannot assign to read only property 'rid' of object '#<Entity>'
      if (!hasProp(this, 'rid')) {
        Object.defineProperty(this, 'rid', {
          writable: true,
          enumerable: true,
          configurable: false
        })
      }

      // on cast les dates avec notre toDate() qui gère mieux les fuseaux,
      // plus pratique ici que dans le constructeur qui ne peut pas faire de require
      if (values) {
        if (values.dateCreation && !(values.dateCreation instanceof Date)) values.dateCreation = toDate(values.dateCreation)
        if (values.dateMiseAJour && !(values.dateMiseAJour instanceof Date)) values.dateMiseAJour = toDate(values.dateMiseAJour)
      }

      // la langue par défaut
      if (this.langue) {
        // on rectifie fre en fra
        if (this.langue === 'fre') this.langue = 'fra'
      } else {
        this.langue = configRessource.langueDefaut
      }
    })

    EntityRessource.validateJsonSchema(schema)

    EntityRessource
      // rid est obligatoire, mais on l'a pas encore à la création… => sparse
      .defineIndex('rid', { unique: true, sparse: true })
      // pour loadByCle
      .defineIndex('cle', { unique: true, sparse: true })
      .defineIndex('aliasOf')
      .defineIndex('origine', { sparse: true })
      .defineIndex('idOrigine', { sparse: true })
      .defineIndex('type')
      .defineIndex('titre')
      .defineIndex('niveaux')
      // par défaut, la valeur de l'index est la valeur du champ, mais on peut fournir une callback qui la remplace
      // on retourne un tableau qui ne contient que les rid des éléments liés sans la nature de la relation
      // @todo renommer l'index en relationRids
      .defineIndex('relations', function () {
        if (!this.relations || !this.relations.length) return null
        // on retourne pour chaque relation le rid lié, tant pis pour la nature de la relation
        // (c'est nécessaire pour une recherche efficace, et en général l'appelant peut déduire
        // le type d'après sa propre relation, c'est le complémentaire)
        return this.relations.map(relation => relation[1])
      })
      // pour les arbres, on indexe tous les enfants, c'est lourd en écriture d'index
      // mais indispensable si on veut retrouver tous les arbres qui contiennent un item donné
      // (pour mettre à jour titre & résumé par ex).
      .defineIndex('enfants', function () {
        if (!this.enfants || !this.enfants.length) return null
        const rids = getRidEnfants(this)
        if (rids.length) return null // arrive si l'arbre ne contient que des dossiers sans ressources dedans
        return rids
      })
      .defineIndex('langue')
      .defineIndex('publie', 'boolean')
      .defineIndex('indexable', 'boolean')
      .defineIndex('restriction', 'integer')
      .defineIndex('dateCreation', 'date')
      .defineIndex('dateMiseAJour', 'date')

    // les array standard
    // faut retourner une vraie fonction (pas une fct fléchée), car elle sera appelée via fn.call(entity)
    const getIndexer = (prop) => function () {
      return basicArrayIndexer(this[prop])
    }
    for (const prop of [
      'categories',
      'typePedagogiques',
      'typeDocumentaires',
      'auteurs',
      'auteursParents',
      'contributeurs'
    ]) {
      EntityRessource.defineIndex(prop, getIndexer(prop))
    }

    // idem mais avec normalizer
    // les groupes chez qui la ressource est publiée
    EntityRessource.defineIndex('groupes', { normalizer: getNormalizedName }, getIndexer('groupes'))
    // les groupes qui ont un droit d'écriture sur la ressource
    EntityRessource.defineIndex('groupesAuteurs', { normalizer: getNormalizedName }, getIndexer('groupesAuteurs'))

    // un index qui combine les champs contenant des pid
    EntityRessource.defineIndex('iPids', function () {
      return basicArrayIndexer([].concat(this.auteurs, this.auteursParents, this.contributeurs))
    })

    // les champs à indexer pour le fulltext
    // ATTENTION, si l'un de ces champs est null (ou array vide) il n'est pas indexé
    // https://www.mongodb.com/docs/manual/core/indexes/index-types/index-text/
    // If an existing or newly inserted document lacks a text index field
    // (or the field is null or an empty array), MongoDB does not add a
    // text index entry for the document.
    // => il faut modifier le beforeStore ci-dessous si cette liste change
    EntityRessource.defineTextSearchFields([
      // si cette liste change il faut mettre à jour le label dans app/client-react/components/ResourceSearchForm.js)
      ['titre', 5],
      ['resume', 2]
      // poid de 1 par défaut pour le reste
      // au 2024-03-08 on vire commentaires & description de la recherche
      // 'commentaires',
      // 'description'
    ])

    // beforeStore était dans $ressourceRepository, pour des questions de cycle d'injection de dépendances
    // on le ramène ici pour vérifier l'intégrité "interne" de l'entity, en laissant là-bas un beforeSave
    // pour vérifier l'intégrité des relations et voir s'il faut archiver

    EntityRessource.beforeStore(function (next) {
      try {
        const logAndNext = (errorMessage) => {
          log.dataError(errorMessage, this)
          const error = Error(errorMessage)
          error.noLog = true
          next(error)
        }

        // un identifiant pour les messages d'erreur
        const id = this.oid ||
          (this.origine && this.idOrigine && `${this.origine}/${this.idOrigine}`) ||
          this.titre

        // type et titre obligatoire
        if (!this.type) return logAndNext(`Ressource sans type, impossible à sauvegarder (${id})`)
        if (!this.titre) return logAndNext(`Ressource sans titre, impossible à sauvegarder (${id})`)

        // il faut que tous les champs de la recherche fulltext soient des strings
        // (ou des array non vides de strings), sinon c'est pas indexé
        for (const f of ['titre', 'resume', 'commentaires', 'description']) {
          if (typeof this[f] !== 'string') this[f] = ''
        }

        // on peut écraser une ressource en fournissant son rid (sans son oid),
        // on commence par vérifier ça
        if (this.rid) {
          const [baseId, oid] = getRidComponents(this.rid)
          if (baseId !== myBaseId) {
            return logAndNext(`Ressource avec rid ${this.rid} qui correspond à une autre Sésathèque (${baseId})`)
          }
          if (this.oid) {
            if (this.oid !== oid) {
              return logAndNext(`Ressource avec rid ${this.rid} incohérent avec son oid ${this.oid}`)
            }
          } else {
            this.oid = oid
          }
        } else if (this.oid) {
          this.rid = `${myBaseId}/${this.oid}`
        } // else rid sera fixé en afterStore d'après l'oid créé

        // on ne peut pas planter là-dessus au create (commun avec EntityArchive)
        // mais faut pas le laisser passer au store
        if (this.oid?.includes('@')) {
          return logAndNext(`Ressource avec id invalide, ${this.oid} est celui d’une archive`)
        }
        // pas de date d'archivage sur une ressource (la seule propriété qu'a une archive en plus d'une ressource
        if (this.dateArchivage) delete this.dateArchivage

        // check cohérence origine et idOrigine
        if (this.origine === myBaseId) {
          // on vire silencieusement car avant 2024-11 ces deux champs étaient toujours complétés (avec myBaseId/oid)
          delete this.origine
          delete this.idOrigine
        } else if (this.origine) {
          if (!this.idOrigine) {
            return logAndNext(`origine sans idOrigine (${id})`)
          }
          // on vérifie qu'on essaie pas d'enregistrer ici une ressource d'une autre sésathèque
          // (à priori un pb de dump, ou un post vers l'api de la mauvaise sesathèque)
          if (exists(this.origine)) {
            return logAndNext(`Cette ressource ${this.origine}/${this.idOrigine}) devrait être enregistrée sur ${this.origine} (${getBaseUrl(this.origine)})`)
          }
        } else if (this.idOrigine) {
          return logAndNext(`idOrigine sans origine (${id})`)
        }
        if (this.type === 'error') {
          // on note quand même dans le log qu'on enregistre une erreur
          return logAndNext(`Impossible d’enregistrer une ressource de type error (${id})`)
        }

        // check aliasOf
        if (this.aliasOf) {
          // on vérifie que ça pointe vers une base connue
          try {
            getRidComponents(this.aliasOf)
          } catch (error) {
            return logAndNext(`aliasOf ${this.aliasOf} invalide (ressource ${id})`)
          }
        }

        // on génère la clé de lecture si elle manque, on la vire si elle n'est plus nécessaire
        if (this.aliasOf || !this.publie || !this.restriction) {
          // alias ou non publié ou publié sans restriction => pas de clé
          if (hasProp(this, 'cle')) delete this.cle
        } else {
          // publié et restreint
          if (!this.cle) this.cle = uuid()
        }

        // les arbres (sauf alias) ont une propriété enfants obligatoire
        if (this.type === 'arbre' && !this.aliasOf) {
          if (!Array.isArray(this.enfants)) return logAndNext(`arbre sans propriété enfants (${id})`)
        }

        // pas le meme pid en auteursParents ou contributeurs s'il est déjà dans auteurs
        if (isArrayNotEmpty(this.auteurs)) {
          const isNotAuteur = pid => !this.auteurs.includes(pid)
          if (isArrayNotEmpty(this.auteursParents)) {
            this.auteursParents = this.auteursParents.filter(isNotAuteur)
          }
          if (isArrayNotEmpty(this.contributeurs)) {
            this.contributeurs = this.contributeurs.filter(isNotAuteur)
          }
        }

        // date de création
        if (!this.dateCreation) this.dateCreation = new Date()
        // date de mise à jour, sauf en cas de réindexation
        // (si c'est pas reindex c'est un autre batch qui indique ça)
        if (!this.$byPassDuplicate) this.dateMiseAJour = new Date()

        // cohérence de la restriction
        if (this.restriction === configRessource.constantes.restriction.groupe && (!this.groupes || !this.groupes.length)) {
          log.dataError(`Ressource ${id} restreinte à ses groupes sans préciser lesquels, on la passe privée`)
          this.restriction = configRessource.constantes.restriction.prive
        }

        // version
        if (!this.version) this.version = 1

        // check des relations dans $ressourceRepository.beforeSave() mais pas ici
        // (pour permettre des batch qui sauvent des paires liées par ex,
        // quand la 1re a pas encore sa relation en base)

        next(null, this)
      } catch (error) {
        next(error)
      }
    })

    // utilisé seulement juste après une création, pour init rid d'après l'oid qui vient d'être créé
    EntityRessource.afterStore(function (next) {
      if (this.rid) {
        next()
      } else {
        this.rid = myBaseId + '/' + this.oid
        this.store(next)
      }
    })

    // attention, c'est aussi déclenché sur un create avec oid
    EntityRessource.onLoad(function () {
      // on stocke la ressource telle qu'elle était au chargement, sérialisée pour ne pas avoir de shallow copy
      if (this.oid) this.$original = stringify(this)
    })
  })
}

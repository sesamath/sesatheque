'use strict'

const Ressource = require('../../constructors/Ressource')
const ressourceSchema = require('./EntityRessource.schema')

const properties = { ...ressourceSchema.properties, dateArchivage: { instanceof: 'Date' } }
const required = [...ressourceSchema.required, 'dateArchivage']
const archiveSchema = { ...ressourceSchema, properties, required }

module.exports = function (component) {
  component.entity('EntityArchive', function () {
    const EntityArchive = this
    /**
     * Idem {@link EntityRessource}, avec dateArchivage en plus et moins d'index
     * @name EntityArchive
     * @entity EntityArchive
     * @extends Ressource
     * @extends Entity
     */
    EntityArchive.construct(function (data) {
      if (!data) throw Error('constructeur d’archive appelé sans ressource ni archive')
      const ressource = new Ressource(data)
      Object.assign(this, ressource)
      if (!this.rid || !Number.isInteger(data.version) || data.version < 1) {
        throw Error('rid ou version invalides pour archiver une ressource')
      }

      // on ajoute une date d'archivage si y'en a pas
      this.dateArchivage = data.dateArchivage || new Date()

      // on force l'unicité en imposant l'oid à partir de ressourceOid @ version
      const ressourceOid = this.rid.substring(this.rid.indexOf('/') + 1)
      this.oid = ressourceOid + '@' + this.version
    })

    EntityArchive.validateJsonSchema(archiveSchema)

    // oid contient déjà oidRessource + version,
    // manque juste le rid pour retrouver toutes les versions d'une ressource
    EntityArchive.defineIndex('rid')
  })
}

'use strict'

// la route /debug est gérée par app/main/controllerDebug.js
/**
 * Controleur de la route /debug/ressource (existe seulement si on est pas en prod)
 * @Controller controlleurDebug
 * @requires {@link $ressourceRepository}
 */
module.exports = function (component) {
  component.controller('debug/ressource', function ($ressourceRepository, EntityRessource) {
    this.get('headers', function (context) {
      context.json({ success: true, headers: context.request.headers })
    })
    this.post('headers', function (context) {
      context.json({ success: true, headers: context.request.headers })
    })
  })
}

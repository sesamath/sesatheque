'use strict'

const { version } = require('../../../package')
const { application: { staging } } = require('../config')
const isDev = !staging.includes('prod')
const robotCanIndex = /^prod/.test(staging) // prod|production mais pas -pre-prod-
const { escapeForHtml } = require('sesajstools')

const rawOptions = { headers: { 'Content-Type': 'text/html' } }
const { encrypt } = require('../../utils/crypto')

/**
 * Retourne le code html de la page pour afficher la ressource (à priori dans une iframe, pas de header / footer ici)
 * @param {Ressource} ressource
 * @returns {string}
 */
module.exports = function displayRessource (context, ressource) {
  if (!ressource) throw Error('Impossible d’afficher une ressource sans la fournir')
  const titre = escapeForHtml(ressource.titre)
  const titreForArg = ressource.titre.replace(/"/g, '“')

  Object.keys(ressource).forEach(p => {
    if (p.substr(0, 1) === '$') delete ressource[p]
  })
  const correction = ressource.parametres && ressource.parametres.correction
  if (correction) {
    ressource.parametres.correction = encrypt(JSON.stringify(correction), ressource.rid)
  }

  const page = `<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="robots" content="${robotCanIndex ? 'noodp' : 'noindex'}"/>
  <title>${titre}</title>
  <meta property="og:title" content="${titreForArg}"/>
</head>
<body class="iframe">
  <div id="root" role="document">
  <div id="errors"></div>
  <div id="pictoFeedback" class="feedbackOff"></div>
  <button id="boutonVu">Vu <img src="/medias/cocheVerte.png" /></button>
  <h1 id="titre">${titre}</h1>
  <div id="display">
    Vous devez avoir un navigateur avec javascript activé pour voir ce contenu.<br />
    Chargement en cours…
  </div>
  <p id="warnObsolete" style="color: red; display: none;">Votre navigateur est très ancien et vous aurez probablement des problèmes d'affichage avec certaines ressources.</p>
<script type="text/javascript" src="/display.js?${version}"></script>
<script type="application/javascript">
(function () {
  function printError (error) {
    if (!error) return
    document.getElementById('errors').innerHTML = error.toString();
    if (console && console.error) console.error(error);
  }
  try {
    if (typeof stdisplay !== 'function') throw new Error('Le chargement a échoué, impossible de charger le module display');
    var options = {
      isDev: ${isDev},
      verbose: ${isDev}
    };
    if (window.location.hash === '#formateur') options.isFormateur = true
    stdisplay(${JSON.stringify(ressource)}, options, printError);
  } catch(error) {
    printError(error)
  }
})()
</script>
</div>
</body>
</html>
`
  context.raw(page, rawOptions)
}

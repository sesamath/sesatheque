'use strict'

const rawOptions = { headers: { 'Content-Type': 'text/html' } }

/**
 * Retourne le code html d'une page d'erreur
 * @param {Context} context
 * @param {Error|string} error
 * @param {number} [status=200] Un éventuel status qu'on affectera à context
 * @returns {string}
 */
module.exports = function displayError (context, error, status) {
  if (status) context.status = status
  if (!error) throw Error('Pour afficher une erreur il faut la fournir')
  const errorMessage = error.message || error
  let titre = 'Erreur'
  if (context.status) titre += ' ' + context.status

  const page = `<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="robots" content="noindex"/>
  <title>${titre}</title>
</head>
<body class="iframe">
  <div id="root" role="document">
    <div id="errors">${errorMessage}</div>
  </div>
</body>
</html>
`
  context.raw(page, rawOptions)
}

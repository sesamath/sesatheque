'use strict'

const { constantes: { categories, niveaux, relations, restriction, typeDocumentaires, typePedagogiques } } = require('./config')

// pour éviter de refaire ces Object.values au runtime à chaque validate on les construit ici au 1er appel
const categoriesValues = Object.values(categories)
const niveauxValues = Object.values(niveaux)
const relationPredicatValues = Object.values(relations)
const restrictionValues = Object.values(restriction)
const typeDocumentairesValues = Object.values(typeDocumentaires)
const typePedagogiquesValues = Object.values(typePedagogiques)

module.exports = {
  type: 'object',
  // ATTENTION, en cas d'ajout de propriété ici penser à aller mettre à jour
  // * la constante droppedPropertiesOnClone dans serviceRessourceConverter.js (pour clone / forkAlias)
  // * le constructeur sesatheque-client:src/constructors/Ressource.js
  properties: {
    oid: { type: 'string' },
    rid: { type: 'string' },
    aliasOf: { type: 'string' },
    cle: { type: 'string' },
    cleOriginal: { type: 'string' },
    origine: { type: 'string' },
    idOrigine: { type: 'string' },
    titre: { type: 'string' },
    type: { type: 'string' },
    resume: { type: 'string' },
    description: { type: 'string' },
    commentaires: { type: 'string' },
    niveaux: {
      type: 'array',
      items: {
        type: 'string',
        enum: niveauxValues
      },
      uniqueItems: true
    },
    categories: {
      type: 'array',
      items: {
        type: 'integer',
        enum: categoriesValues
      },
      uniqueItems: true
    },
    typePedagogiques: {
      type: 'array',
      items: {
        type: 'integer',
        enum: typePedagogiquesValues
      },
      uniqueItems: true
    },
    typeDocumentaires: {
      type: 'array',
      items: {
        type: 'integer',
        enum: typeDocumentairesValues
      },
      uniqueItems: true
    },
    parametres: { type: 'object' },
    enfants: {
      type: 'array',
      items: { $ref: '#/definitions/enfant' }
    },
    // Chaque relation est un tableau [type, rid], ce format n'est pas idéal…
    // Un objet avec clé rid et le type en valeur serait plus simple à gérer dans le code
    // mais on pourrait avoir deux types différents vers le même rid, par ex
    // estLaCorrectionDe et requiert, si la correction a besoin de charger son exercice pour fonctionner,
    // idem avec estTraductionDe & requiert
    // (pas encore vu de cas se présenter mais ça pourrait).
    // On aurait pu alors faire un objet avec le rid en clé et les types en valeur,
    // mais c'est pas le choix initial et ça ferait bcp de modifs dans le code (sans le simplifier bcp…)
    relations: {
      type: 'array',
      items: { $ref: '#/definitions/relation' }
    },
    relationsTitres: {
      // https://ajv.js.org/json-schema.html#patternproperties
      type: 'object',
      patternProperties: {
        '^[a-zA-Z0-9]+/[a-z0-9]+$': { type: 'string' }
      }
    },
    auteurs: { $ref: '#/definitions/arrayOfMixId' },
    auteursParents: { $ref: '#/definitions/arrayOfMixId' },
    contributeurs: { $ref: '#/definitions/arrayOfMixId' },
    groupes: { $ref: '#/definitions/arrayOfStrings' },
    groupesAuteurs: { $ref: '#/definitions/arrayOfStrings' },
    langue: { type: 'string' },
    publie: { type: 'boolean' },
    restriction: { type: 'integer', enum: restrictionValues },
    dateCreation: { instanceof: 'Date' },
    dateMiseAJour: { instanceof: 'Date' },
    version: { type: 'integer', minimum: 1 },
    versionNeedIncrement: { type: 'boolean' },
    // lui faut le laisser tant qu'il y en a en bdd (viré par le constructeur depuis le 2024-04-19)
    inc: { type: 'integer', minimum: 0 },
    indexable: { type: 'boolean' },
    archiveOid: { type: 'string' }
  },

  additionalProperties: false,
  required: ['titre', 'type'],

  definitions: {
    arrayOfMixId: {
      type: 'array',
      items: { $ref: '#/definitions/mixId' },
      uniqueItems: true
    },
    arrayOfStrings: {
      type: 'array',
      items: {
        type: 'string',
        minLength: 1
      },
      uniqueItems: true
    },
    enfant: {
      type: 'object',
      properties: {
        titre: { type: 'string' },
        type: { type: 'string' },
        aliasOf: { type: 'string' },
        enfants: {
          type: 'array',
          items: { $ref: '#/definitions/enfant' }
        }
      },
      required: ['titre', 'type'],
      additionalProperties: true
    },
    mixId: {
      type: 'string',
      pattern: '^[a-zA-Z0-9_-]+/[a-z0-9_-]+$'
    },
    relation: {
      type: 'array',
      items: [
        // le 1er élément de chaque relation est un prédicat
        { type: 'integer', enum: relationPredicatValues },
        // le 2e est un rid
        { $ref: '#/definitions/mixId' }
      ],
      minItems: 2,
      maxItems: 2
    }
  }
}

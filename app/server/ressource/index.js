'use strict'
module.exports = function ressourceComponentFactory (lassi) {
  // Composant de gestion des ressources
  const ressourceComponent = lassi.component('ressource')

  require('./EntityArchive')(ressourceComponent)
  require('./EntityRessource')(ressourceComponent)

  require('./serviceCacheRessource')(ressourceComponent)
  require('./serviceRessourceAutocomplete')(ressourceComponent)
  require('./serviceRessourceRemote')(ressourceComponent)
  require('./serviceRoutes')(ressourceComponent)
  require('./serviceRessourceRepository')(ressourceComponent)
  require('./serviceRessourceFetch')(ressourceComponent)
  require('./serviceRessourceControl')(ressourceComponent)

  require('./serviceRessourceConverter')(ressourceComponent)

  // les pages display
  require('./controllerRessource')(ressourceComponent)
  // l'api json
  require('./controllerApi')(ressourceComponent)
  require('./controllerApiListe')(ressourceComponent)
  // les exports
  require('./controllerExport')(ressourceComponent)

  // import calculatice
  require('./controllerImportEc')(ressourceComponent)

  // En dev on ajoute des routes de debug
  if (!global.isProd) {
    require('./controllerDebug')(ressourceComponent)
  }

  // settings
  ressourceComponent.config(function ($settings) {
    // on vérifie que l'on a un cache avec des valeur acceptables
    const cacheTTL = $settings.get('components.ressource.cacheTTL', null)
    if (!cacheTTL) log.error('Pas de TTL pour le cache de ressource  (components.ressource.cacheTTL, en s), fixé à 1h')
    else if (cacheTTL < 60) throw new Error("Le cache ressource doit avoir un TTL d'au moins 60s")
    else if (cacheTTL > 24 * 3600) throw new Error('Le cache ressource doit avoir un TTL inférieur à 24h (86400s)')
  })
}

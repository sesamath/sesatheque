'use strict'

// le fetch natif de node n'est passé stable qu'avec node 21, et il n'est pas encore dans nos prérequis
const nodeFetch = require('node-fetch')
const { exists, getBaseIdFromRid, getBaseUrl, getComponents } = require('sesatheque-client/dist/server/sesatheques')

const appConfig = require('../config')
const myBaseId = appConfig.application.baseId
const tokens = {}
appConfig.sesatheques.forEach(({ baseId, apiToken }) => {
  if (baseId && apiToken) tokens[baseId] = apiToken
})

/**
 * Service d'accès aux ressources d'autres sesatheques
 * @service $ressourceRemote
 * @type {$ressourceRemote}
 */

module.exports = function (component) {
  component.service('$ressourceRemote', function () {
    /**
     * Notifie une sesatheque distante d'une mise à jour ici (en appelant externalUpdate chez elle)
     * @memberOf $ressourceRemote
     * @param {string} baseId celle de la sésathèque à appeler
     * @param {string[]} oids liste des oid concernées là-bas (pour de la vérif et du signalement d'anomalie)
     * @param {Ressource} ressource la ressource modifiée à lui envoyer (titre et relations seulement) via un POST /api/ressource/externalUpdate
     * @returns {Promise<void>}
     */
    async function externalUpdate (baseId, oids, ressource) {
      log.debug('externalUpdate (remote) avec', { baseId, oids, ressource })
      if (baseId === myBaseId) throw Error('$ressourceRemote.externalUpdate ne gère pas les ressources locales')
      if (!exists(baseId)) throw Error(`Sésathèque ${baseId} inconnue`)
      if (!tokens[baseId]) throw Error(`pas de token en configuration pour appeler ${baseId}`)
      const { rid, titre, relations } = ressource
      const baseUrl = getBaseUrl(baseId)
      const abortController = new AbortController()
      // ça peut être assez long s'il y a bcp de ressources à modifier, on met 10s pour avoir de la marge
      const abortTimerId = setTimeout(() => abortController.abort(), 10_000)
      const url = `${baseUrl}api/ressource/externalUpdate`
      const response = await nodeFetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-ApiToken': encodeURIComponent(tokens[baseId])
        },
        signal: abortController.signal,
        body: JSON.stringify({ rid, titre, relations, oids })
      })
      clearTimeout(abortTimerId)
      const { message } = await response.json()
      // log.debug(`${baseUrl}api/ressource/externalUpdate répond ${message} avec`, { rid, titre, relations, oids })
      if (message !== 'OK') throw Error(`${url} répond avec l’erreur : ${message} suite à une modification de ${rid}`)
    }

    /**
     * Récupère une ressource externe (throw si elle n'est pas externe, passer par $serviceRessourceFetch.fetch si on ne sait pas, il nous appellera si c'est distant)
     * Résoud avec undefined si la ressource n'existe pas (et throw sur une autre erreur, 403 ou timeout par ex)
     * @memberOf $ressourceRemote
     * @param {string} rid
     * @returns {Promise<Ressource|undefined, Error>}
     */
    async function fetch (rid) {
      const [baseId, oid] = getComponents(rid)
      if (baseId === myBaseId) throw Error(`$ressourceRemote.load appelé avec une ressource locale ${rid}`)
      if (!exists(baseId)) throw Error(`rid ${rid} invalide ou correspondant à une sesathèque inconnue`)
      if (!tokens[baseId]) throw Error(`pas de token en configuration pour ${baseId}`)
      const baseUrl = getBaseUrl(baseId)
      const url = `${baseUrl}api/ressource/${oid}`
      const abortController = new AbortController()
      const abortTimerId = setTimeout(() => abortController.abort(), 3_000)
      const response = await nodeFetch(url, {
        headers: {
          'Content-Type': 'application/json',
          'X-ApiToken': encodeURIComponent(tokens[baseId])
        },
        signal: abortController.signal
      })
      // on retourne undefined sur une 404 (idem load local)
      if (response.status === 404) return undefined
      // pour les autres erreurs on retourne cette erreur
      if (!response.ok) throw Error(response.statusText)
      const { message, data: ressource } = await response.json()
      clearTimeout(abortTimerId)
      if (message !== 'OK') throw Error(message)
      if (ressource?.oid !== oid) throw Error(`Réponse incohérente de ${url} (pas la ressource ${oid}`)
      return ressource
    }

    /**
     * Met à jour ressource (préalablement récupérée par un fetch) sur sa sesatheque (en général parce qu'on lui a changé ses relations)
     * @memberOf $ressourceRemote
     * @param {Ressource} ressource
     * @returns {Promise<void>}
     */
    async function update (ressource) {
      const baseId = getBaseIdFromRid(ressource.rid)
      if (baseId === myBaseId) throw Error('$ressourceRemote.update ne gère pas les ressources locales')
      const baseUrl = getBaseUrl(baseId)
      const response = await nodeFetch(`${baseUrl}api/ressource`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-ApiToken': encodeURIComponent(tokens[baseId])
        },
        body: JSON.stringify(ressource)
      })
      const { message } = await response.json()
      if (message !== 'OK') throw Error(message)
    }

    return {
      externalUpdate,
      fetch,
      update
    }
  })
}

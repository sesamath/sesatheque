'use strict'

const { getComponents } = require('sesatheque-client/dist/server/sesatheques')

const appConfig = require('../config')
const myBaseId = appConfig.application.baseId
const tokens = {}
appConfig.sesatheques.forEach(({ baseId, apiToken }) => {
  if (baseId && apiToken) tokens[baseId] = apiToken
})

/**
 * Service d'accès aux ressources, locales ou distantes (ses méthodes retournent une promesse)
 * @service $ressourceFetch
 * @requires $cache
 * @requires $ressourceRemote
 * @requires $ressourceRepository
 */
module.exports = function (component) {
  component.service('$ressourceFetch', function ($cache, $ressourceRemote, $ressourceRepository) {
    /**
     * Renvoie une ressource récupérée ailleurs ou ici (ça peut être un alias)
     * Retourne undefined si elle n'existe pas
     * @memberOf $ressourceFetch
     * @param {string} rid
     * @returns {Promise<Ressource>}
     */
    async function fetch (rid) {
      const [baseId, id] = getComponents(rid)
      if (baseId === myBaseId) {
        return new Promise((resolve, reject) => {
          $ressourceRepository.load(id, function (error, ressource) {
            if (error) return reject(error)
            resolve(ressource)
          })
        })
      }

      return $ressourceRemote.fetch(rid)
    }

    /**
     * Renvoie une ressource récupérée ici ou ailleurs, en allant chercher l'original si rid remonte un alias.
     * Retourne undefined si elle n'existe pas.
     * Passe une erreur si la 2e ressource récupérée est encore un alias
     * (un alias d'alias ne devrait pas exister)
     * @memberOf $ressourceFetch
     * @param {string} aliasOf
     */
    async function fetchOriginal (aliasOf) {
      const ressource = await fetch(aliasOf)
      if (!ressource) throw Error(`L’alias ${aliasOf} n’existe pas`)
      if (ressource.aliasOf) {
        // si c'est un alias on recommence, mais une seule fois (trop risqué de mettre du récursif ici)
        log.dataError(`alias d’alias (${aliasOf} => ${ressource.aliasOf})`)
        const ress2 = await fetch(ressource.aliasOf)
        if (!ress2) throw Error(`L’alias ${aliasOf} pointe sur ${ressource.rid} qui est aussi un alias pointant sur ${ressource.aliasOf} qui n’existe pas`)
        if (ress2.aliasOf) throw Error(`Trop d’alias imbriqués (${aliasOf} => ${ressource.aliasOf} => ${ress2.aliasOf})`)
        return ress2
      }
      return ressource
    }

    return {
      fetch,
      fetchOriginal
    }
  })
}

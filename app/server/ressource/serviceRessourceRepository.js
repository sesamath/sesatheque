'use strict'

const flow = require('an-flow')
const _ = require('lodash')
const fetch = require('node-fetch')
const { parse, stringify } = require('sesajstools')
const { getRidComponents } = require('sesatheque-client/dist/server/sesatheques')
const { ensure, haveSameSetValues, isEntity } = require('../lib/tools')

const config = require('./config')
const appConfig = require('../config')

const myBaseId = appConfig.application.baseId
const { constantes: { relations: { contientRefA, estVersionDe }, relationComplementaire } } = config

// pour prévenir une boucle infinie, en cas de bug dans le flux du save (sauvegarder en boucle explose node assez vite)
const minMsBetweenSameSave = 200
// objet oid: timerId
const pendingSaves = {}

// et des petites fonctions utiles
// const prependMyBaseId = (oid) => myBaseId + '/' + oid
// const getRealRid = (ressource) => ressource.aliasOf || ressource.rid

module.exports = function (ressourceComponent) {
  ressourceComponent.service('$ressourceRepository', function (EntityRessource, EntityArchive, $ressourceRemote, $ressourceControl, $cacheRessource, $cache, $routes) {
    // on applique toujours un limit
    const { listeMax, listeNbDefault } = config.limites
    if (!listeMax) throw new Error('ressource.limites.listeMax manquant en configuration')

    /**
     * Nettoie les relations (helper de save), vire les invalides (type inconnu ou rid non conforme ou rid identique à la ressource) et ne conserve la dernière relation estVersionDe s'il y en a plusieurs
     * @param {Ressource} ressource
     */
    function cleanRelations (ressource) {
      // log.debug(`cleanRelations de ${ressource.oid} avec`, ressource.relations, { max: 1000 })
      const { oid, relations } = ressource
      if (!relations.length) return
      const dedup = new Set()
      const cleanedRelations = relations
        .filter((relation) => {
          // on veut un tableau de 2 éléments
          if (!Array.isArray(relation) || relation.length !== 2) {
            log.dataError(`${oid} contenait une relation invalide`, relation)
            return false
          }
          const [type, rid] = relation
          // dont le 1er est une relation connue
          if (!config.listes.relations[type]) {
            log.dataError(`${oid} contenait une relation invalide (type inconnu)`, relation)
            return false
          }
          // le 2e un rid plausible
          if (!/^[a-zA-Z0-9]+\/[a-zA-Z0-9]+$/.test(rid)) {
            log.dataError(`${oid} contenait une relation invalide (rid invalide)`, relation)
            return false
          }
          // qui n'est pas le rid de la ressource
          if (rid === ressource.rid) {
            log.dataError(`${oid} contenait une relation vers elle-même`, relation)
            return false
          }
          if (dedup.has(`${type}-${rid}`)) {
            log.dataError(`${oid} contenait une relation en double`, relation)
            return false
          }
          dedup.add(`${type}-${rid}`)
          // éventuel cast du type (forcément un entier car connu d'après le check plus haut)
          if (typeof type === 'string') {
            log.dataError(`${oid} contenait une relation dont le type était une string => transformé en number`, relation)
            relation[0] = Number(type)
          }
          return true
        })
      if (cleanedRelations.length < ressource.relations.length) {
        ressource.relations = cleanedRelations
      }

      if (!cleanedRelations.length) return

      // on limite le nb de ressources liées, il y en avait parfois des centaines
      // mais faut virer éventuellement les premières (les plus anciennes donc)
      // le complément existeAussi chez les ressources liées dont on vire les relations sera viré par updateRelationsAddedOrRemoved plus tard dans le flux du save
      if (ressource.relations.length > 1) {
        const allRidsVersionDe = []
        const otherRids = new Set()
        for (const [type, rid] of ressource.relations) {
          if (type === estVersionDe) allRidsVersionDe.push(rid)
          else otherRids.add(rid)
        }
        if (allRidsVersionDe.length > 4) {
          if (!ressource.relationsTitres) ressource.relationsTitres = {}
          const keptRids = allRidsVersionDe.slice(allRidsVersionDe.length - 1)
          ressource.relations = ressource.relations.filter(([type, rid]) => {
            if (type !== estVersionDe || keptRids.includes(rid)) return true
            // sinon faut le virer, on vire d'abord le titre s'il existe et n'est pas utilisé par un autre type de relation
            if (ressource.relationsTitres[rid] && !otherRids.has(rid)) {
              delete ressource.relationsTitres[rid]
            }
            return false
          })
          // log.debug(`cleanRelations a supprimé ${allRidsVersionDe.length - 4} relation(s) estVersionDe pour ne garder que les 4 dernières`)
        }
      }
      // fin limitation estVersionDe

      // log.debug(`fin de cleanRelations de ${ressource.oid} avec`, ressource.relations, { max: 1000 })
    } // cleanRelations

    /**
     * Ajoute les relationsTitres manquants éventuels (appelé par le flux du save)
     * (vérifie relations/relationsTitres de chaque ressource locale liée dont
     * on a pas le titre et la met à jour si besoin en tâche de fond)
     * @param {Ressource} ressource
     * @param next appelé avec une éventuelle liste de ressources à sauvegarder ensuite
     */
    function checkRelationsTitres (ressource, next) {
      if (!ressource.relations) ressource.relations = []
      if (!ressource.relations.length) {
        // on init avec une objet vide, ça effacera ce qu'il y avait si besoin
        ressource.relationsTitres = {}
        return next(null, [])
      }
      const ressToSaveLater = []
      const missingTitres = new Set()
      const knownTitres = new Set()
      if (!ressource.relationsTitres) ressource.relationsTitres = {}
      for (const [, rid] of ressource.relations) {
        if (ressource.relationsTitres[rid]) {
          knownTitres.add(rid)
        } else {
          missingTitres.add(rid)
        }
      }
      // on vire les titres inutiles
      for (const rid of Object.keys(ressource.relationsTitres)) {
        if (!knownTitres.has(rid)) delete ressource.relationsTitres[rid]
      }
      if (!missingTitres.size) return next(null, [])
      // faut ajouter les titres manquants, on trie local / remote
      const localOids = []
      const remoteRids = []
      for (const rid of missingTitres) {
        const [baseId, oid] = getRidComponents(rid)
        if (baseId === myBaseId) localOids.push(oid)
        else remoteRids.push(rid)
      }
      flow(localOids)
        .seqEach(function (oid) {
          // on les charge une par une en entier pour corriger si besoin leurs relations aussi
          // (et si elles sont en cache c'est plus performant qu'une requête sur le titre seul)
          load(oid, this)
        })
        .seqEach(function (linkedRess, index) {
          if (linkedRess) {
            const { rid, titre, relations, relationsTitres } = linkedRess
            ressource.relationsTitres[rid] = titre
            let linkedChanged = false
            // faut regarder si elle est sensée avoir des relations vers ressource,
            // et donc regarder les types de relations ressource => linkedRess pour trouver les complémentaires
            const types = ressource.relations
              .map(([t, r]) => r === rid ? relationComplementaire[t] : null)
              .filter(Boolean) // pour virer les null (ou undefined si une relation n'avait pas de complémentaire
            if (types.length) {
              // elle doit en avoir, on vérifie
              for (const type of types) {
                if (!relations.some(([t, r]) => t === type && r === ressource.rid)) {
                  log.debug(`${rid} n’avait pas de relation ${type} vers ${ressource.rid}`)
                  relations.push([type, ressource.rid])
                  linkedChanged = true
                }
              }
              if (relationsTitres[ressource.rid] !== ressource.titre) {
                relationsTitres[ressource.rid] = ressource.titre
                linkedChanged = true
              }
              if (linkedChanged) {
                // on ne peut pas la sauvegarder en tâche de fond, ça pourrait relancer un save de la ressource courante
                // (alors que son store n'a pas encore eu lieu, on est dans le flux du save)
                ressToSaveLater.push(linkedRess)
                // mais on met tout de suite en cache ces modifs, pour qu'un autre load qui aurait lieu avant son save
                // retourne cette version "corrigée"
                $cacheRessource.set(linkedRess)
              }
            }
          } else {
            log.dataError(`${ressource.oid} a une relation vers ${localOids[index]} qui n'existe pas`)
          }
          this()
        })
        .set(remoteRids)
        .seqEach(function (rid) {
          $ressourceRemote.fetch(rid)
            .then(ressource => this(null, ressource))
            .catch(error => {
              log.dataError(`En cherchant le titre de ${rid} (relation de ${ressource.oid}) on a eu l’erreur :`, error)
              this()
            })
        })
        .seqEach(function (remoteRessource, index) {
          if (remoteRessource) {
            ressource.relationsTitres[remoteRessource.rid] = remoteRessource.titre
          } else {
            log.dataError(`${ressource.oid} a une relation vers ${remoteRids[index]} mais $ressourceRemote.fetch() n’a rien ramené`)
          }
          // on ne modifie par remote, c'est updateRelationsAddedOrRemoved qui appellera sa sesathèque qui se mettra alors à jour
          this()
        })
        .seq(function () {
          next(null, ressToSaveLater)
        })
        .catch(next)
    }

    /**
     * Modifie ref en virant récursivement les enfants de tout item qui serait un alias
     * (pour garder l'aspect dynamique et obliger à charger les enfants chez l'original)
     * Ça ne devrait pas arriver (un alias avec enfants), mais on pourrait nous poster n'importe quoi.
     * @private
     * @param {Ref|Ressource} ref
     */
    function dropAliasEnfants (ref) {
      if (ref.enfants) {
        if (ref.aliasOf) {
          delete ref.enfants
        } else if (ref.enfants.length) {
          for (const enfant of ref.enfants) {
            if (!enfant.enfants) continue
            dropAliasEnfants(enfant)
          }
        }
      }
    }

    /**
     * Retourne la requête lassi, préparée d'après searchQuery
     * @private
     * @param {SearchQuery} searchQuery Un objet avec les match à faire en propriété et les valeurs à matcher en value (toujours Array)
     * @param {getListeFilters} [options.filters] tableau de filtres
     * @return {EntityQuery}
     */
    function getLassiQuery (searchQuery) {
      const lassiQuery = EntityRessource.match() // sans argument ça retourne une EntityQuery vierge
      for (const [prop, values] of Object.entries(searchQuery)) {
        if (!Array.isArray(values)) throw new Error(`values invalides (pas un tableau) pour ${prop}`)
        if (values.length) {
          if (prop === 'fulltext') {
            lassiQuery.textSearch(`"${values.join('" "')}"`)
          } else if (values.length > 1) {
            // plusieurs valeurs, on peut préciser une liste d'exclusion si la 1re valeur est '!'
            if (values[0] === '!') {
              lassiQuery.match(prop).notIn(values.slice(1))
            } else {
              lassiQuery.match(prop).in(values)
            }
          } else {
            const value = values[0]
            if (typeof value === 'string' && value.includes('%')) {
              lassiQuery.match(prop).like(value)
            } else if (typeof value === 'string' && value.startsWith('!')) {
              // on ne peut gérer ici que les strings, si on veut une négation d'entier faudra passer par le notIn, avec par ex ['!', 42]
              lassiQuery.match(prop).notEquals(value.substring(1))
            } else {
              lassiQuery.match(prop).equals(value)
            }
          }
        } else {
          // pas de valeur, on fait juste un match sur la prop, sauf fulltext qui n'y a pas droit
          if (prop === 'fulltext') throw new Error('searchQuery avec fulltext sans values')
          lassiQuery.match(prop)
        }
      }

      return lassiQuery
    }

    /**
     * Purge les urls publiques de la ressource sur varnish (si varnish est dans la conf, ne fait rien sinon)
     * (rend la main avant les réponses mais après avoir lancé les requêtes)
     * @param {Ressource|string} ressource ou son oid
     */
    function purgeVarnish (ressource) {
      if (!appConfig.varnish) return
      const myBaseUrl = appConfig.application.baseUrl
      // on ne purge que les ressources publiques (les autres ne sont pas en cache)
      if (ressource.publie && ressource.restriction === config.constantes.restriction.aucune) {
        // log.debug(`purge varnish de ${ressource.oid} en utilisant ${myBaseUrl}`)
        for (const url of [
          $routes.getAbs('api', ressource),
          $routes.getAbs('display', ressource),
          $routes.getAbs('describe', ressource),
          $routes.getAbs('preview', ressource)
        ]) {
          fetch(myBaseUrl + url.substring(1), { method: 'PURGE' })
            .then(response => {
              log.debug('purge ' + url + ' ' + (response.status))
              if (!response.ok) {
                log.error(`purge KO pour ${url} ${response.status} ${response.statusText}`)
              }
            }).catch(error => log.error(`purge KO pour ${url}`, error))
        }
      }
    }

    /**
     * Helper du save, pour
     * - incrémenter le n° de version si la ressource a une propriété versionNeedIncrement
     *   ou si une des propriétés listées dans config.versionTriggers a changée de valeur
     * - si besoin, archive en tâche de fond l'ancienne version
     * @private
     * @param {EntityRessource} ressource
     * @param {EntityRessource} previous
     * @param {Function} next
     */
    function archivePreviousIfNeeded (ressource, previous, next) {
      if (!ressource.oid || !previous.oid || ressource.oid !== previous.oid) return next(Error(`Comparaison impossible (${ressource.oid} vs ${previous.oid})`))

      // incrément de version ?
      // pour la comparaison, deux objets avec la même définition littérale sont vus != en js
      // on utilise https://lodash.com/docs#isEqual
      const versionNeedIncrement = ressource.versionNeedIncrement || config.versionTriggers.some((prop) => !_.isEqual(ressource[prop], previous[prop]))
      // version
      ressource.version = previous.version
      if (versionNeedIncrement) ressource.version++
      // on passe au suivant
      next(null, ressource)

      // ce qui suit continue en tâche de fond
      if (versionNeedIncrement) {
        log.debug(`archivePreviousIfNeeded de ${ressource.oid} a incrémenté la version et va archiver`, ressource)
        archive(previous, function (error, archive) {
          if (error) log.error(error)
          else log.debug('ressource archivée', archive)
        })
      }
    } // archivePreviousIfNeeded

    /**
     * Met à jour ressource.relations pour les relations de type contientRefA d'après le contenu de l'arbre|serie|sequenceModele (sync)
     * @param {Ressource} ressource
     */
    function refreshRelationsContientRefA (ressource) {
      // log.debug(`refreshRelationsContientRefA de ${arbre.oid} avec`, { relations: arbre.relations, enfants: arbre.enfants }, { max: 1000 })
      if (!ressource.relationsTitres) ressource.relationsTitres = {}
      // liste des rids liés actuellement avec contientRefA
      const oldRelationsContientRefA = new Set()
      for (const [type, rid] of ressource.relations) {
        if (type === contientRefA) oldRelationsContientRefA.add(rid)
      }
      // liste des rids pour les relations contientRefA d'après le contenu
      const newRelationsContientRefA = new Set()
      const parseEnfants = (branche) => {
        const list = Array.isArray(branche) ? branche : branche.enfants
        for (const enfant of list) {
          const { aliasOf, enfants, titre, type } = enfant
          if (aliasOf) {
            // log.debug(`trouvé l’enfant ${aliasOf}`)
            if (!oldRelationsContientRefA.has(aliasOf) && !newRelationsContientRefA.has(aliasOf)) {
              // c'est une nouvelle relation que l'on a pas encore ajoutée
              ressource.relations.push([contientRefA, aliasOf])
              ressource.relationsTitres[aliasOf] = titre
            }
            newRelationsContientRefA.add(aliasOf)
          } else if (type === 'arbre' && enfants?.length) {
            parseEnfants(enfant)
          }
        }
      }
      if (ressource.enfants?.length) {
        parseEnfants(ressource)
      } else if (ressource.parametres?.serie?.length) {
        parseEnfants(ressource.parametres.serie)
      } else if (ressource.parametres?.sousSequences?.length) {
        for (const ssSeq of ressource.parametres.sousSequences) {
          if (ssSeq.serie?.length) {
            parseEnfants(ssSeq.serie)
          }
        }
      }
      // log.debug('pour refA on avait', Array.from(oldRelationsContientRefA), { max: 1000 })
      // log.debug('pour refA on veut', Array.from(newRelationsContientRefA), { max: 1000 })

      // faut virer les relations qui auraient été supprimées, on les liste d'abord
      const deleted = new Set()
      for (const rid of oldRelationsContientRefA) {
        if (!newRelationsContientRefA.has(rid)) deleted.add(rid)
      }
      if (deleted.size) {
        ressource.relations = ressource.relations.filter(([type, rid]) => !(type === contientRefA && deleted.has(rid)))
      }
      // log.debug(`fin refreshRelationsContientRefA de ${arbre.oid} avec`, arbre.relations, { max: 1000 })
    } // refreshRelationsContientRefA

    /**
     * Met à jour ressource.relations pour ce qui concerne linkedRessource (qui a changé)
     * puis sauvegarde ressource si besoin, et appelle next
     * Appelé par updateRelationsAddedOrRemoved et externalUpdate
     * @param {Ressource} ressource
     * @param {Ressource} linkedRessource les propriétés {rid, titre, relations} de la ressource liée
     * @param next
     */
    function updateRelations (ressource, linkedRessource, next) {
      const { rid, relations } = ressource
      const lRid = linkedRessource.rid
      if (lRid === rid) {
        log.dataError(Error(`Ressource liée identique (${rid})`))
        return next()
      }
      log.debug(`updateRelations de ${ressource.oid} vers ${lRid}`, { relations: ressource.relations, linkedRessourceRelations: linkedRessource.relations }, { max: 1000 })
      let hasChanged = false
      // on regarde parmi les relations de linkedRessource celles qui concernent la ressource à mettre à jour
      // les types de relations qu'on doit avoir au final vers linkedRessource
      const newTypes = new Set()
      for (const [t, r] of linkedRessource.relations) {
        if (r === rid) {
          const tComp = relationComplementaire[t]
          if (tComp) newTypes.add(tComp)
        }
      }
      // les types de relation qu'on a déjà vers linkedRessource
      const actualTypes = new Set()
      for (const [t, r] of relations) {
        if (r === lRid) actualTypes.add(t)
      }
      log.debug(`updateRelations de ${ressource.oid} vers ${lRid} avec les types`, { before: Array.from(actualTypes), after: Array.from(newTypes) })
      // on peut màj
      if (!ressource.relationsTitres) ressource.relationsTitres = {}
      if (ressource.relationsTitres[lRid] !== linkedRessource.titre) {
        ressource.relationsTitres[lRid] = linkedRessource.titre
        hasChanged = true
      }
      if (!haveSameSetValues(actualTypes, newTypes)) {
        hasChanged = true
        if (newTypes.size) {
          // on doit avoir des relations, on regarde d'abord les éventuels ajouts
          for (const type of newTypes) {
            if (actualTypes.has(type)) continue
            hasChanged = true
            relations.push([type, lRid])
            actualTypes.add(type)
          }
          // les suppressions
          if (actualTypes.size > newTypes.size) {
            ressource.relations = relations.filter(([t, r]) => !(r === lRid && !newTypes.has(t)))
          }
          // màj du titre
          if (ressource.relationsTitres[lRid] !== linkedRessource.titre) {
            ressource.relationsTitres[lRid] = linkedRessource.titre
            hasChanged = true
          }
        } else {
          // il ne doit plus y avoir de relation vers linkedRessource
          if (actualTypes.size) {
            // on en a, faut les virer
            ressource.relations = relations.filter(([, r]) => r !== lRid)
            hasChanged = true
          }
          if (ressource.relationsTitres[lRid]) {
            delete ressource.relationsTitres[lRid]
            hasChanged = true
          }
        }
      }
      if (!hasChanged) {
        log.debug(`updateRelations de ${ressource.oid} n'avait rien à changer vers ${lRid}`)
        return next()
      }
      // et on sauvegarde les modifs
      ressource.store(function (error, ressource) {
        if (error) return next(error)
        log.debug(`fin updateRelations de ${ressource.oid} qui a désormais les relations`, ressource.relations, { max: 1000 })
        $cacheRessource.set(ressource)
        purgeVarnish(ressource)
        next()
      })
    } // updateRelations

    /**
     * Met à jour toutes les relations de ressource ajoutées ou supprimées (avec un type ayant une réciproque)
     * (appelé par save après le store de la ressource)
     * @param {Ressource} ressource
     * @param {Ressource} previous l'état précédent de la ressource
     * @param {string[]} ridsAlreadyProcessed La liste des rids déjà mis à jour
     */
    function updateRelationsAddedOrRemoved (ressource, previous, ridsAlreadyProcessed, next) {
      if (!previous) return next()
      log.debug(`updateRelationsAddedOrRemoved de ${ressource.oid} avec`, { actual: ressource.relations, previous: previous.relations }, { max: 1000 })
      if (!previous.relations) {
        log.dataError(Error('ressource avec previous sans relations'), ressource)
        previous.relations = []
      }
      // pour éviter de boucler plusieurs fois (boucle dans une boucle),
      // on note les relations que les liées doivent avoir vers ressource dans un objet par rid {rid: Set(types)}
      const previousRelations = {}
      const actualRelations = {}
      // pour remplir ça
      const populateTypeCompl = (ress, cumul) => {
        for (const [t, r] of ress.relations) {
          if (r === ress.rid) continue // bizarre mais déjà signalé par ailleurs
          const linkedType = relationComplementaire[t]
          if (linkedType) {
            if (!cumul[r]) cumul[r] = new Set()
            cumul[r].add(t)
          }
        }
      }
      populateTypeCompl(previous, previousRelations)
      populateTypeCompl(ressource, actualRelations)

      // la liste des rids à modifier
      const relatedToChange = new Set()

      // si notre titre change, toutes les relations doivent être modifiées
      if (ressource.titre !== previous.titre) {
        for (const rid of Object.keys(actualRelations)) relatedToChange.add(rid)
        // pas besoin de boucler sur les anciennes, les disparues sont gérées plus bas
      } else {
        // sinon, on regarde les relations ajoutées
        for (const [rid, types] of Object.entries(actualRelations)) {
          if (previousRelations[rid]) {
            // rid avait déjà une ou des relations vers nous, on regarde si c'était les mêmes
            if (!haveSameSetValues(types, previousRelations[rid])) {
              relatedToChange.add(rid)
            }
          } else {
            // on avait pas rid
            relatedToChange.add(rid)
          }
        }
      }
      // et dans tous les cas les disparues
      for (const rid of Object.keys(previousRelations)) {
        if (!actualRelations[rid]) relatedToChange.add(rid)
      }
      if (!relatedToChange.size) {
        log.debug(`updateRelationsAddedOrRemoved de ${ressource.rid} n’a rien à faire (pas de relation ajoutée|supprimée)`)
        return next()
      }
      // on vire les rids déjà traités
      if (ridsAlreadyProcessed.length) {
        for (const rid of ridsAlreadyProcessed) {
          if (relatedToChange.has(rid)) relatedToChange.delete(rid)
        }
      }
      if (relatedToChange.has(ressource.rid)) {
        log.error(`bug dans updateRelationsAddedOrRemoved qui a ajouté une relation vers la ressource elle-même (${ressource.rid})`)
        relatedToChange.delete(ressource.rid)
      }
      if (!relatedToChange.size) {
        log.debug(`updateRelationsAddedOrRemoved de ${ressource.rid} n’a rien à faire (toutes les relations ajoutées|supprimées ont déjà été traitées)`)
        return next()
      }
      log.debug(`la modif de ${ressource.rid} va entraîner des modifs (relation ajoutée|supprimée) pour`, Array.from(relatedToChange))

      // relatedToChange contient maintenant tous les rids à modifier, on trie par local / remote
      // (on peut mettre des array car on est maintenant sûr de l'unicité)
      const localOids = []
      const remoteOids = {} // baseId: []
      for (const rid of relatedToChange.values()) {
        const [baseId, oid] = getRidComponents(rid)
        if (baseId === myBaseId) {
          localOids.push(oid)
        } else {
          if (!remoteOids[baseId]) remoteOids[baseId] = []
          remoteOids[baseId].push(oid)
        }
      }
      // on a notre liste d'actions, triées en local / remote
      flow()
        // les actions locales
        .seq(function () {
          if (!localOids.length) return this(null, [])
          EntityRessource.match('oid').in(localOids).grab(this)
        })
        .seqEach(function (ress) {
          updateRelations(ress, ressource, this)
        })
        // on continue avec les remote
        .set(Object.keys(remoteOids))
        .seqEach(function (baseId) {
          $ressourceRemote.externalUpdate(baseId, remoteOids[baseId], ressource)
            .then((ressource) => this(null, ressource))
            .catch(this)
        })
        .empty()
        .done(next)
    } // updateRelationsAddedOrRemoved

    /**
     * Met à jour ici les ressources liées à une ressource externe
     * Appelé par le contrôleur POST /api/ressource/externalUpdate
     * @param {Object} datas
     * @param {string} datas.rid Le rid de la ressource externe modifiée
     * @param {string} datas.titre son titre
     * @param {Relation[]} datas.relations ses relations
     * @param {string[]} datas.oids la liste des oids ici qui devraient être liées à cette ressource (pour détecter une anomalie où la sésathèque qui appelle ignore une relation vers elle que l'on a ici)
     * @param next
     */
    function externalUpdate ({ rid, titre, relations, oids }, next) {
      try {
        log.debug('externalUpdate (local) avec', { rid, titre, relations, oids }, { max: 1000 })
        // d'après les relations de cette ressource, on construit les relations qu'on doit avoir ici
        const wantedRelationsByRid = {}
        for (const [type, rid] of relations) {
          if (!rid.startsWith(myBaseId)) continue // pas pour nous
          const typeComplementaire = relationComplementaire[type]
          if (typeComplementaire) {
            if (!wantedRelationsByRid[rid]) wantedRelationsByRid[rid] = new Set()
            wantedRelationsByRid[rid].add(typeComplementaire)
          }
        }

        // la liste des rids analysés
        const parsed = new Set()

        flow().seq(function () {
          // on commence par chercher nos ressources qui ont déjà une relation vers cette ressource externe (rid)
          EntityRessource.match('relations').equals(rid).grab(this)
        }).seqEach(function (ressource) {
          parsed.add(ressource.rid)
          updateRelations(ressource, { rid, titre, relations }, this)
        }).seq(function () {
          // et on regarde si de nouvelles ressources ont été liées à cette ressource distante
          const newRidsConcerned = []
          for (const [, rid] of relations) {
            if (!rid.startsWith(myBaseId)) continue // pas pour nous
            if (!parsed.has(rid)) newRidsConcerned.push(rid)
          }
          this(null, newRidsConcerned)
        }).seq(function (newRidsConcerned) {
          if (!newRidsConcerned.length) return this(null, [])
          EntityRessource.match('rid').in(newRidsConcerned).grab(this)
        }).seqEach(function (ressource) {
          parsed.add(ressource.rid)
          updateRelations(ressource, { rid, titre, relations }, this)
        }).seq(function () {
          if (parsed.size !== oids?.length) {
            log.dataError(`externalUpdate appelé avec la liste d’oids ${oids?.join(' ')} alors qu'on a étudié localement ${Array.from(parsed).join(' ')}`)
          }
          if (parsed.size) log.debug(`externalUpdate a étudié les ressources ${Array.from(parsed).join(' ')}`)
          else log.dataError(`externalUpdate n’a eu aucune ressource à étudier pour ${rid} qui avait les relations`, relations)
          next()
        }).catch(next)
      } catch (error) {
        next(error)
      }
    } // externalUpdate

    /**
     * Met en cache et fait suivre
     * Si on trouve un parametres.xml, on le jsonify
     * @private
     * @param {Error|null} error
     * @param {Ressource|Ressource[]|undefined} ressources ressource ou tableau de ressources (ou rien, sera passé à next tel quel)
     * @param {ressourceCallback|ressourcesCallback} next appelé avec (error, ressources)
     * @throws {Error} Si ressources est défini mais n'est pas une ressource ou un tableau de ressources
     */
    function cacheAndNext (error, ressources, next) {
      /**
       * Helper qui process une ressource et la met en cache (et rend la main sans attendre le résultat)
       * @private
       * @param ressource
       */
      function processOne (ressource) {
        if (!ressource || !ressource.oid) throw new Error('Paramètre invalide (ressource attendue)')
        $cacheRessource.set(ressource)
      }

      try {
        if (error) {
          log.error(error)
          return next(new Error('Problème d’accès à la base de données'))
        }
        if (Array.isArray(ressources)) ressources.forEach(processOne)
        else if (ressources) processOne(ressources)
        next(null, ressources)
      } catch (error) {
        next(error)
      }
    }

    /**
     * Les méthodes du service exportées
     */

    /**
     * Enregistre la ressource en archive et appelle next avec, mais ne modifie pas la ressource
     * @memberOf $ressourceRepository
     * @param {EntityRessource} ressource
     * @param next appelé avec (error, archive)
     */
    function archive (ressource, next) {
      if (!ressource.oid) return next(Error('Impossible d’archiver une ressource qui n’existe pas encore'))
      const data = Object.assign({}, ressource, { oid: undefined })
      EntityArchive.create(data).store(next)
    }

    /**
     * Efface une ressource (et ses index)
     * @memberOf $ressourceRepository
     * @param {EntityRessource|string} ressource (ou son oid)
     * @param {errorCallback}   next
     * @returns {undefined}
     */
    function remove (ressource, next) {
      const ressourceOid = typeof ressource === 'string' ? ressource : (ressource && ressource.oid)
      if (!ressourceOid) return next(new Error('remove appelé sans ressource'))
      log.debug(`La ressource ${ressourceOid} va être effacée`)
      // on vire du cache de toute façon
      $cacheRessource.delete(ressourceOid)
      EntityRessource.match('oid').equals(ressourceOid).purge(function (error, nb) {
        if (error) return next(error)
        if (nb > 1) next(new Error(`L’effacement de la ressource ${ressourceOid} a provoqué ${nb} suppressions`))
        if (nb === 1) log.debug(`La ressource ${ressourceOid} a été effacée`)
        else log.debug(`La ressource ${ressourceOid} n’existait pas`)
        purgeVarnish(ressource)
        next()
      })
    }

    /**
     * Supprime un groupe de toutes les ressources qui le contiennent (groupes et groupesAuteurs)
     * @param {string} nom
     * @param {errorCallback} next
     */
    function removeGroup (nom, next) {
      // Supprime dans groupes (+groupesAuteurs pour les ressources concernées)
      const deleteInGroupes = (skip) => {
        flow().seq(function () {
          EntityRessource.match('groupes').equals(nom).grab(this)
        }).seqEach(function (ressource) {
          ressource.groupes = ressource.groupes.filter(notMatch)
          if (ressource.groupesAuteurs.length) ressource.groupesAuteurs = ressource.groupesAuteurs.filter(notMatch)
          save(ressource, this)
        }).seq(function (ressources) {
          if (ressources.length < limit) return deleteInGroupesAuteurs(0)
          deleteInGroupes(skip + limit)
        }).catch(next)
      }
      // Supprime dans groupesAuteurs pour celles qui restent
      const deleteInGroupesAuteurs = (skip) => {
        flow().seq(function () {
          EntityRessource.match('groupesAuteurs').equals(nom).grab(this)
        }).seqEach(function (ressource) {
          ressource.groupesAuteurs = ressource.groupesAuteurs.filter(notMatch)
          save(ressource, this)
        }).seq(function (ressources) {
          if (ressources.length < limit) return next()
          deleteInGroupesAuteurs(skip + limit)
        }).catch(next)
      }

      const limit = 100
      const notMatch = (groupeNom) => groupeNom !== nom
      deleteInGroupes(0)
    }

    /**
     * Récupère un liste de ressource d'après critères
     * @param {SearchQuery} searchQuery Les critères de tri
     * @param {SearchQueryOptions} queryOptions Les options (skip & limit + orderBy éventuel)
     * @param {ressourcesCallback} next appelée avec (error, ressources)
     */
    function grabSearch (searchQuery, queryOptions, next) {
      try {
        if (!queryOptions) queryOptions = {}
        const lassiQuery = getLassiQuery(searchQuery)

        const sort = (orderBy) => {
          const [key, order] = orderBy
          if (order === 'desc') lassiQuery.sort(key, 'desc')
          else lassiQuery.sort(key)
        }

        // orderBy
        if (Array.isArray(queryOptions.orderBy)) {
          // un cas limite, un tableau à 2 elts dont le 2e est 'desc'
          if (queryOptions.orderBy.length === 2 && ['asc', 'desc'].includes(queryOptions.orderBy[1])) {
            sort(queryOptions.orderBy)
          } else {
            for (const orderBy of queryOptions.orderBy) {
              if (typeof orderBy === 'string') {
                lassiQuery.sort(orderBy)
              } else if (Array.isArray(orderBy)) {
                sort(orderBy)
              }
            }
          }
        }

        // limit & skip
        let { limit, skip } = queryOptions
        const wantedLimit = ensure(limit, 'integer', listeNbDefault)
        if (wantedLimit > 0 && wantedLimit <= listeMax) {
          limit = wantedLimit
        } else {
          // y'a un pb
          if (wantedLimit > listeMax) {
            log.error(new Error(`limite ${wantedLimit} demandée trop haute, ramenée à ${listeMax}`))
            limit = listeMax
          } else {
            log.error(new Error(`limite ${wantedLimit} invalide, mise à ${listeNbDefault}`))
            limit = listeNbDefault
          }
        }
        skip = ensure(skip, 'integer', 0)

        flow().seq(function () {
          lassiQuery.grab({ limit, skip }, this)
        }).seq(function (ressources) {
          if (ressources.length) cacheAndNext(null, ressources, next)
          else next(null, [])
        }).catch(next)
      } catch (error) {
        next(error)
      }
    } // grabSearch

    /**
     * Compte le nb de ressources d'après les critères de recherche
     * @param {SearchQuery} searchQuery Les critères de tri
     * @param {function} next appelée avec (error, total)
     */
    function grabSearchCount (searchQuery, next) {
      try {
        const lassiQuery = getLassiQuery(searchQuery)
        lassiQuery.count(next)
      } catch (error) {
        next(error)
      }
    }

    /**
     * Récupère une ressource et la passe à next (ressource undefined si elle n’existe pas)
     * @memberOf $ressourceRepository
     * @param {number|String}     oid  L'identifiant de la ressource (on accepte oid ou string origine/idOrigine)
     * @param {entityRessourceCallback} next appelée avec une EntityRessource
     * @returns {undefined}
     */
    function load (oid, next) {
      if (typeof oid !== 'string') return next(Error(`load veut un id en string, pas ${typeof oid}`))
      if (oid.includes('/')) {
        const [origin, idOrigin, bug] = oid.split('/')
        if (bug) return next(Error('identifiant invalide : ' + oid))
        if (origin === 'cle') return loadByCle(idOrigin, next)
        return loadByOrigin(origin, idOrigin, next)
      }
      // sinon c'est un oid
      $cacheRessource.get(oid, function (error, ressourceCached) {
        if (error) return next(error)
        if (ressourceCached) return next(null, ressourceCached)
        if (oid.includes('@')) {
          EntityArchive.match('oid').equals(oid).grabOne(function (error, archive) {
            if (error) return next(error)
            // pas de cache sur les archives
            if (archive) return next(null, EntityRessource.create(archive))
            next()
          })
        } else {
          EntityRessource.match('oid').equals(oid).grabOne(function (error, ressource) {
            cacheAndNext(error, ressource, next)
          })
        }
      })
    }

    /**
     * Récupère une ressource d'un auteur d'après son aliasOf et un pid auteur
     * (pour voir si cette personne a déjà un alias qui pointe sur aliasOf)
     * @memberOf $ressourceRepository
     * @param {string}            aliasOf
     * @param {string}            pid
     * @param {ressourcesCallback} next  appelée avec une EntityRessource
     */
    function loadByAliasAndPid (aliasOf, pid, next) {
      EntityRessource
        .match('aliasOf').equals(aliasOf)
        .match('auteurs').equals(pid)
        .grabOne(next)
    } // loadByAliasAndPid

    /**
     * Récupère une ressource d'après sa cle et la passe à next
     * @memberOf $ressourceRepository
     * @param {string}            cle
     * @param {entityRessourceCallback} next      appelée avec une EntityRessource
     */
    function loadByCle (cle, next) {
      if (!cle) return next(new Error('Clé manquante, impossible de charger la ressource'))
      $cacheRessource.getByOrigine('cle', cle, function (error, ressourceCached) {
        if (error) return next(error)
        if (ressourceCached) return next(null, ressourceCached)
        EntityRessource
          .match('cle').equals(cle)
          .grabOne(function (error, ressource) {
            cacheAndNext(error, ressource, next)
          })
      })
    } // loadByCle

    /**
     * Récupère une ressource d'après son idOrigine (ou son rid, dans ce cas origine est notre baseId) et la passe à next
     * @memberOf $ressourceRepository
     * @param {string}            origine (ou "cle" avec idOrigine qui est la clé, ou la baseId courante avec idOrigine qui est l'oid)
     * @param {string}            idOrigine
     * @param {entityRessourceCallback} next      appelée avec une EntityRessource
     */
    function loadByOrigin (origine, idOrigine, next) {
      if (!origine || !idOrigine) {
        return next(new Error('Origine ou idOrigine manquant, impossible de charger la ressource'))
      }
      // on est appelé par les controleurs sur les urls xxx/:origine/:idOrigine
      // mais le client de l'api peut passer un rid ou une clé,
      // on le gère ici plutôt que de mettre des if dans chaque contrôleur
      if (origine === 'cle') {
        return loadByCle(idOrigine, next)
      }
      if (origine === myBaseId) {
        return load(idOrigine, next)
      }
      // c'est un vraie origine
      $cacheRessource.getByOrigine(origine, idOrigine, function (error, ressourceCached) {
        if (error) return next(error)
        if (ressourceCached) return next(null, ressourceCached)
        EntityRessource
          .match('origine').equals(origine)
          .match('idOrigine').equals(idOrigine)
          .grabOne(function (error, ressource) {
            cacheAndNext(error, ressource, next)
          })
      })
    } // loadByOrigin

    /**
     * Récupère la version actuelle d'une ressource (d'après un oid d'archive ou de ressource)
     * @memberOf $ressourceRepository
     * @param {string} oid
     * @param {function} next appelée avec la version (undefined si la ressource n'existe pas)
     */
    function fetchVersion (oid, next) {
      if (oid.includes('@')) oid = oid.replace(/@.*/, '')
      $cacheRessource.get(oid, (error, ressource) => {
        if (error) return next(error)
        if (ressource) return next(null, ressource.version)
        EntityRessource
          .match('oid').equals(oid)
          .grab({ fields: ['version'] }, function (error, results) {
            return next(error, results?.[0]?.version)
          })
      })
    }

    /**
     * Renomme un groupe chez toutes les ressources qui l'ont (dans groupes ou groupesAuteurs)
     * @param oldName
     * @param newName
     * @param next
     */
    function renameGroup (oldName, newName, next) {
      const limit = 100
      const modifier = (nom) => nom === oldName ? newName : nom

      const updateGroupes = () => {
        flow().seq(function () {
          EntityRessource.match('groupes').equals(oldName).grab({ limit, offset }, this)
        }).seqEach(function (ressource) {
          ressource.groupes = ressource.groupes.map(modifier)
          ressource.groupesAuteurs = ressource.groupesAuteurs.map(modifier)
          save(ressource, this)
        }).seq(function (ressources) {
          if (ressources.length < limit) {
            // on a fini
            offset = 0
            return updateGroupesAuteurs()
          }
          // faut refaire un tour
          offset += limit
          updateGroupes()
        }).done(next)
      }

      const updateGroupesAuteurs = () => {
        flow().seq(function () {
          // pour les groupes auteur qui ne publiaient pas dans leur groupe
          EntityRessource.match('groupesAuteurs').equals(oldName).grab({ limit, offset }, this)
        }).seqEach(function (ressource) {
          ressource.groupesAuteurs = ressource.groupesAuteurs.map(modifier)
          save(ressource, this)
        }).seq(function (ressources) {
          if (ressources.length < limit) return next()
          offset += limit
          updateGroupesAuteurs()
        }).done(next)
      }

      let offset = 0
      updateGroupes()
    }

    /**
     * Ajoute ou modifie une ressource (contrôle la validité avant et incrémente la version au besoin),
     * met à jour le cache (interne + varnish)
     * et toutes les relations (créées pour les arbres s'il en manque).
     *
     * ATTENTION, c'est la seule méthode qui garanti l'intégrité du cache, entityRessource.store()
     * utilisé directement peut être plus efficace pour du batch, mais faut y réfléchir à deux fois !
     * @memberOf $ressourceRepository
     * @param {EntityRessource}   ressource
     * @param {entityRessourceCallback} [next]    appelée avec une EntityRessource
     */
    function save (ressource, next) {
      log.debug(`save de ${ressource.oid || ressource.titre} avec ${ressource.relations?.length ?? 0} relation(s)`)

      let previous, ressourcesToSaveLater
      const { oid } = ressource

      // trick anti boucle infinie (au cas où un bug dans notre flux du save déclencherait du ping-pong entre une ressource et une relation)
      // mais on peut avoir des save très rapprochés et légitimes, en cas d'update d'un arbre qui a N ressources toutes en relation
      // avec la ressource R, on peut avoir du save(R) N fois en quelques ms (pour l'upgrade 43 par ex il faut descendre le minDelay à 50ms)
      if (oid && ressource.dateMiseAJour) {
        const now = Date.now()
        const last = ressource.dateMiseAJour.valueOf()
        if (now - last < minMsBetweenSameSave) {
          // on pourrait faire un clearTimeout et en relancer un, pour faire du debounce, sauf que le save qui est déjà en attente
          // pourrait avoir des modifs que le suivant n'a pas encore, il vaut mieux planter le 2e et laisser le 1er se terminer
          if (pendingSaves[oid]) return next(Error(`3e save lancé pour la ressource ${ressource.oid} à moins de ${minMsBetweenSameSave}ms d’intervalle`))
          pendingSaves[oid] = setTimeout(() => {
            save(ressource, function (error, ressource) {
              delete pendingSaves[oid]
              next(error, ressource)
            })
          }, minMsBetweenSameSave)
          return
        }
      }

      flow().seq(function () {
        const nextStep = this
        if (!isEntity(ressource, 'EntityRessource')) {
          // ça planterait plus loin au beforeStore mais inutile de continuer si on le sait déjà
          if (ressource.oid?.includes('@')) {
            return next(Error('$ressourceRepository.save() ne peut pas sauvegarder une archive'))
          }
          // log.debug('save d’une ressource qui n’est pas une entity', ressource)
          // ça sort pas de la base, donc on le charge ou on le crée…
          ressource = EntityRessource.create(ressource)
          if (ressource.oid) {
            // il faut que $original soit ce qui est dans la base
            return load(ressource.oid, function (error, ress) {
              if (error) return next(error)
              if (ress) {
                delete ress.$original
                ressource.$original = stringify(ress)
              } else {
                delete ressource.$original
              }
              nextStep()
            })
          }
        }
        nextStep()
      }).seq(function () {
        if (['arbre', 'serie', 'sousSequence'].includes(ressource.type)) {
          refreshRelationsContientRefA(ressource)
        }
        // si ressource est un alias, faut virer les enfants (en récursif dans l'arbre
        if (ressource.enfants?.length) dropAliasEnfants(ressource)
        cleanRelations(ressource)
        // difficile de savoir si ça sort de la base ou si c'est un objet avec oid passé au create
        // car dans les deux cas onLoad est appelé et ça génère un $original, qui n'est donc pas forcément
        // ce qu'il y a en base (faudra fixer ça dans lassi, mais y'avait une raison pour appeler onLoad sur
        // du create avec oid…)
        // On avait un load ici avant 2024-05-03, mais le seul EntityRessource.create()
        // avec un oid sans que ce soit qqchose qui vient de sortir de la base est géré ci-dessus
        if (ressource.$original) {
          // log.debug(`avant archivePreviousIfNeeded de ${ressource.oid}`, ressource.relations, { max: 1000 })
          previous = parse(ressource.$original)
          return archivePreviousIfNeeded(ressource, previous, this)
        }
        this()
      }).seq(function () {
        // log.debug(`après archivePreviousIfNeeded les ${ressource.relations.length} relation(s) de ${ressource.oid}`, ressource.relations, { max: 1000 })
        checkRelationsTitres(ressource, this)
      }).seq(function (_ressourcesToSaveLater) {
        ressourcesToSaveLater = _ressourcesToSaveLater
        log.debug(`checkRelationsTitres a retourné ${_ressourcesToSaveLater.length} ressources modifiées (${_ressourcesToSaveLater.map(r => r.oid).join(' ')}) d'après les ${ressource.relations.length} relations de ${ressource.oid ?? ' cette nouvelle ressource à enregistrer'}`, ressource.relations, { max: 1000 })
        ressource.store(this)
      }).seq(function (savedRess) {
        // log.debug(`après le store de ${ressource.oid} on a ${savedRess.relations.length} relation(s)`, ressource.relations, { max: 1000 })
        if (!savedRess.oid) throw Error('Après un write la ressource n’a pas d’oid')
        // mise en cache, purge varnish et passage au suivant
        // gestion du cache pas possible en afterStore car le cache dépend de l'entité.
        // C'est aussi plus logique que $ressourceRepository gère cache + intégrité croisée des données,
        // et le gestionnaire d'entité seulement l'intégrité interne des données d'une entité
        $cacheRessource.set(savedRess)
        purgeVarnish(savedRess)
        log.debug(`ressource ${savedRess.rid} enregistrée avec ${savedRess.relations.length} relations`)
        // on pourrait appeller next dès maintenant, mais on le fait quand même à la fin pour ếviter d'être
        // rappelé trop tôt avec une ressource encore à modifier dans ce flux
        // maintenant que la ressource est sauvegardée, on peut lancer une màj chez les relations déjà modifiées par checkRelationsTitres
        ressource = savedRess
        this(null, ressourcesToSaveLater)
      }).seqEach(function (ress) {
        log.debug(`après store de ${ressource.oid} on save ${ress.oid} (modifiée par checkRelationsTitres)`)
        save(ress, this)
      }).seq(function () {
        // puis modifier les relations disparues ou ajoutées
        if (previous) updateRelationsAddedOrRemoved(ressource, previous, ressourcesToSaveLater.map(r => r.rid), this)
        else this()
      }).seq(function () {
        if (next) next(null, ressource)
      }).catch(function (error) {
        // on log toujours ici
        log.error(error)
        if (next) next(error)
      })
    } // save

    /**
     * Récupère un liste de ressource d'après critères
     * @memberOf $ressourceRepository
     * @param {SearchQuery} searchQuery Les critères de tri
     * @param {SearchQueryOptions} queryOptions Les options (skip & limit + orderBy éventuel)
     * @param {ressourcesCallback} next appelée avec (error, {ressources, total})
     */
    function search (searchQuery, queryOptions, next) {
      grabSearchCount(searchQuery, function (error, total) {
        if (error) return next(error)
        if (total === 0) return next(null, { ressources: [], total })
        grabSearch(searchQuery, queryOptions, function (error, ressources) {
          if (error) return next(error)

          next(null, { ressources, total })
        })
      })
    }

    /**
     * Service d'accès aux ressources, utilisé par les différents contrôleurs
     * @service $ressourceRepository
     * @requires EntityRessource
     * @requires EntityArchive
     * @requires $ressourceControl
     * @requires $cacheRessource
     * @requires $cache
     */
    return {
      archive,
      externalUpdate,
      fetchVersion,
      grabSearch,
      grabSearchCount,
      load,
      loadByAliasAndPid,
      loadByCle,
      loadByOrigin,
      refreshRelationsContientRefA,
      remove,
      removeGroup,
      renameGroup,
      save,
      search
    }
  })
}

/**
 * @callback ressourcesCallback
 * @param {Error} error
 * @param {Ressource[]} ressources
 */
/**
 * Tableau de filtres
 * @typedef {getListeFilter[]} getListeFilters
 */
/**
 * Filtre de la forme {index:'indexAFiltrer', values:valeurs},
 * valeurs peut être un tableau de valeurs ou [undefined] (ça filtrera sur les ressources ayant cet index)
 * @typedef {{index: string, values: Array}} getListeFilter
 */

'use strict'

const { exportRessourceAsGift } = require('../lib/moodle')

/**
 * Envoie une erreur en plain text
 * @param {Context} context
 * @param {Error} error
 * @param {number} [status=500]
 */
const sendPlainError = (context, error, status = 500) => {
  context.status = status
  if (status === 500) console.error(error)
  return context.plain(`Error: ${error.message}`)
}

/**
 * Controleur de la route /export/ (qui répond en plain/text pour les exports moodle ou du csv)
 * Toutes les routes contenant /public/ ignorent la session (cookies viré par varnish,
 * cela permet de mettre le résultat en cache et devrait être privilégié pour les ressources publiques)
 * @Controller controllerExport
 */
module.exports = function (component) {
  component.controller('export', function ($ressourceRepository, $ressourceConverter, $ressourceControl, $accessControl) {
    const controller = this
    /**
     * Retourne un gift pour moodle
     * @route GET /export/public/moodle/:oid
     */
    controller.get('public/moodle/:oid', function (context) {
      const { oid } = context.arguments
      $ressourceRepository.load(oid, function (error, ressource) {
        if (error) return sendPlainError(context, error)
        if (!ressource) return sendPlainError(context, Error(`La ressource ${oid} n’existe pas`), 404)
        // ici pas du 401 car même en étant authentifié on ne peut pas récupérer de gift de ressource privée
        if (!$accessControl.isPublic(ressource)) return sendPlainError(context, Error(`La ressource ${oid} n’est pas publique`), 400)
        exportRessourceAsGift(ressource)
          .then(gift => context.plain(gift))
          .catch(error => sendPlainError(context, error))
      })
    })
  })
}

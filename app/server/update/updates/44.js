const flow = require('an-flow')

const updateNum = __filename.substring(__dirname.length + 1, __filename.length - 3)
const updateLog = require('an-log')('update' + updateNum)

const name = 'Ajout de cleOriginal sur les alias'

module.exports = {
  name,
  description: 'Afin qu’un alias d’une ressource restreinte puisse s’afficher, il doit conserver la clé de la ressource originale',
  run: function run (done) {
    const EntityRessource = lassi.service('EntityRessource')
    const $ressourceRepository = lassi.service('$ressourceRepository')
    const $ressourceFetch = lassi.service('$ressourceFetch')

    let nbAliases = 0
    const limit = 50
    let skip = 0

    // met à jour les relations / relationsTitres de toutes les relations ajoutées ou supprimées
    // (locales ou distantes), donc normalement chez tous les enfants aussi.
    const grabAliasWithCle = () => {
      // le nb d'alias traités dans ce batch
      let nb
      let nbKO = 0
      flow().seq(function () {
        nb = 0
        EntityRessource
          .match('aliasOf').isNotNull()
          .match('cle').isNotNull()
          .sort('oid').grab({ skip, limit }, this)
      }).seqEach(function (alias) {
        nb++
        // on vire la clé
        delete alias.cle
        // faudra sauvegarder quoi qu'il arrive
        // (cb et pas directement this, pour virer le 2e elt, l'alias,
        // histoire de ne pas le conserver dans la pile)
        const nextAlias = (error) => this(error)

        if (alias.cleOriginal) {
          // bizarre, il avait cle ET cleOriginal
          updateLog.error(`L’alias ${alias.oid} a les deux propriétés cle ${alias.cle} ET cleOriginal ${alias.cleOriginal}`)
          return nextAlias()
        }

        // on cherche la cle de l'original
        $ressourceFetch.fetch(alias.aliasOf)
          .then(ressource => {
            if (!ressource) {
              updateLog.error(`L’alias ${alias.oid} pointe vers ${alias.aliasOf} qui n’existe pas (ou plus)`)
            } else if (ressource.aliasOf) {
              updateLog.error(`L’alias ${alias.oid} pointe vers ${alias.aliasOf} qui est aussi un alias (vers ${ressource.aliasOf})`)
            } else if (ressource.cle) {
              alias.cleOriginal = ressource.cle
            }
          })
          .then(() => {
            $ressourceRepository.save(alias, nextAlias)
          })
          .catch(error => {
            nbKO++
            updateLog.error(`Pb pour récupérer l’original ${alias.aliasOf} de l’alias ${alias.oid}`, error)
            // on laisse en l'état (faut pouvoir relancer le script et récupérer les alias avec cle à pb)
            nextAlias()
          })
      }).seq(function () {
        nbAliases += nb
        if (nb < limit) {
          updateLog(`FIN : ${nbAliases} alias traités`)
          return done()
        }
        // faut refaire un tour, attention, on ajoute pas le nb traités à skip, car il ne remontent plus dans la requête,
        // faut simplement sauter ceux qui ont eu une erreur et ont conservé leur clé
        skip = nbKO
        process.nextTick(grabAliasWithCle)
      }).catch(done)
    } // grabArbres

    grabAliasWithCle()
  } // run
}

'use strict'

const flow = require('an-flow')
// const config = require('../../config')
// const myBaseId = config.application.baseId

const updateNum = __filename.substring(__dirname.length + 1, __filename.length - 3)
const updateLog = require('an-log')('update' + updateNum)

const name = 'Ajout des relations contientRefA dans les arbres (et met à jour aussi les ressources liées, locales ou distantes)'

module.exports = {
  name,
  description: 'Les arbres contiennent depuis la v2.0.0 toutes les relations vers leurs enfants, cet update les met tous à jour en base (via un save)',
  run: function run (done) {
    const EntityRessource = lassi.service('EntityRessource')
    const $ressourceRepository = lassi.service('$ressourceRepository')
    let nbArbres = 0
    let nbRessOk = 0
    let nbRessMod = 0

    const limit = 50
    let skip = 0

    // met à jour les relations / relationsTitres de toutes les relations ajoutées ou supprimées
    // (locales ou distantes), donc normalement chez tous les enfants aussi.
    const grabArbres = () => {
      flow().seq(function () {
        EntityRessource.match('type').equals('arbre').sort('oid').grab({ skip, limit }, this)
      }).seqEach(function (arbre) {
        $ressourceRepository.save(arbre, this)
      }).seq(function (arbres) {
        nbArbres += arbres.length
        if (arbres.length < limit) {
          updateLog(`${nbArbres} arbres avec relations mises à jour, on passe aux autres ressources`)
          // on passe à grabOthers
          skip = 0
          process.nextTick(grabOthers)
        } else {
          updateLog(`${nbArbres} arbres traités`)
          skip += limit
          process.nextTick(grabArbres)
        }
      }).catch(done)
    } // grabArbres

    // faut compléter relationsTitres chez toutes les autres ressources qui ont des relations
    const grabOthers = () => {
      flow().seq(function () {
        EntityRessource
          .match('type').notEquals('arbre')
          .match('relations').isNotNull()
          .sort('oid').grab({ skip, limit }, this)
      }).seqEach(function (ressource) {
        // si on a le même nombre de rid dans relations et de relationsTitres on passe au suivant sans regarder le détail
        const ridsRelated = new Set(ressource.relations.map(r => r[1]))
        if (ressource.relationsTitres && Object.keys(ressource.relationsTitres).length === ridsRelated.size) {
          nbRessOk++
          return this()
        }
        nbRessMod++
        $ressourceRepository.save(ressource, (error) => {
          if (error) {
            log.error(error)
            return this(error)
          }
          // on ajoute un delai pour ne pas sauvegarder la même ressource trop vite (si plusieurs relations pointent dessus)
          // mais il faut quand même faire sauter le check dans le save
          setTimeout(this, 100)
        })
      }).seq(function (ressources) {
        if (ressources.length < limit) {
          updateLog(`FIN avec ${nbRessMod} ressources avec relations mises à jour et ${nbRessOk} ok`)
          done()
        } else {
          updateLog(`${nbRessMod} ressources avec relations mises à jour et ${nbRessOk} ok`)
          skip += limit
          process.nextTick(grabOthers)
        }
      }).catch(done)
    }

    grabArbres()
  } // run
}

const appConfig = require('../config')
const ressourceConfig = require('../ressource/config')

/**
 * Réexporte à plat des éléments de configuration d'origine variée
 * @module
 * @type {{baseId: string, baseUrl: string, listeMax: number, listeNbDefault: number, sesatheques: Array}}
 */
module.exports = {
  baseId: appConfig.application.baseId,
  baseUrl: appConfig.application.baseUrl,
  listeMax: ressourceConfig.limites.listeMax,
  listeNbDefault: ressourceConfig.limites.listeNbDefault,
  sesatheques: appConfig.sesatheques
}

/**
 * Ajoute une erreur à la ressource (en créant le tableau $errors s'il n'existait pas)
 * @param {Ressource} ressource
 * @param {string} error
 */
function addError (ressource, error) {
  if (!ressource.$errors) ressource.$errors = []
  if (typeof error === 'string') ressource.$errors.push(error)
  else ressource.$errors.push(error.toString())
}

/**
 * Ajoute un warning à la ressource
 * - crée le tableau $warnings s'il n'existait pas
 * - n'ajoute rien si le warning y était déjà
 * @param {Ressource} ressource
 * @param {string} warning
 */
function addWarning (ressource, warning) {
  if (!ressource.$warnings) ressource.$warnings = []
  if (!ressource.$warnings.includes(warning)) ressource.$warnings.push(warning)
}

/**
 * Retourne les rid de tous les enfants
 * @param arbre
 * @returns {Array}
 */
function getRidEnfants (ressource) {
  // on veut toutes les refs récursivement
  function addRids (enfants) {
    enfants.forEach(enfant => {
      if (!enfant) return log.dataError('enfant invalide dans', ressource)
      if (enfant.aliasOf) rids.add(enfant.aliasOf)
      if (enfant.enfants && enfant.enfants.length) addRids(enfant.enfants)
    })
  }
  if (!ressource) throw new Error('getRidEnfants appelé sans ressource')
  if (!ressource.enfants || !ressource.enfants.length) return []
  // on peut chercher
  const rids = new Set()
  addRids(ressource.enfants)

  return Array.from(rids)
}

module.exports = {
  addError,
  addWarning,
  getRidEnfants
}

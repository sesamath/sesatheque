/**
 * module de log pour écrire dans un fichier infos/warning/error de l'application,
 * celles liées à des incohérences de données, du processing, etc.
 * Les plantages de code devraient continuer à être écrit sur le stderr "normal" via console.error
 * mais ils peuvent aussi être mis dans un autre log (utiliser appLog.getLogger())
 */
const { createWriteStream, existsSync, mkdirSync } = require('node:fs')
const { join } = require('node:path')

const { formatDateTime } = require('sesajs-date')
const { stringify } = require('sesajstools')

const config = require('../config')
let logDir = config.appLog?.dir || config.logs?.dir
if (!logDir) {
  throw Error('il manque logs.dir dans la config')
}
if (config.appLog?.addBaseIdSuffix) logDir += '.' + config.application.baseId
if (!existsSync(logDir)) mkdirSync(logDir, { recursive: true, mode: 0o775 })

const defaultLogLevel = config.appLog?.this.logLevel ?? 'warning'

/**
 * La liste des niveaux de log possible
 * @private
 */
const levels = {
  debug: 0,
  notice: 1,
  info: 1,
  warn: 2,
  warning: 2,
  error: 3
}

const streamOptions = {
  flags: 'a',
  mode: '0644',
  autoClose: true, // pas besoin d'appeler stream.end() au shutdown lassi
  emitClose: false
}

class Logger {
  constructor (name, { logLevel = defaultLogLevel } = {}) {
    /**
     * Le logLevel courant, modifiable via setLoglevel
     * @type {number}
     * @private
     */
    this.logLevel = 2
    this.setLogLevel(logLevel)

    if (!/\.log/.test(name)) name += '.log'
    /**
     * stream vers le fichier de log
     * @type {node:fs.WriteStream}
     * @private
     */
    this.stream = createWriteStream(join(logDir, name), streamOptions)

    // et on retourne une fonction pour pouvoir être appelé sans devoir ajouter le suffixe info ou log
    return this.info.bind(this)
  }

  /**
 * Écrit dans le log en préfixant par la date (avec ms)
 * @private
 */
  _write (...args) {
    if (!args[0]) return console.error(Error('Le premier argument passé à appLog ne doit pas être falsy, arguments reçus : '), ...args)
    const prefix = `[${formatDateTime()}] `
    const content = args
      .map(arg => {
        if (arg instanceof Error && arg.logged) {
          return arg.message + ' (stack already logged)'
        }
        return typeof arg === 'object' ? stringify(arg) : arg
      })
      .join(' ')
    this.stream.write(prefix + content + '\n')
    // et on marque les erreurs comme loggées (on pouvait pas le faire dans le map sinon ça log aussi cette propriété)
    for (const arg of args) {
      if (arg instanceof Error) arg.logged = true
    }
  }

  /**
 * Ajoute un message de debug dans app.log (si logLevel ≥ debug, sinon ne fait rien)
 * Nombre d'arguments variables
 */
  debug = (...args) => {
    if (this.logLevel > 0) return
    args.unshift('debug')
    this._write.apply(null, args)
  }

  /**
 * Ajoute un message dans app.log (si logLevel ≥ info, sinon ne fait rien)
 * Nombre d'arguments variables
 */
  info () {
    if (this.logLevel > 1) return
    this._write.apply(null, arguments)
  }

  /**
 * Ajoute un message de warning dans app.log (si logLevel ≥ warning, sinon ne fait rien)
 * Nombre d'arguments variables
 */
  warn = (...args) => {
    if (this.logLevel > 2) return
    args.unshift('WARNING')
    this._write.apply(null, args)
  }

  /**
 * Ajoute un message d'erreur dans app.log
 * Nombre d'arguments variables
 */
  error = (...args) => {
    args.unshift('ERROR')
    this._write.apply(null, args)
  }

  /**
 * Change le logLevel courant
 * @param {string} level passer debug|info|warning|error
 */
  setLogLevel = (level) => {
    if (level in levels) {
      this.logLevel = levels[level]
    } else {
      console.error(Error(`level ${level} invalide`))
    }
  }
}

const loggers = {
  app: new Logger('app')
}

/**
 * Ajoute un message dans app.log (si logLevel ≥ info, sinon ne fait rien)
 * Nombre d'arguments variables
 */
const appLog = loggers.app

// et on lui ajoute une méthode getLogger (à cette instance)
/**
 * Retourne un nouveau logger (le même si on avait déjà demandé un logger avec ce nom)
 * @param {string} name
 * @param {Object} [options]
 * @param {string} [options.logLevel=warning]
 * @return {Logger}
 */
appLog.getLogger = (name, options) => {
  if (!loggers[name]) {
    loggers[name] = new Logger(name, options)
  }
  return loggers[name]
}

module.exports = appLog

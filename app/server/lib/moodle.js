/**
 * Génération de gift pour moodle (module utilisé par le controleur /export et la commande cli exportMoodle)
 * @module
 */
const { addSesatheques, getBaseUrl } = require('sesatheque-client/dist/server/sesatheques')
const { getNormalizedFileName } = require('./normalize')
const { application: { baseUrl }, sesatheques: mySesatheques } = require('../config')

const getDateFr = () => {
  const now = new Date()
  return now.getDate().toString().padStart(2, '0') + '/' + (now.getMonth() + 1).toString().padStart(2, '0') + '/' + now.getFullYear()
}

// pour node < 18 qui n'a pas encore fetch nativement (en fait on le met pour tous les node car node 18 sort un warning pénible à chaque usage)
if (typeof fetch !== 'function' || typeof window === 'undefined') {
  globalThis.fetch = require('node-fetch')
}

// on ajoute nos sésathèques
try {
  addSesatheques(mySesatheques)
} catch (error) {
  console.error(error)
}

const maxGiftSize = 1e7 // 10Mo max

// @todo à déplacer dans nodeClient
async function fetchPublicRef (rid, { wrapError } = {}) {
  try {
    if (!rid) throw Error('identifiant manquant')
    const [baseId, oid] = rid.split('/')
    if (!baseId || !oid) throw Error(`identifiant ${rid} invalide`)
    const baseUrl = getBaseUrl(baseId)
    const url = `${baseUrl}api/public/${oid}?format=ref`
    // @todo améliorer la gestion d'erreur
    const response = await fetch(url)
    const { data: ref } = await response.json()
    return ref
  } catch (error) {
    if (wrapError) return { titre: error.message, type: 'error', parametres: { error } }
    // sinon on forward l'erreur
    throw error
  }
}
// @todo à déplacer dans nodeClient
async function fetchEnfants (ref) {
  if (typeof ref.enfants === 'string') ref = await fetchPublicRef(ref.enfants) // c'est un rid
  if (ref.aliasOf && !ref?.enfants?.length) {
    ref = await fetchPublicRef(ref.aliasOf)
  }
  if (!Array.isArray(ref.enfants)) {
    // un dossier vide dans un arbre n'a pas de propriété enfants, faut pas râler dans ce cas
    if (typeof ref.enfants !== 'undefined') console.error(Error(`Ref invalide (${ref.aliasOf} ${ref.titre})`))
    ref.enfants = []
  }
  // on zap les erreurs contenues dans un arbre (un dossier qui pointe vers un truc qui n'existe plus
  const enfants = ref.enfants.filter(ref => ref.type !== 'error')
  return Promise.all(enfants.map(enfant => (typeof enfant === 'string') ? fetchPublicRef(enfant) : enfant))
}
// @todo à déplacer dans nodeClient
function getPublicDisplayUrl (rid) {
  try {
    const [baseId, oid] = rid.split('/')
    const baseUrl = getBaseUrl(baseId)
    return `${baseUrl}public/voir/${oid}`
  } catch (error) {
    console.error(error)
    return ''
  }
}

/**
 * Génère le code Gift pour un arbre de ressources
 * @param {Ressource|ClientItem} tree
 * @param {Object} [options]
 * @param {string} [options.path]
 * @return {string}
 */
async function exportTreeAsGift (tree, { path = '' } = {}) {
  if (tree.type !== 'arbre') throw Error('Cette ressource n’est pas un arbre')
  if (path) path += '/'
  path += tree.titre.replaceAll('/', '//') // on échappe les / en les doublant
  let gift = ''
  let childs
  try {
    childs = await fetchEnfants(tree)
    for (const child of childs) {
      try {
        gift += await exportRefAsGift(child, { path }) + '\n\n'
      } catch (error) {
        console.error(Error(`Pb dans l'export gift du child ${child} de l’arbre ${tree.titre} (${tree.rid || tree.aliasOf}) pour l’enfant`), error)
      }
      if (gift.length > maxGiftSize) {
        gift += `\n// Taille limite de ${maxGiftSize / 1e6}Mo dépassée, export stoppé à ${Math.round(gift.length / 1e5) / 10}Mo pour l'arbre ${tree.rid || tree.aliasOf} (${tree.titre})\n`
        break
      }
    }
  } catch (error) {
    console.error(Error(`Pb dans l'export gift de l’arbre ${tree.titre} (${tree.rid || tree.aliasOf})`), error)
  }
  return gift
}

/**
 * Génère le code Gift d'une ressource
 * @param {Ref} ref
 * @param {Object} [options]
 * @param {string} [options.path]
 * @returns {string}
 */
async function exportRefAsGift (ref, { path = '' } = {}) {
  const { type, titre, aliasOf } = ref
  const giftEscape = s => s.replace(/[~=#{}:]/g, '\\$&') // échappement des caratères spéciaux pour les GIFT
  if (type === 'arbre') return exportTreeAsGift(ref, { path })
  if (type === 'error') throw Error(titre)
  if (!aliasOf) {
    console.warn('ref invalide', ref)
    throw Error('ref invalide (sans aliasOf)')
  }
  // ajout d'un commentaire avec la date de l'export
  let gift = path ? '' : `// Export de la Sesatheque pour Moodle au format GIFT généré le ${getDateFr()}\n\n`
  const url = getPublicDisplayUrl(aliasOf)
  if (!url) return `// ERROR Impossible de trouver l’url publique pour afficher la ressource ${aliasOf}`
  if (path) gift += '$CATEGORY: ' + giftEscape(path) + '\n\n'
  gift += `:: Sesatheque - ${giftEscape(titre)} ::
<script src\\="${giftEscape(baseUrl)}/moodle.module.js" type\\="module"></script>
<sesatheque-moodle url\\="${giftEscape(url)}" />
{
=%100%100=%90%90=%80%80=%70%70=%60%60=%50%50=%40%40=%30%30=%20%20=%10%10=%0%0
}`
  return gift
}

/**
 * Génère le code Gift d'une ressource
 * @param {Ressource} ressource
 * @param {Object} [options]
 * @param {string} [options.path]
 * @returns {string}
 */
async function exportRessourceAsGift (ressource, { path = '' } = {}) {
  // y'a que ça qui nous intéresse, on passe pas par le constructeur Ref pour si peu
  const { type, titre, rid } = ressource
  return exportRefAsGift({ type, titre, aliasOf: rid }, { path })
}

const getGiftFilename = (rid, titre) => getNormalizedFileName(`${rid.replace('/', '-')}-${titre}.gift.txt`)

module.exports = {
  exportRefAsGift,
  exportRessourceAsGift,
  fetchPublicRef,
  getGiftFilename
}

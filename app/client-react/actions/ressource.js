import { push } from 'connected-react-router'

import { DELETE, GET, POST } from '../utils/httpMethods'
import { addNotification } from './notifications'
import getUrls from 'sesatheque-client/src/getUrls'
import serverConfig from '../../server/config'
import { getRessourceCloneUrl, getRessourceUrl, getForkAliasUrl, getRestoreArchiveUrl } from '../apiRoutes'

const { baseUrl } = serverConfig
/**
 * Retourne une fct à mettre dans un catch de promesse
 * @private
 * @param dispatch
 * @param {string} [message] un '%s' éventuel sera remplacé par error.message
 */
const getErrorCatcher = (dispatch, message = 'Une erreur est survenue (%s)') => error => {
  console.error(error)
  // si message contient %s on le remplace par le message d'erreur
  dispatch(addNotification({
    level: 'error',
    message: message.replace('%s', error.message)
  }))
}

/**
 * Propage une notif info
 * @private
 * @param dispatch
 * @param {string} message
 */
const addInfo = (dispatch, message) => dispatch(addNotification({ level: 'info', message }))

/**
 * Une fct qui ne fait rien
 * @private
 */
const dummy = () => {}

/**
 * Retourne l'action de type SET_RESSOURCE pour affecter une ressource dans le store
 * @param ressource
 * @return {{type: string, ressource: Ressource}}
 */
const setRessource = (ressource) => {
  // cet appel récupère les urls et plantera si on a ni oid ni aliasOf, ou si
  ressource._urls = getUrls(ressource, baseUrl)

  return {
    type: 'SET_RESSOURCE',
    ressource
  }
}

/**
 * Retourne l'action de type CLEAR_RESSOURCE pour retirer la ressource du store
 * @return {{type: string}}
 */
const clearRessource = () => ({
  type: 'CLEAR_RESSOURCE'
})

/**
 * Retourne l'actionCreator qui clone la ressource
 * @param {string} oid
 * @param {function} success cb appelée si ok
 * @returns {promisedThunk} qui supprime puis dispatch clearRessource & redirect
 */
export const cloneRessource = (oid, success) => (dispatch) => GET(getRessourceCloneUrl({ oid }))
  .then((ressource) => {
    if (!ressource || !ressource.oid) throw Error('La réponse n’est pas au format attendu')
    dispatch(setRessource(ressource))
    addInfo(dispatch, 'La ressource a été dupliquée')
    success(ressource.oid)
  })
  .catch(getErrorCatcher(dispatch, 'La duplication a échouée : %s'))

/**
 * Retourne l'actionCreator qui supprime la ressource
 * @param {string} oid
 * @param {function} success cb appelée si ok
 * @returns {promisedThunk} qui supprime puis dispatch clearRessource & redirect
 */
export const deleteRessource = (oid, success) => (dispatch) => DELETE(getRessourceUrl({ oid }))
  .then(() => {
    addInfo(dispatch, 'La ressource a été supprimée')
    return success()
  })
  .catch(getErrorCatcher(dispatch, 'La suppression a échouée : %s'))

/**
 * @callback promisedThunk
 * @param dispatch
 * @param getState
 * @return {Promise}
 */

/**
 * Retourne l'actionCreator qui transforme l'alias en ressource (pour l'éditer)
 * @param {string} oid
 * @return {promisedThunk}
 */
export const forkAlias = (oid) => (dispatch) => GET(getForkAliasUrl({ oid }))
  .then(ressource => dispatch(setRessource(ressource)))
  .catch(getErrorCatcher(dispatch, 'Impossible d’éditer cette ressource : %s'))

/**
 * Retourne l'actionCreator qui va sauvegarder la ressource via un POST sur l'api
 * @param {Ressource} ressource
 * @param {function} [success] cb appelée si ok
 * @return {promisedThunk} qui va faire le post puis dispatch de setRessource & addNotification
 */
export const saveRessource = (ressource, success = dummy) => (dispatch) => POST(getRessourceUrl({ format: 'full' }), { body: ressource })
  .then((responseRessource) => {
    if (responseRessource.$warnings) {
      responseRessource.$warnings.forEach(message => dispatch(addNotification({ level: 'warning', message })))
    }
    dispatch(setRessource(responseRessource))
    addInfo(dispatch, 'La ressource a été sauvegardée')
    return success(responseRessource)
  })
  .catch(getErrorCatcher(dispatch, 'La sauvegarde a échoué : %s'))

export const restoreArchive = (oid) => (dispatch) => GET(getRestoreArchiveUrl({ oid }))
  .then(ressource => dispatch(setRessource(ressource)))
  .then(() => dispatch(push(`/ressource/decrire/${oid}`)))
  .catch(getErrorCatcher(dispatch, 'Impossible de restaurer cette archive : %s'))

/**
 * Retourne l'actionCreator qui charge une ressource via GET sur l'api
 * @param oid
 * @return {function(*, *): Promise<any>}
 */
export const loadRessource = (oid) => (dispatch, getState) => {
  // shorcut si on a déjà la bonne ressource
  const currentRessource = getState().ressource
  if (currentRessource && currentRessource.oid === oid) return Promise.resolve() // on retourne une promesse résolue pour être toujours async

  // On lance toujours un clear avant load pour ne pas garder l'ancienne dans le store
  // (au cas où le load plante)
  // Si le dispatch throw (à cause d'un reducer qui plante) ça remontera (sans renvoyer de promesse)
  return Promise.resolve(dispatch(clearRessource()))
    .then(() => GET(getRessourceUrl({ oid, format: 'full' })))
    .then(ressource => dispatch(setRessource(ressource)))
    .catch(getErrorCatcher(dispatch, 'Impossible de charger la ressource : %s'))
}

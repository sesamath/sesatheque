import { deleteRessource, cloneRessource } from '../actions/ressource'

/**
 * Fonction de suppression d'une ressource (demandera confirmation avant de le faire)
 * @typedef CbDelete
 * @param {Object} ressource
 * @param {string} ressource.oid
 * @param {string} ressource.titre
 * @return {void}
 */
/**
 * Retourne la fct de suppression
 * @param dispatch
 * @param success
 * @returns {CbDelete}
 */
export const askDelete = (dispatch, success) => ressource => {
  // eslint-disable-next-line no-alert
  if (confirm(`Êtes vous sûr de vouloir supprimer cette ressource « ${ressource.titre} » ?`)) {
    return dispatch(deleteRessource(ressource.oid, success))
  }
}

export const askClone = (dispatch, success) => oid => {
  // eslint-disable-next-line no-alert
  if (confirm('Êtes vous sûr de vouloir dupliquer cette ressource ?')) {
    return dispatch(cloneRessource(oid, success))
  }
}

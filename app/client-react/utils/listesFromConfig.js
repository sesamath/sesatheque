import sjt from 'sesajstools'

// attention, on importe ça avant override de la conf avec _private
// Si on voulait pouvoir faire de l'override aussi là-dessus il faudrait gérer tout ça au runtime
// (retourner cette conf lors de l'apel de /api/personne/current et la stocker dans le store redux)
// Pour le moment on en a pas le besoin et c'est plus simple et performant comme ça.
import { editable, listes, listesOrdonnees, typePerso } from '../../server/ressource/config'

const { hasProp } = sjt

/**
 * Les listes sous forme de tableaux ordonnés avec label & value
 * On ajoute une liste editableTypes (types filtrés)
 * @type {{[listName]: string, ListeItem[]}}
 */
const formattedLists = {}

for (const [key, values] of Object.entries(listes)) {
  if (hasProp(listesOrdonnees, key)) {
    formattedLists[key] = listesOrdonnees[key].map(n => ({
      label: values[n],
      value: n
    }))
  } else {
    formattedLists[key] = Object.entries(values).map(([value, label]) => ({
      value,
      label
    }))
  }
}

formattedLists.editableTypes = formattedLists.type.filter(({ value }) => editable[value])
formattedLists.createTypes = formattedLists.type.filter(({ value }) => typePerso[value])

export default formattedLists

import Bugsnag from '@bugsnag/js'
import FingerprintJS from '@fingerprintjs/fingerprintjs'

import config from '../../server/config'
import getParentUrls from './getParentUrls'

const { application, bugsnag, version } = config
const apiKeyByType = bugsnag?.apiKeyByType || {}

let userId = Date.now()
// Initialize an agent at application startup.
FingerprintJS
  .load()
  .then(fp => fp.get())
  .then((result) => {
    if (result?.visitorId) userId = result.visitorId
    else console.error(Error('Impossible de déterminer un hash anonyme pour ce user, on utilise le timestamp de démarrage '))
  })

/**
 * Appelé avant d'envoyer le rapport, pour filtrer
 * @param event
 * @return {boolean}
 */
function onError (event) {
  // cf https://docs.bugsnag.com/platforms/browsers/js/customizing-error-reports/
  if (/local/.test(window.location.hostname)) return false
  if (/^file:\/\//.test(event.request.url)) return false
  // on étudie la 1re erreur
  const { errorClass, errorMessage, stacktrace } = event.errors[0]
  // on vire les pbs réseau
  if (/NS_ERROR_FAILURE/.test(errorClass)) {
    // eslint-disable-next-line no-alert
    alert('Problème de connexion réseau, si vous constatez des anomalies vous devriez recharger la ressource ou la page')
    return false
  }
  if (/ChunkLoadError/.test(errorClass)) {
    // eslint-disable-next-line no-alert
    alert('Il y a un problème de chargement de l’application, vous devriez essayer de vider le cache de votre navigateur pour le résoudre (en général ctrl+maj+suppr puis cocher « fichiers en cache », ça peut être différent suivant les navigateurs).')
    return false
  }

  // les erreurs de navigateurs trop vieux
  try {
    // si on teste import.meta !== 'object' ou import.meta?.url !== 'string' alors webpack râle avec
    // Accessing import.meta directly is unsupported (only property access or destructuring is supported)
    if (typeof import.meta.url !== 'string' || /dynamic.*import/.test(errorMessage)) {
      return false
    }
  } catch (error) {
    console.error(error)
    return false
  }

  // les fichiers concernés
  const files = stacktrace.map(({ file }) => file).filter(Boolean)

  // si y'a que du vendors (mathquill ou Blockly ou …) ou des lib tierces de node_modules => on ignore
  // (sert pas à grand chose dans la plupart des cas car file est le nom du fichier
  // minifié, souvent app.js même si ça correspond à un bout de code d'une dépendance)
  // Pour j3p toutes les dépendances sont dans du vendor-xxx
  if (files.length && files.every(file => /(vendor|node_modules|externals)/.test(file) && !file.includes('sesamath'))) return false
  // et les traces avec une seule ligne unknown, sans intérêt (en général c'est IE qui envoie du "Accès refusé" ou "Erreur non spécifiée"
  if (files.every(file => /unknown/i.test(file))) return false
  if (files.some(f => /(vendors|node_modules|unknown)/.test(f))) {
    // on ajoute ça pour comprendre pourquoi on a des trucs qui passent au travers de nos filtres
    event.addMetadata('request', { files })
  }

  // on vire tous les plantages qui concernent ce genre de pbs
  if (stacktrace.some(({ file, errorClass }) =>
    // une extension firefox ou chrome
    /^(chrome|moz)-extension:\/\//.test(file) ||
    // un fichier es5 (trop de vieux navigateurs avec des comportements loufoques)
    /[./]es5[./]/.test(file) ||
    // ou un pb ruffle
    /ruffle/.test(file) ||
    // ou un pb réseau
    /NS_ERROR_FAILURE/.test(errorClass)
  )) {
    return false
  }

  // on regarde suivant le contexte (cf https://docs.bugsnag.com/platforms/javascript/customizing-error-reports/#getmetadata)
  const type = event.getMetadata('exo', 'type')

  if (!type || ['am', 'em'].includes(type)) {
    // si pas de type on teste quand même ce qui suit
    // apparemment le flash tente de lire des trucs sur la fenêtre parente, et il a pas le droit
    if (/Permission denied to (get|access) property/.test(errorMessage)) return false
    if (/Accès refusé/.test(errorMessage)) return false
    if (/attempted to read protected variable/.test(errorMessage)) return false
  }
  // on ignore pour le moment les erreurs des js de calculatice, y'en a un peu trop…
  if (type === 'ecjs') {
    // des erreurs fréquentes sur ecjs qu'on regardera le jour où on aura la main sur le code
    if (/BigError/.test(errorClass)) return false
    if (/Unable to get property/.test(errorMessage)) return false
    // on vire tous les rapport qui ont du replication_calculatice dans la stacktrace
    if (stacktrace.some(stackFrame => /replication_calculatice/.test(stackFrame.file))) return false
  } else if (type === 'j3p') {
    // mathlive déconne avec ça
    if (/Can't find variable: AudioContext/.test(errorMessage)) return false
  }

  // si y'a une apiKey spécifique à ce type on l'ajoute
  // cf https://docs.bugsnag.com/platforms/javascript/customizing-error-reports/#apikey
  if (apiKeyByType[type]) event.apiKey = apiKeyByType[type]
  if (window[`${type}Version`] && event.app) event.app.version = window[`${type}Version`]
  // si on est toujours là on ajoute ça avant d'envoyer
  event.addMetadata('request', { frames: getParentUrls() })
  // et on ajoute un userId anonyme pour que toutes les notifs n'apparaissent pas comme venant du même utilisateur
  // (ce qui serait le cas avec notre collectUserIp: false)
  event.setUser(userId)
  // en cas de pb avec des données personnelles envoyées cf https://docs.bugsnag.com/api/data-access/gdpr-examples/
}

/**
 * Retourne un client bugsnag si on a une apiKey en config, null sinon
 * @return {Client|null}
 */
export default function getBugsnagClient (config = {}) {
  if (!bugsnag?.apiKey) return null
  const defaultConfig = {
    // https://docs.bugsnag.com/platforms/browsers/js/configuration-options/#apikey
    apiKey: bugsnag.apiKey,
    // cfhttps://docs.bugsnag.com/platforms/javascript/angular/customizing-error-reports/#preventing-ip-address-collection
    collectUserIp: false,
    // https://docs.bugsnag.com/platforms/javascript/customizing-error-reports/
    onError,
    // https://docs.bugsnag.com/platforms/browsers/js/configuration-options/#appversion
    appVersion: version,
    // https://docs.bugsnag.com/platforms/browsers/js/configuration-options/#releasestage
    releaseStage: application.staging
  }
  Bugsnag.start({ ...defaultConfig, ...config })
  return Bugsnag
}

/* version module du client react (cf polyfill.js pour les ajouts destinés aux vieux navigateurs, ajouté par webpack pour la version nomodule) */
import { createElement } from 'react'
import { render } from 'react-dom'
import App from './App'
import checkBrowser from './utils/checkBrowser'

checkBrowser()
render(createElement(App), document.getElementById('root'))

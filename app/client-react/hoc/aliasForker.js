import { goBack } from 'connected-react-router'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { forkAlias } from '../actions/ressource'
import getDisplayName from '../utils/getDisplayName'

const mapDispatchToProps = (dispatch) => ({
  ensuresIsForked: ({ oid, aliasOf }) => {
    if (!aliasOf) return

    // eslint-disable-next-line no-alert
    if (confirm('Cette ressource est un alias, une copie va donc être créée, voulez-vous continuer ?')) {
      dispatch(forkAlias(oid))
    } else {
      dispatch(goBack())
    }
  }
})

const mapStateToProps = ({ ressource }) => ({ ressource })

/**
 * High Order Component pour déclencher automatiquement un fork si on édite un alias
 * @param {Component} WrappedComponent
 * @return {Component}
 */
const aliasForker = (WrappedComponent) => {
  class AliasForker extends Component {
    componentDidMount () {
      this.props.ensuresIsForked(this.props.ressource)
    }

    componentDidUpdate () {
      this.props.ensuresIsForked(this.props.ressource)
    }

    render () {
      // à l'édition d'un alias on ne rend rien, en attendant l'effet du dispatch(forkAlias)
      if (this.props.ressource.aliasOf) {
        return null
      }

      return (
        <WrappedComponent {...this.props} />
      )
    }
  }

  AliasForker.displayName = `aliasForker(${getDisplayName(WrappedComponent)})`

  AliasForker.propTypes = {
    ensuresIsForked: PropTypes.func,
    ressource: PropTypes.shape({
      aliasOf: PropTypes.string,
      oid: PropTypes.string
    })
  }

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(AliasForker)
}

export default aliasForker

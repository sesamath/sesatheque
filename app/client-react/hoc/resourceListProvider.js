import PropTypes from 'prop-types'
import queryString from 'query-string'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import addNotifyToProps from './addNotifyToProps'
import { GET } from '../utils/httpMethods'
import { listes } from '../../server/ressource/config'
import { getRessourceListUrl } from '../apiRoutes'
import getDisplayName from '../utils/getDisplayName'

const limitDefault = 25
const limitMin = 5
const limitMax = 100

const emptyState = {
  resources: [],
  total: 0
}

const errorState = {
  resources: [],
  total: 0,
  error: 'Une erreur est survenue lors de l’appel pour récupérer la liste de ressources.'
}

/**
 * High Order Component qui se base sur les query params pour enrichir le composant donné
 * en ajoutant les props resources et total
 * @param {Component} WrappedComponent
 * @return {Component} Le composant enrichi
 */
const resourceListProvider = (WrappedComponent) => {
  class ResourceListProvider extends Component {
    constructor (props) {
      super(props)
      this.state = emptyState
    }

    fetchList () {
      const { query, queryOptions, notify } = this.props
      let xhrQuery
      if (query.mots) {
        // il faut transformer ça en un array et le passer à fulltext
        const fulltext = []
        // on sort les expressions d'abord
        let toParse = query.mots
        let chunks
        // eslint-disable-next-line no-cond-assign
        while (chunks = /^([^"]*)"([^"]*)"(.*)/.exec(toParse)) {
          if (chunks[2]) fulltext.push(chunks[2])
          toParse = chunks[1] + ' ' + chunks[3]
        }
        // et on ajoute les autres mots s'il en reste
        if (toParse) fulltext.push(...toParse.split(' '))
        if (fulltext.length) {
          // faut pas modifier props.query
          xhrQuery = { ...query, fulltext }
          delete xhrQuery.mots
        }
      } else {
        xhrQuery = query
      }
      const search = queryString.stringify({
        ...xhrQuery,
        ...queryOptions,
        format: 'light'
      })
      const url = getRessourceListUrl({ search })

      GET(url)
        .then(({ liste, total }) => {
          this.setState({
            resources: Object.values(liste),
            total
          })
        })
        .catch((error) => {
          // en notif l'erreur envoyée par le serveur ou construite par httpMethods
          notify({ message: error.message, level: 'error' })
          // en console l'erreur complète
          console.error(error)
          // et sur la page un message plus userFriendly et général
          this.setState(errorState)
        })
    }

    componentDidMount () {
      if (this.props.search) this.fetchList(this.props.query)
    }

    componentDidUpdate ({ search }) {
      if (search !== this.props.search) {
        if (this.props.search) this.fetchList()
        else this.setState(emptyState)
      }
    }

    render () {
      return (
        <WrappedComponent
          {...this.props}
          resources={this.state.resources}
          total={this.state.total}
          error={this.state.error}
          refreshList={this.fetchList.bind(this)}
        />
      )
    }
  }

  ResourceListProvider.displayName = `resourceListProvider(${getDisplayName(WrappedComponent)})`

  ResourceListProvider.propTypes = {
    // fourni par le connect mapStateToProps (ça vient du router)
    hash: PropTypes.string,
    search: PropTypes.string,
    // construit à partir de search (par buildQuery)
    query: PropTypes.object,
    queryOptions: PropTypes.shape({
      skip: PropTypes.number,
      limit: PropTypes.number
    }),
    notify: PropTypes.func
  }

  /**
   * Retourne l'objet query, qui ne contient que les critères de recherche non vide, avec le bon type
   * @param {object} parsedSearch la queryString parsée
   * @return {object} L'objet query nettoyé et typé
   */
  const buildQuery = (parsedSearch) => {
    if (!parsedSearch) {
      return {
        publie: true,
        restriction: 0
      }
    }
    // on normalise query, en ne conservant que les entrées utiles
    // avec cast en int pour les index numériques et affectation des valeurs par défaut
    const query = {}
    // cas particuliers pour oid & origine+idOrigine, si présents ils doivent être les seuls
    // (ça ne peut remonter qu'un seul résultat, inutile d'ajouter des critères qui pourraient le masquer)
    if (parsedSearch.oid) {
      query.oid = parsedSearch.oid
    } else if (parsedSearch.origine && parsedSearch.idOrigine) {
      query.origine = parsedSearch.origine
      query.idOrigine = parsedSearch.idOrigine
    } else {
      // pour les valeurs uniques (select ou input), on récupère tel quel si non vide
      for (const prop of ['mots', 'titre', 'type', 'langue', 'auteurs', 'groupes']) {
        if (parsedSearch[prop]) query[prop] = parsedSearch[prop]
      }
      // multiselect
      for (const prop of ['categories', 'niveaux', 'typePedagogiques', 'typeDocumentaires']) {
        let values = parsedSearch[prop]
        if (!values) continue
        // on veut un array…
        if (!Array.isArray(values)) values = [values]
        if (!values.length) continue
        // … d'entiers (sauf pour niveaux), car ça vient de la queryString donc toutes les valeurs sont des strings
        if (prop === 'niveaux') {
          // on filtre sur les valeurs connues
          const niveaux = values.filter(value => listes.niveaux[value])
          if (niveaux.length) query.niveaux = niveaux
        } else {
          // cast integer
          values = values
            .filter(value => listes[prop][value]) // la prop existe
            .map(value => parseInt(value, 10)) // cast
          if (values.length) query[prop] = values
        }
      }

      // faire une recherche sur un auteur ou un groupe est le seul cas où on peut ne pas préciser publie et restriction
      if (query.auteurs || query.groupes) {
        if (parsedSearch.publie) query.publie = parsedSearch.publie !== 'false'
        if (parsedSearch.restriction) query.restriction = listes.restriction[parsedSearch.restriction] ? Number(parsedSearch.restriction) : 0
      } else {
        // cas standard, checkbox pour publié, à true par défaut
        query.publie = ['true', '', undefined].includes(parsedSearch.publie)
        // et restriction en int, à 0 par défaut
        query.restriction = listes.restriction[parsedSearch.restriction] ? Number(parsedSearch.restriction) : 0
      }
      // et on ajoute orderBy si on nous le demande dans l'url (en attendant d'avoir le form pour le faire)
      if (parsedSearch.orderBy) {
        query.orderBy = parsedSearch.orderBy
      }
    }

    return query
  }

  /**
   * Normalise skip & limit d'après la queryString
   * @param {object} parsedSearch la queryString parsée
   * @return {{skip: number, limit: number}}
   */
  const buildQueryOptions = (parsedSearch) => {
    if (!parsedSearch) return { skip: 0, limit: limitDefault }
    // on normalise skip et limit
    const queryOptions = {
      limit: Math.round(parsedSearch.limit) || limitDefault,
      skip: Math.round(parsedSearch.skip) || 0
    }
    // faut vérifier la cohérence limit/skip, les caster en number, leur affecter des valeurs
    // par défaut et ajuster skip si besoin
    // (sinon la pagination fait des trucs bizarre si "ça tombe pas juste" avec skip qui serait pas un multiple de perPage)
    if (queryOptions.limit < limitMin) queryOptions.limit = limitMin
    else if (queryOptions.limit > limitMax) queryOptions.limit = limitMax

    // normalize skip, doit être un multiple de limit, on rabote si besoin
    const offsetPage = queryOptions.skip % queryOptions.limit
    if (offsetPage) queryOptions.skip -= offsetPage
    if (queryOptions.skip < 0) queryOptions.skip = 0

    return queryOptions
  }

  // pour récupérer search d'après le router et construire query et queryOptions
  const mapStateToProps = ({ router: { location: { search, hash } } }) => {
    // si on ne demande rien on affiche le form et on ne cherche rien
    if (!search) {
      return { hash, query: {}, queryOptions: {}, search, allowAnyOption: false }
    }
    const parsedSearch = queryString.parse(search)
    const query = buildQuery(parsedSearch)
    const queryOptions = buildQueryOptions(parsedSearch)
    const allowAnyOption = query && ((query.auteurs || query.groupes) !== undefined)

    // màj search pour (c'est la sérialisation de query utilisée pour savoir
    // s'il faut mettre à jour les résultats)
    search = queryString.stringify({ ...query, ...queryOptions })

    return { hash, query, queryOptions, search, allowAnyOption }
  }

  return addNotifyToProps(connect(mapStateToProps, {})(ResourceListProvider))
}

export default resourceListProvider

import { goBack } from 'connected-react-router'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { restoreArchive } from '../actions/ressource'
import getDisplayName from '../utils/getDisplayName'

const mapDispatchToProps = (dispatch) => ({
  ensuresIsArchive: ({ oid, aliasOf }) => {
    if (!oid.includes('@')) return

    // eslint-disable-next-line no-alert
    if (confirm('Cette ressource est une archive, elle va être restaurée et créer une nouvelle version, voulez-vous continuer ?')) {
      dispatch(restoreArchive(oid))
    } else {
      dispatch(goBack())
    }
  }
})

const mapStateToProps = ({ ressource }) => ({ ressource })

/**
 * High Order Component pour déclencher automatiquement une récupération d'archive si on édite une archive
 * @param {ResourceForm} ResourceForm
 * @return {Component}
 */
const archiveRestorer = (ResourceForm) => {
  class ArchiveRestorer extends Component {
    componentDidMount () {
      this.props.ensuresIsArchive(this.props.ressource)
    }

    componentDidUpdate () {
      this.props.ensuresIsArchive(this.props.ressource)
    }

    render () {
      // si on édite une archive on ne rend rien (ça devrait pas arriver)
      if (this.props.ressource.oid.includes('@')) {
        return null
      }

      return (
        <ResourceForm {...this.props} />
      )
    }
  }

  ArchiveRestorer.displayName = `archiveRestorer(${getDisplayName(ResourceForm)})`

  ArchiveRestorer.propTypes = {
    ensuresIsArchive: PropTypes.func,
    ressource: PropTypes.shape({
      aliasOf: PropTypes.string,
      oid: PropTypes.string
    })
  }

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(ArchiveRestorer)
}

export default archiveRestorer

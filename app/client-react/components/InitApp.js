import PropTypes from 'prop-types'
import queryString from 'query-string'
import { Component } from 'react'
import { connect } from 'react-redux'
import { getCurrentSession } from '../actions/session'
import addNotifyToProps from '../hoc/addNotifyToProps'

import '../../client-common/registerSesatheques'

class InitApp extends Component {
  componentDidMount () {
    // On notifie si la query contient une erreur
    const { error } = queryString.parse(this.props.search)
    if (error !== undefined) {
      this.props.notify({
        level: 'error',
        message: error
      })
    }

    // chargement de la session
    const { getCurrentSession } = this.props
    getCurrentSession()
  }

  render () {
    return null
  }
}

InitApp.propTypes = {
  getCurrentSession: PropTypes.func,
  notify: PropTypes.func,
  search: PropTypes.string
}

const mapStateToProps = ({ router: { location: { search } } }) => ({ search })

const mapDispatchToProps = {
  getCurrentSession
}

export default addNotifyToProps(
  connect(mapStateToProps, mapDispatchToProps)(
    InitApp
  )
)

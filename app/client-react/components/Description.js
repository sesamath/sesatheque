import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import icons from 'plugins/icons'
import resourceLoader from '../hoc/resourceLoader'
import NavMenu from './NavMenu'
import ResourceHistory from './ResourceHistory'

import { baseId } from 'server/config'
import { getGiftFilename } from 'server/lib/moodle'
import { formats, listes, labels } from 'server/ressource/config'
import { getRestrictionString } from '../utils/labels'

import './Description.scss'
import { formatDate } from 'sesajs-date'

const isDateFr = formats.jour !== 'YYYY-MM-DD'
const dateFormat = (date) => formatDate({ date: new Date(date), fr: isDateFr })

/**
 * Remplace les \n par des <br key="i" />
 * str.replace(/\n/, '<br>') semble bcp plus simple, mais c'est pas du jsx et le html sera pas rendu
 * @private
 * @param {string} str
 * @return du jsx
 */
const nl2br = (str) => {
  if (!str) return null
  // Le 1er replace sert au cas où y'aurait du <br> dans la string d'origine.
  // Parenthèses capturantes dans la regex pour avoir les \n comme élément de tableau
  // (sinon un split('\n') suffisait mais ça obligeait à mettre du <Fragment> dans le retour du map)
  return str.replace(/<[bB][rR] *\/?>/g, '\n').split(/(\n)/g).map((part, index) => {
    return (part === '\n') ? (<br key={index} />) : part
  })
}

/**
 * Page de description d'une ressource
 * @type {PureComponent}
 * @return {Fragment}
 */
export const Description = ({ ressource }) => {
  const {
    _urls,
    titre,
    oid,
    rid,
    publie,
    restriction,
    aliasOf,
    dateCreation,
    dateMiseAJour,
    langue,
    niveaux,
    categories,
    typePedagogiques,
    typeDocumentaires,
    resume,
    description,
    commentaires,
    origine,
    idOrigine,
    type,
    version,
    _enfants = [],
    _auteurs = [],
    _contributeurs = [],
    _relations = [],
    groupes = []
  } = ressource
  // on précalcule quelques flags & labels pour la lisibilité

  // origine/idOrigine si y'a
  const externalId = (origine !== baseId && idOrigine) ? (<i> ({origine}/{idOrigine})</i>) : null

  // lien en target _blank
  const getLink = (type, label, attrs = {}) => {
    const url = _urls[type]
    if (!url) return null
    // target _blank par défaut
    if (!attrs.target) attrs.target = '_blank'
    // NavLink n'accepte que des urls locales
    if (url.startsWith('/')) return (<NavLink to={url} {...attrs}>{label}</NavLink>)
    return (<a href={url} {...attrs} rel="noopener noreferrer">{label}</a>)
  }

  const isArchive = /@[0-9]+$/.test(oid)
  const oidSansVersion = isArchive ? oid.replace(/@.*/, '') : oid

  return (
    <Fragment>
      <NavMenu ressource={ressource} />
      <div className="block ressource">
        <span className="btn fr tag">{publie ? 'Publié' : 'NON PUBLIÉ'}</span>
        <span className="btn fr tag">{labels.restriction}&nbsp;:&nbsp;{getRestrictionString({ restriction, groupes })}</span>

        <section className="grid-5 has-gutter clear-right">
          <div className="txtbold">{labels.oid}&nbsp;:</div>
          <div className="col-4">
            {rid}{externalId}{' '}
            {getLink('dataUrl', 'json')}{' '}
            {/* avec download: true on laisse le navigateur décider du nom de fichier et de l'extension d'après l'url, on préfère l'imposer */}
            {(publie && !restriction)
              ? getLink('moodleUrl', 'gift Moodle', { download: getGiftFilename(rid, titre) })
              : (<span>(pas de gift moodle car la ressource n’est pas publique)</span>)
            }
          </div>

          <div className="txtbold">{labels.type}&nbsp;:</div>
          <div className="col-4">{type}</div>
          <div className="txtbold">{labels.version}&nbsp;:</div>
          <div className="col-4">{version}</div>

          {aliasOf
            ? (
            <Fragment>
              <div className="txtbold"><strong>Alias de</strong></div>
              <div className="col-4">{getLink('originalDescribeUrl', titre)}</div>
            </Fragment>
              )
            : (
            <Fragment>
              <div className="txtbold">{labels.dateCreation}&nbsp;:</div>
              <div className="col-4">{dateFormat(dateCreation)}</div>

              <div className="txtbold">{labels.dateMiseAJour}&nbsp;:</div>
              <div className="col-4">{dateFormat(dateMiseAJour)}</div>

              <div className="txtbold">{labels.langue}&nbsp;:</div>
              <div className="col-4">{listes.langue[langue]}</div>

              <div className="txtbold">{labels.niveaux}&nbsp;:</div>
              <div className="col-4">{niveaux.map((niveau) => listes.niveaux[niveau]).join(', ')}</div>

              <div className="txtbold">{labels.typePedagogiques}&nbsp;:</div>
              <div className="col-4">{typePedagogiques.map((typePedagogique) => listes.typePedagogiques[typePedagogique]).join(', ')}</div>

              <div className="txtbold">{labels.typeDocumentaires}&nbsp;:</div>
              <div className="col-4">{typeDocumentaires.map((typeDocumentaire) => listes.typeDocumentaires[typeDocumentaire]).join(', ')}</div>
            </Fragment>
              )}

          <div className="txtbold">{labels.categories}&nbsp;:</div>
          <div className="col-4">{categories.map((categorie) => listes.categories[categorie]).join(', ')}</div>

          <div className="txtbold">{labels.resume}&nbsp;:</div>
          <div className="col-4">{nl2br(resume)}</div>

          <div className="txtbold">{labels.description}&nbsp;:</div>
          <div className="col-4">{nl2br(description)}</div>

          <div className="txtbold">{labels.commentaires}&nbsp;:</div>
          <div className="col-4">{nl2br(commentaires)}</div>

          {_enfants.length
            ? (
            <Fragment>
              <div className="txtbold">Enfants&nbsp;:</div>
              <div className="col-4">
                <ul>
                  {_enfants.map(({ url, titre }, index) => (
                    <li key={index.toString()}>
                      {url
                        ? (
                          <NavLink
                            to={url.replace(location.origin, '') /* faut passer une url qui démarre avec / */}
                            target="_blank"
                          >{titre}</NavLink>
                          )
                        : titre}
                    </li>
                  ))}
                </ul>
              </div>
            </Fragment>
              )
            : null}

          {!aliasOf && _auteurs.length
            ? (
            <Fragment>
              <div className="txtbold">{labels.auteurs}&nbsp;:</div>
              <div className="col-4">
                {_auteurs.length > 1
                  ? (
                  <ul>
                    {_auteurs.map(auteur => (
                      <li key={auteur}>{auteur}</li>
                    ))}
                  </ul>
                    )
                  : _auteurs[0]}
              </div>
            </Fragment>
              )
            : null}

          {!aliasOf && _contributeurs.length
            ? (
            <Fragment>
              <div className="txtbold">{labels.contributeurs}&nbsp;:</div>
              <div className="col-4">
                <ul>
                  {_contributeurs.map(contributeur => (
                    <li key={contributeur}>{contributeur}</li>
                  ))}
                </ul>
              </div>
            </Fragment>
              )
            : null}

          {!aliasOf && _relations.length
            ? (
            <Fragment>
              <div className="txtbold">{labels.relations}&nbsp;:</div>
              <div className="col-4">
                <ul className="relations">
                  {_relations.map(({ predicat, rid, titre, type, url }) => (
                    <li key={rid}>
                      {predicat}
                      <img src={icons[type]} />
                      <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {titre}
                      </a>
                      ({rid})
                    </li>
                  ))}
                </ul>
              </div>
            </Fragment>
              )
            : null}

          {!aliasOf && groupes.length
            ? (
            <Fragment>
              <div className="txtbold">{labels.groupes}&nbsp;:</div>
              <div className="col-4">
                <ul className="groupes">
                  {groupes.map(groupe => (
                    <li key={groupe}>{groupe}</li>
                  ))}
                </ul>
              </div>
            </Fragment>
              )
            : null}

          <ResourceHistory oid={oidSansVersion} version={version} isArchive={isArchive} />

        </section>
      </div>
    </Fragment>
  )
}

Description.propTypes = {
  /** La ressource dont on veut afficher la description */
  ressource: PropTypes.object
}

export default resourceLoader(Description)

import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { NavLink } from 'react-router-dom'
import { getContext } from 'recompose'
import icons from 'plugins/icons'

import getUrls from 'sesatheque-client/src/getUrls'

import serverConfig from '../../server/config'
import { askDelete } from '../utils/ressourceOperations'

import './ResourceSearchList.scss'

const { baseUrl } = serverConfig

/**
 * Liste de résultats de recherche (utilisé par les pages ResourceSearch et ResourceSearchAuto
 * @param isIframeLayout
 * @param handlePageClick
 * @param askDelete
 * @param refreshList
 * @param queryOptions
 * @param resources
 * @param showSearchLink
 * @param subNavText
 * @param total
 * @param error
 * @returns {JSX.Element}
 * @constructor
 */
const ResourceSearchList = ({
  isIframeLayout,
  handlePageClick,
  askDelete,
  refreshList,
  queryOptions,
  resources,
  showSearchLink,
  subNavText,
  total,
  error
}) => {
  if (error) {
    return (
      <p className="alert--danger">{error}</p>
    )
  }
  // query et queryOptions vont toujours ensemble
  if (!queryOptions) {
    // pas très normal…
    console.error(Error('ResourceSearchList appelé sans queryOptions'))
    return (
      <p className="alert--info">Aucun critère de recherche. {showSearchLink ? (<a href="#form">(rechercher)</a>) : ''}.</p>
    )
  }
  if (!total) {
    return (
      <p className="alert--info">Aucune ressource ne correspond à vos critères de recherche. {showSearchLink ? (<a href="#form">(modifier)</a>) : ''}</p>
    )
  }
  const { skip, limit } = queryOptions
  const last = Math.min(skip + limit, total)
  const hasPages = skip > 0 || last < total
  const pagination = hasPages
    ? (
    <ReactPaginate
      previousLabel={'<'}
      nextLabel={'>'}
      breakLabel={<a href="">...</a>}
      forcePage={Math.ceil(skip / limit)}
      pageCount={Math.ceil(total / limit)}
      onPageChange={handlePageClick}
      containerClassName={'pagination'}
      activeClassName={'active'} />
      )
    : null
  const subNav = subNavText
    ? (<Fragment>{subNavText}</Fragment>)
    : (
    <Fragment>
      <p className="fl">Ressources de {skip + 1} à {last} sur {total}</p>
      {pagination}
    </Fragment>
      )

  return (
    <div className="search-results">
      {subNav}
      <table className="table resourceList">
        <thead>
          <tr>
            <th>Type</th>
            <th>Identifiant</th>
            <th>Titre</th>
            <th colSpan="4">Actions</th>
          </tr>
        </thead>
        <tbody>
          {/* @todo ajouter ici un <tr> avec un lien pour télécharger le gift moodle de ce resultat de recherche */}
          {resources.map(({
            oid,
            rid,
            aliasOf,
            titre,
            type,
            $droits
          }, index) => {
            // on peut avoir des items de type error avec seulement un titre
            if (type === 'error') {
              return (
                <tr key={index}>
                  <td className="type"><img src={icons[type]} alt="" title={type} /></td>
                  <td></td>
                  <td>{titre}</td>
                  <td colSpan="4" className="links"></td>
                </tr>
              )
            }
            let aliasRmq = null
            let originalDesc = null
            let displayLink
            if (aliasOf) {
              aliasRmq = (<sup><i className="fa fa-link always" title={`alias de ${aliasOf}`}></i></sup>)
              const { displayUrl, describeUrl } = getUrls(aliasOf, baseUrl)
              // c'est ResourceSearchList.scss qui masque <i> sur écrans larges
              // et masque <span> et affiche <i> sur écrans étroits
              displayLink = (
                /* eslint-disable-next-line react/jsx-no-target-blank */
                <a href={displayUrl} target="_blank">
                  <span>Voir</span>
                  <i className="fa fa-eye"></i>
                </a>
              )
              originalDesc = describeUrl.startsWith('/')
                ? (<NavLink to={describeUrl}>
                    <span title="Aller sur la description de la ressource originale de cet alias">Original</span>
                    <i className="fa fa-file-alt"></i>
                  </NavLink>)
                // c'est sur une autre sesathèque, faut pas de NavLink mais un lien ordinaire
                : (<a href={describeUrl} target="_blank" rel="noreferrer">
                  <span title="Aller sur la description de la ressource originale de cet alias">Original</span>
                  <i className="fa fa-file-alt"></i>
                </a>)
            } else {
              displayLink = (<NavLink
                to={`/ressource/voir/${oid}`}
                target="_blank">
                <span>Voir</span>
                <i className="fa fa-eye"></i>
              </NavLink>)
            }

            return (
              <tr key={oid}>
                <td className="type"><img src={icons[type]} alt="" title={type} /></td>
                <td className="identifiant">{oid}{aliasRmq}</td>
                <td>{titre}</td>
                <td colSpan="4" className="links">
                  {originalDesc}
                  <NavLink
                    to={`/ressource/decrire/${oid}`}
                    title="Description">
                    <span>Description</span>
                    <i className="fa fa-file-alt"></i>
                  </NavLink>
                  <NavLink
                    to={`/ressource/apercevoir/${oid}`}
                    title="Aperçu">
                    <span>Aperçu</span>
                    <i className="fa fa-eye-slash"></i>
                  </NavLink>
                  {displayLink}
                  {$droits.includes('W')
                    ? (
                    <NavLink
                      to={`/ressource/modifier/${oid}`}
                      title="Modifier">
                      <span>Modifier</span>
                      <i className="fa fa-edit"></i>
                    </NavLink>
                      )
                    : null}
                  {$droits.includes('D')
                    ? (
                    <a
                      onClick={(e) => {
                        e.preventDefault()
                        askDelete({ oid, titre }, refreshList)
                      }}
                      href="#"
                      title="Supprimer">
                      <span>Supprimer</span>
                      <i className="fa fa-trash"></i>
                    </a>
                      )
                    : null}
                  {isIframeLayout
                    ? (
                    <a
                      onClick={(e) => {
                        e.preventDefault()
                        window.parent.postMessage({
                          action: 'ressource-copy',
                          rid
                        }, '*')
                      }}
                      href="#"
                      title="Copier">
                      <span>Copier</span>
                      <i className="fa fa-share"></i>
                    </a>
                      )
                    : null}
                </td>
              </tr>
            )
          })}
          {!resources.length
            ? (
            <tr>
              <td colSpan="7" className="empty">-</td>
            </tr>
              )
            : null}
        </tbody>
      </table>
      {subNav}
    </div>
  )
}

ResourceSearchList.propTypes = {
  isIframeLayout: PropTypes.bool,
  resources: PropTypes.arrayOf(PropTypes.shape({
    oid: PropTypes.string.isRequired,
    rid: PropTypes.string.isRequired,
    aliasOf: PropTypes.string,
    type: PropTypes.string.isRequired,
    titre: PropTypes.string.isRequired,
    $droits: PropTypes.string.isRequired,
    resume: PropTypes.string,
    description: PropTypes.string,
    commentaires: PropTypes.string
  })).isRequired,
  showSearchLink: PropTypes.bool,
  total: PropTypes.number.isRequired,
  error: PropTypes.string,
  handlePageClick: PropTypes.func.isRequired,
  askDelete: PropTypes.func.isRequired,
  // fourni par resourceListProvider
  query: PropTypes.object,
  queryOptions: PropTypes.shape({
    skip: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired
  }),
  refreshList: PropTypes.func.isRequired,
  subNavText: PropTypes.string
}

const mapDispatchToProps = (dispatch, { refreshList }) => ({
  askDelete: askDelete(dispatch, refreshList)
})

export default getContext({ isIframeLayout: PropTypes.bool })(connect(null, mapDispatchToProps)(ResourceSearchList))

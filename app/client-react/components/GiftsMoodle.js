import React, { Component } from 'react'
import { getGiftFilename } from 'server/lib/moodle'
import { moodle } from 'server/config'

const jsonUrl = '/moodle/staticExports.json'

class GiftsMoodle extends Component {
  constructor (props) {
    super(props)

    this.state = {
      errorMessage: '',
      staticExports: null
    }
  }

  componentDidMount () {
    const setError = (errorMessage) => this.setState({ errorMessage })
    fetch(jsonUrl).then(resp => {
      if (resp.status === 404) return setError('Aucun export prédéfini disponible ici (mais chaque ressource propose le sien)')
      if (!resp.ok) return setError('Impossible de récupérer la liste')
      return resp.json()
    }).then(result => {
      if (Array.isArray(result?.liste)) {
        // on vérifie le format du contenu recupéré
        const staticExports = result.liste.filter(e => e?.href && e.titre && e.rid)
        if (staticExports.length) this.setState({ staticExports })
        else setError('Liste vide')
      } else {
        setError('Format de liste invalide')
      }
    }).catch(error => {
      console.error(error)
      setError('Impossible de récupérer la liste')
    })
  }

  render () {
    const { errorMessage, staticExports } = this.state
    return (<div>
        <h1>Liste d’exports de gifts pour Moodle</h1>
        <p>Moodle est une plateforme d’apprentissage en ligne qui propose notamment de créer des tests comportant des questions évaluées. Vous avez la possibilité d’inclure des exercices de la sesathèque directement dans la plateforme Moodle avec récupération automatique des scores grâce aux fichiers GIFT que vous pouvez télécharger (sur cette page ou directement depuis chaque ressource) pour ensuite les importer dans votre banque de questions Moodle.<br />
        </p>
        {
          moodle?.pageAide
            ? (<p className="alert--info" style={{ marginBottom: '1rem' }}>Plus d’infos sur cette page d’<a href={moodle.pageAide} target="_blank" rel="noreferrer">aide</a></p>)
            : null
        }
        <p>Attention, les collections d’exercices proposés sur cette page sont assez fournies, vous pouvez améliorer votre sobriété numérique en important individuellement les ressources qui vous intéressent (via le lien présent sur leur page de description).</p>
        <ul>{errorMessage
          ? <span className="error">{this.state.errorMessage}</span>
          : staticExports === null
            ? 'Chargement en cours'
            : staticExports.map(({ href, rid, titre }, index) => (<li key={index.toString()}>{titre}
                <ul>
                  <li><a href={href} download={getGiftFilename(rid, titre)} target="_blank" rel="noreferrer">Télécharger le gift</a></li>
                  <li><a href={`/ressource/apercevoir/${rid.replace(/^.*\//, '')}`} target="_blank" rel="noreferrer">Aperçu</a></li>
                </ul>
              </li>)
            )
        }
        </ul>
      </div>
    )
  }
}

export default GiftsMoodle

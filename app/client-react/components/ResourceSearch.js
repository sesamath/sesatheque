import { push } from 'connected-react-router'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import React, { Fragment } from 'react'
import ResourceSearchForm from './ResourceSearchForm'
import ResourceSearchList from './ResourceSearchList'
import resourceListProvider from '../hoc/resourceListProvider'

// TODO recherche :
// - revoir la position des champs, pour remonter le niveau et mettre tout les params avancés ensemble en dessous (tous sauf mots/type/auteurs/groupes) dans un truc qui se déplie
// - pour les recherches qui remontent des type error (pb de droits), ne pas les lister (cf une recherche de non publié sur commun en user standard) et préciser qu'il y a des ressources qu'on ne peut pas lister pour des pbs de droit d'accès
// - fix recherche sur origine / idOrigine qui semble déconner
// - ajouter un critère de tri

/**
 * Page de recherche (ResourceSearchForm + ResourceSearchList)
 * @param props
 * @returns {JSX.Element}
 */
const ResourceSearch = (props) => {
  // si y'a une error dans les props (mise par resourceListProvider), on laisse ResourceSearchList l'afficher, pas besoin de le gérer ici
  const { hash, query, allowAnyOption, search } = props
  const defaultFormValues = {
    publie: true,
    restriction: 0,
    // faut garantir des array pour tous les multi-valués
    categories: [],
    niveaux: [],
    typePedagogiques: [],
    typeDocumentaires: []
  }

  // Il faut mettre query dans les valeurs initiales (si le composant est chargé d'après une url de résultats)
  const initialValues = { ...defaultFormValues, ...query }
  // mais on met le type j3p par défaut dans initialValues que si aucun critère n'est demandé.
  // C'est important de le faire dans initialValues (valable seulement pour l'init du reduxForm) et pas dans query,
  // afin que l'utilisateur puisse préciser un type "peu importe" et qu'on ne lui remette plus ensuite j3p d'office
  if (!search) {
    initialValues.type = 'j3p'
  }

  // c'est le hash qui impose form / liste (query est toujours présent, c'est resourceListProvider qui l'assure, search est vide si on a rien demandé)
  const isFormOpen = hash === '#form' || !search
  const title = isFormOpen ? 'Recherche' : 'Résultat de la recherche'

  return (
    <Fragment>
      <h1>{title}</h1>
      <div className="alert--info">Vous pouvez aussi essayer la recherche assistée <NavLink to="/autocomplete">depuis cette page</NavLink></div>
      <ResourceSearchForm
        isOpen={isFormOpen}
        query={query}
        initialValues={initialValues}
        allowAnyOption={allowAnyOption}
      />
      {!isFormOpen && (<ResourceSearchList {...props} showSearchLink={true} />)}
    </Fragment>
  )
}

ResourceSearch.propTypes = {
  /** fourni par hoc resourceListProvider (construit à partir de query) */
  allowAnyOption: PropTypes.bool,
  /** fourni par le router (hoc resourceListProvider) */
  hash: PropTypes.string,
  /** fourni par hoc resourceListProvider (construit à partir de search) */
  query: PropTypes.object,
  /** fourni par hoc resourceListProvider (construit à partir de search) */
  queryOptions: PropTypes.shape({
    skip: PropTypes.number,
    limit: PropTypes.number
  }),
  /** fourni par hoc resourceListProvider (qui dispatch le fetch) */
  resources: PropTypes.array,
  /** La queryString fournie par le router (hoc resourceListProvider) */
  search: PropTypes.string,
  /** fourni par hoc resourceListProvider (set au retour de l'api) */
  total: PropTypes.number
}

// pour ajouter le comportement du changement de page
const mapDispatchToProps = (dispatch, { query, queryOptions }) => ({
  // au clic sur un changement de pagination faut mettre à jour l'url
  // (et resourceListProvider mettra à jour la liste resources)
  handlePageClick: (data) => {
    const params = {
      ...query,
      skip: (Math.round(data.selected) || 0) * queryOptions.limit
    }

    dispatch(push({
      pathname: '/ressource/rechercher',
      search: queryString.stringify(params)
    }))
  }
})

export default resourceListProvider(
  connect(null, mapDispatchToProps)(ResourceSearch)
)

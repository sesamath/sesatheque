import { push } from 'connected-react-router'
import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getContext } from 'recompose'
import { askDelete, askClone } from '../utils/ressourceOperations'
import NavMenuItem from './NavMenuItem'
import NavButton from './NavButton'
import './NavMenu.scss'
import listes from '../utils/listesFromConfig'

const { createTypes } = listes
const typesAllowedForCreation = new Set(createTypes.map(({ value }) => value))

const NavMenu = ({
  isIframeLayout,
  ressource,
  doClone,
  doDelete,
  titre,
  hasCreateAllPermission,
  hasCreatePermission
}) => {
  if (!titre) titre = ressource.titre
  // pas de menu en layout iframe
  if (isIframeLayout) {
    return <h1>{titre}</h1>
  }

  const { oid, type, publie, restriction } = ressource
  const isAlias = Boolean(ressource.aliasOf)
  const droits = ressource._droits ?? ''
  const hasDuplicatePermission = !isAlias && (hasCreateAllPermission || (hasCreatePermission && typesAllowedForCreation.has(type)))
  const isArchive = oid.includes('@')
  const isPublic = publie && restriction === 0
  const displayUrl = isPublic ? `/public/voir/${oid}` : `/ressource/voir/${oid}`

  return (
    <Fragment>
      <div id="actions">
        <ul>
          <NavMenuItem
            to={`/ressource/decrire/${oid}`}
            title="Description"
            icon="file-alt"
          />
          <NavMenuItem
            to={`/ressource/apercevoir/${oid}`}
            title="Aperçu"
            icon="eye-slash"
          />
          <NavMenuItem
            to={displayUrl}
            title="Voir"
            icon="eye"
            target="_blank"
          />
          {droits.includes('W')
            ? isArchive
              ? (<NavMenuItem
                  to={`/ressource/restaurer/${oid}`}
                  title="Restaurer"
                  icon="undo"
                  id="buttonRestore"
                />)
              : (<NavMenuItem
                  to={`/ressource/modifier/${oid}`}
                  title="Modifier"
                  icon="edit"
                  id="buttonEdit"
                />)
            : null}
          {hasDuplicatePermission
            ? (
            <NavButton
              onClick={doClone.bind(null, oid)}
              title="Dupliquer"
              icon="copy"
              id="buttonDuplicate"
            />
              )
            : null}
          {droits.includes('D') && !isArchive
            ? (
            <NavButton
              onClick={doDelete.bind(null, ressource)}
              title="Supprimer"
              icon="trash"
              id="buttonDelete"
            />
              )
            : null}
        </ul>
        <h1 className="fl">{titre}</h1>
        <div className="clearfix"></div>
      </div>
    </Fragment>
  )
}

NavMenu.propTypes = {
  doClone: PropTypes.func.isRequired,
  doDelete: PropTypes.func.isRequired,
  isIframeLayout: PropTypes.bool,
  ressource: PropTypes.shape({
    oid: PropTypes.string.isRequired,
    publie: PropTypes.bool.isRequired,
    restriction: PropTypes.number.isRequired,
    titre: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    _droits: PropTypes.string.isRequired,
    // ce qui suit n'est pas fourni par ResourceForm (édition de ressource)
    aliasOf: PropTypes.string
  }).isRequired,
  titre: PropTypes.string,
  hasCreateAllPermission: PropTypes.bool,
  hasCreatePermission: PropTypes.bool
}

const mapStateToProps = ({ session }) => ({
  hasCreateAllPermission: Boolean(session && session.personne && session.personne.permissions && session.personne.permissions.createAll),
  hasCreatePermission: Boolean(session && session.personne && session.personne.permissions && session.personne.permissions.create)
})

// les props sont passées en 2e argument
const mapDispatchToProps = (dispatch) => ({
  doDelete: askDelete(dispatch, () => dispatch(push('/'))),
  doClone: askClone(dispatch, clonedOid => dispatch(push(`/ressource/modifier/${clonedOid}`)))
})

export default getContext({ isIframeLayout: PropTypes.bool })(connect(mapStateToProps, mapDispatchToProps)(NavMenu))

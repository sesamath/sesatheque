import { identity } from 'lodash'
import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { withProps } from 'recompose'
import { reduxForm } from 'redux-form'
import { Prompt } from 'react-router'
import { parse } from 'query-string'

import MetaForm from './MetaForm'
import EditorSimple from './EditorSimple'
import GroupContainer from './GroupContainer'
import aliasForker from '../hoc/aliasForker'
import archiveRestorer from '../hoc/archiveRestorer'
import resourceLoader from '../hoc/resourceLoader'
import resourceSaver from '../hoc/resourceSaver'
import ensureLogged from '../hoc/ensureLogged'
import NavMenu from './NavMenu'
import onSubmitFail from '../utils/onSubmitFail'
import commonValidate from '../utils/ressourceValidate'
import getEditor from 'plugins/editors'

/**
 * Formulaire d'édition d'une ressource (en cas de création c'est après le choix
 * des métadonnées, et donc après sauvegarde de la ressource minimale)
 * @param {Ressource} ressource
 * @param handleSubmit
 * @param submitting
 * @param pristine
 * @returns {JSX.Element}
 * @constructor
 */
const ResourceForm = ({
  initialValues,
  handleSubmit,
  submitting,
  pristine
}) => {
  const { titre, type } = initialValues
  const { editor: Editor = EditorSimple } = getEditor(type)
  // _droits est mis par l'api, mais à la création il est vide
  if (!initialValues._droits) initialValues._droits = 'RWD'

  return (
    <Fragment>
      <NavMenu
        titre={`Modifier la ressource « ${titre} »`}
        ressource={initialValues}
      />
      <form onSubmit={handleSubmit}>
        <MetaForm />
        <hr />
        <GroupContainer />
        <hr />
        <Editor />
        <div className="buttons-area">
          <button
            type="submit"
            className="btn--primary"
            disabled={submitting}
          >
            Enregistrer
          </button>
        </div>
      </form>
      <Prompt
        when={!pristine}
        message="Il existe des changements non sauvegardés sur le formulaire, êtes vous sûr de vouloir changer de page ?"
      />
    </Fragment>
  )
}

ResourceForm.propTypes = {
  initialValues: PropTypes.shape({
    oid: PropTypes.string.isRequired,
    publie: PropTypes.bool.isRequired,
    restriction: PropTypes.number.isRequired,
    titre: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    _droits: PropTypes.string
  }).isRequired,
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  saveRessource: PropTypes.func,
  pristine: PropTypes.bool
}

const validate = (values) => {
  const errors = commonValidate(values)
  const { type } = values
  const { validate: typeValidate } = getEditor(type)
  if (typeValidate) {
    // console.log(`pour le type ${type} on a un validate`)
    typeValidate(values, errors)
  }
  // console.log(`pour le type ${type} on retourne les erreurs de validation`, errors)
  return errors
}

const onSubmit = (values, dispatch, { saveRessource }) => {
  const { saveHook = identity } = getEditor(values.type)
  // on passe les valeurs du form à l'éditeur
  const transformedValues = saveHook(values)
  if (!transformedValues || transformedValues.type !== values.type || transformedValues.oid !== values.oid) {
    throw Error(`L’éditeur du type ${values.type} ne fonctionne pas correctement (il ne retourne pas les valeurs attendues)`)
  }

  return saveRessource(transformedValues, (savedRessource) => {
    // On notifie le parent concernant la mise à jour de la ressource
    if (parent !== window && parent.postMessage) {
      const parsedQuery = parse(window.location.search)
      if (parsedQuery.closerId) {
        // @todo harmoniser ces préfixes _ retournés par l'api pour mettre du $ partout (maintenant qu'on passe plus par context.rest qui les virait)
        const ressource = { ...savedRessource }
        delete ressource._droits
        ressource.$droits = savedRessource._droits
        parent.postMessage({
          action: 'iframeCloser',
          id: parsedQuery.closerId,
          ressource
        }, '*')
      }
    }
  })
}

const propsAfterLoadHook = ({ ressource }) => {
  const { loadHook = identity } = getEditor(ressource.type)

  return {
    initialValues: loadHook(ressource)
  }
}
const withPropsWrapper = withProps(propsAfterLoadHook)

const reduxFormWrapper = reduxForm({
  // ATTENTION, ne pas modifier cet id car des plugins l'utilisent pour se brancher sur notre redux-form
  // (plugin iep qui a besoin de notre props.change par ex, il le récupère en wrappant son subForm avec
  // reduxForm et le même id)
  form: 'ressource',
  validate,
  onSubmit,
  onSubmitFail,
  enableReinitialize: true
})

export default ensureLogged(
  resourceLoader(
    aliasForker( // fork si on édite un alias
      archiveRestorer( // restaure une archive si on en édite une
        resourceSaver( // fournit saveRessource
          withPropsWrapper(
            reduxFormWrapper(ResourceForm)
          )
        )
      )
    )
  )
)

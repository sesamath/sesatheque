import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router-dom'
import { getContext } from 'recompose'
// pour récupérer la version, on passe par la config (déjà chargée par webpack) plutôt que le package.json (inutile de tout mettre dans le bundle)
import config from '../../server/config'
import './Footer.scss'

const { version } = config

export const Footer = ({ isIframeLayout }) => {
  if (isIframeLayout) return null

  return (
    <footer>
      <ul className="clearfix unstyled">
        <li className="fl">
          <Link to="/mentionsLegales">Infos légales</Link>
        </li>
        <li className="fl">
          <a href="http://www.sesamath.net">Association Sésamath</a>
        </li>
        <li className="fr">
          Propulsé par <a href="https://git.sesamath.net/sesamath/sesatheque">Sésathèque</a> {version}
        </li>
      </ul>
    </footer>
  )
}

Footer.propTypes = {
  isIframeLayout: PropTypes.bool
}

export default getContext({ isIframeLayout: PropTypes.bool })(Footer)

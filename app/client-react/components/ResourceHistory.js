import { getArchivesListUrl } from 'client-react/apiRoutes'
import { GET } from 'client-react/utils/httpMethods'
import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { formatDateTime } from 'sesajs-date'

class ResourceHistory extends Component {
  constructor (props) {
    super(props)

    this.state = {
      archives: props.version === 1 ? [] : null,
      loading: false,
      errorMessage: ''
    }
  }

  loadArchives () {
    this.setState({ loading: true })

    GET(getArchivesListUrl({ oid: this.props.oid }))
      .then(({ archives }) => {
        archives = archives
          // on vire l'archive en cours d'affichage (si c'est le cas)
          .filter(({ oid }) => oid !== this.props.oid)
          // on restaure les dates (elles sont en string) et on ajoute archVersion en number
          .map(({ dateMiseAJour, oid, titre }) => ({
            oid,
            archVersion: Number(oid.replace(/.*@/, '')),
            date: new Date(dateMiseAJour),
            titre
          }))
          // et il faut trier
          .sort((a, b) => a.version < b.version ? -1 : 1)
        this.setState({ archives, loading: false, errorMessage: '' })
      })
      .catch((error) => {
        // inutile d'afficher l'erreur en console, GET l'a déjà fait
        // en notif l'erreur envoyée par le serveur ou construite par httpMethods
        this.setState({ errorMessage: error.message, loading: false })
      })
  }

  render () {
    const { oid, isArchive } = this.props
    return (
      <Fragment>
        <div className="txtbold">Historique&nbsp;:</div>
        <div className="col-4 history">
          {isArchive
            ? (<a href={oid}>Version actuelle</a>)
            : null
          }
          {this.state.errorMessage
            ? (<p className="error">{this.state.errorMessage}</p>)
            : null
          }
          <ul>
            {this.state.loading
              ? (<div id="spinner">Chargement en cours…</div>)
              : this.state.archives
                ? this.state.archives.length
                  ? this.state.archives.map(({ date, oid, titre, archVersion }) => {
                    const jour = formatDateTime({ date, fr: true, withoutMs: true })
                    const label = `Version ${archVersion} du ${jour}`
                    return (
                      <li key={oid} title={titre}>{label}
                        <a href={'../apercevoir/' + oid}>aperçu</a>
                        - <a href={oid} >description</a>
                        - <NavLink className="btn btn--rounded" to={'../restaurer/' + oid}>Restaurer cette version</NavLink>
                      </li>
                    )
                  })
                  : 'Aucune version précédente n’est archivée.'
                : (<button
                    className="btn btn--rounded"
                    onClick={this.loadArchives.bind(this)}>Afficher la liste des versions précédentes</button>
                  )
            }
          </ul>
        </div>
      </Fragment>
    )
  }
}

ResourceHistory.propTypes = {
  oid: PropTypes.string,
  isArchive: PropTypes.bool,
  version: PropTypes.number
}

export default ResourceHistory

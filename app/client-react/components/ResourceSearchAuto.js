import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { autocomplete, search } from 'sesatheque-client/src/client'
import { debounce } from 'lodash'
import config from '../../server/config'
import { labels, listes } from '../../server/ressource/config'
import { Async as Select } from 'react-select'
import ResourceSearchList from './ResourceSearchList'

const defaultQuery = {
  skip: 0,
  limit: 100
}

class OptionValue {
  constructor (filter, filterValue) {
    this.filter = filter
    this.filterValue = filterValue
  }

  toString () {
    return `${this.filter}-${this.filterValue}`
  }
}

const optionValueToText = (label, value) => listes[label] ? listes[label][value] : value
const optionLabelToText = label => labels[label]

const debouncedGET = debounce((input, callback) => {
  autocomplete(config.baseId, input, (error, filters) => {
    if (error) return console.error(error)
    const options = [{
      value: new OptionValue('fulltext', input),
      label: `Texte libre : ${input}`
    }]

    for (const [filter, filterValue] of Object.entries(filters)) {
      options.push({
        value: new OptionValue(filter, filterValue),
        label: `${optionLabelToText(filter)}: ${optionValueToText(filter, filterValue)}`
      })
    }
    callback(options)
  })
}, 500)

const getOptions = (input, setOptions) => {
  if (!input || input.length <= 2) return setOptions([])
  debouncedGET(input, setOptions)
}

const customStyles = {
  multiValue: (styles, { data }) => {
    const overridedStyles = {
      ...styles,
      backgroundColor: 'rgba(0, 126, 255, 0.08)',
      borderRadius: '2px',
      border: '1px solid #c2e0ff',
      color: 'rgba(255,255,255,0.5)'
    }

    if (data.value.filter === 'fulltext') {
      overridedStyles.backgroundColor = '#fcf8e3'
      overridedStyles.color = '#8a6d3b'
      overridedStyles.border = '1px solid #DFCFB3'
    }

    return overridedStyles
  }
}

/**
 * Page de recherche assistée, qui gère elle-même son form et utilise ResourceSearchList pour les résultats
 */
class ResourceSearchAuto extends Component {
  constructor (props) {
    super(props)

    this.state = {
      resources: [],
      selection: []
    }
  }

  filterOption ({ value }) {
    const key = value.toString()
    return this.state.selection.every(option => option.value.toString() !== key)
  }

  searchResources () {
    const queryFilters = {
      ...defaultQuery
    }
    for (const element of this.state.selection) {
      if (!queryFilters[element.value.filter]) queryFilters[element.value.filter] = []
      queryFilters[element.value.filter].push(element.value.filterValue)
    }
    // on force public
    queryFilters.restriction = 0
    queryFilters.publie = true

    search(config.baseId, queryFilters, (error, resources) => {
      if (error) return console.error(error)
      this.setState({ resources })
    })
  }

  render () {
    const subNavText = this.state.resources.length === 100 ? 'Seules les 100 premières ressources sont affichées, vous devriez affiner la recherche' : `Ressources de 1 à ${this.state.resources.length}`
    return (
      <Fragment>
        <h1>Recherche assistée</h1>
        <div className="grid-5">
          <p className="col-4">Lorsque vous tapez un mot ou une expression, l’automplétion propose par défaut « Texte libre » (ça signifie que ce mot ou cette expression sera recherchée dans les champs titre|résumé|commentaires) mais si cela correspond à un autre critère possible vous aurez d’autres choix (par exemple « sixième » peut être un critère de niveau pour restreindre à ce niveau ou bien un mot à rechercher)
          </p>
        </div>
        <div className="grid-5">
          <div className="col-4">
            <Select
              classNamePrefix="react-select"
              className="react-select"
              value={this.state.selection}
              clearable
              closeOnSelect={false}
              filterOption={this.filterOption.bind(this)}
              hideSelectedOptions
              onChange={selection => this.setState({ selection })}
              placeholder="Votre recherche"
              noOptionsMessage={() => 'Aucun résultat trouvé'}
              loadingMessage={() => 'Recherche en cours'}
              loadOptions={getOptions}
              styles={customStyles}
              isMulti
            />
          </div>
          <button
            className="btn btn--rounded"
            onClick={this.searchResources.bind(this)}>Rechercher</button>
        </div>
        <p>La recherche est actuellement limitée aux ressources publiques (publiées et sans restriction).</p>
        <div className="alert--info">Vous pouvez aussi utiliser la <NavLink to="/ressource/rechercher#form">recherche classique</NavLink></div>
        <ResourceSearchList
          handlePageClick={() => {}}
          refreshList={this.searchResources.bind(this)}
          queryOptions={defaultQuery}
          resources={this.state.resources}
          subNavText={subNavText}
          total={this.state.resources.length} />
      </Fragment>
    )
  }
}

export default ResourceSearchAuto

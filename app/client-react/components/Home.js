import React from 'react'

// #homeContent dans le source initial de la page est mis là
// au start de l'appli server (par app/server/main/reactPage.js)
// Ce composant le remet dans le dom react
const homeContentContainer = document.getElementById('homeContent')
let homeContent = ''
if (homeContentContainer) {
  homeContent = homeContentContainer.innerHTML
  document.body.removeChild(homeContentContainer)
} else {
  console.error(Error('Pas trouvé de #homeContent dans le source de la page'))
}

const Home = () => (
    <div>
      <h1>Bienvenue sur cette Sésathèque</h1>
      <div dangerouslySetInnerHTML={{ __html: homeContent }}></div>
    </div>
)

export default Home

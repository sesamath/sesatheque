import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import NavMenu from './NavMenu'
import resourceLoader from '../hoc/resourceLoader'

/**
 * @returns {JSX.Element}
 * @type {Component}
 */
const Preview = ({ ressource }) => {
  const { _urls, oid, titre } = ressource
  return (
    <Fragment>
      <NavMenu
        ressource={ressource}
        titre={`Aperçu de ${oid.includes('@') ? 'l’ARCHIVE' : 'la ressource'} : ${titre}`}
      />
      <iframe src={_urls.displayUrl} />
    </Fragment>
  )
}

Preview.propTypes = {
  ressource: PropTypes.shape({
    _urls: PropTypes.shape({
      displayUrl: PropTypes.string.isRequired
    }).isRequired,
    oid: PropTypes.string.isRequired,
    aliasOf: PropTypes.string,
    titre: PropTypes.string.isRequired
  })
}

export default resourceLoader(Preview)

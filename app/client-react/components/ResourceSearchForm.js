import { push } from 'connected-react-router'
import PropTypes from 'prop-types'
import React from 'react'
import { reduxForm } from 'redux-form'
import { labels, listes } from '../../server/ressource/config'
import listesFromConfig from '../utils/listesFromConfig'
import Classification from './Classification'
import { InputField, SelectField, SwitchField } from './fields'
import queryString from 'query-string'

import './ResourceSearchForm.scss'

const anyOption = {
  value: '',
  label: 'peu importe'
}
// ces deux là on toujours any en 1er choix (pour les autres ça dépend de allowAnyOption)
const langue = [anyOption, ...listesFromConfig.langue]
const type = [anyOption, ...listesFromConfig.type]

const publieSelectOptions = [
  anyOption,
  { value: true, label: 'oui' },
  { value: false, label: 'non' }
]

/*
La prop allowAnyOption peut sembler curieuse, c'est pour éviter de se retrouver avec restriction "peu importe" et publié "peu importe" qui
- empêchent d'utiliser le cache http
- vont remonter des ressources que l'utilisateur ne pourra probablement pas voir

On n'autorise ces choix que lorsque l'utilisateur précise un auteur ou un groupe (faudrait limiter à lui et ses groupes).
 */

const ResourceSearchForm = ({ handleSubmit, isOpen, query, allowAnyOption }) => {
  if (isOpen) {
    return (
      <form onSubmit={handleSubmit}>
        <fieldset>
          <div className="grid-3">
            <InputField
              className="col-2"
              label="Mots recherchés (dans titre et résumé)"
              placeholder="Utilisez les guillemets &quot; pour rechercher une expression exacte."
              name="mots" />
            <SelectField
              label={labels.langue}
              name="langue"
              options={langue}
            />
            <SelectField
              label={labels.type}
              name="type"
              options={type}
            />
            <SelectField
              label={labels.restriction}
              options={allowAnyOption
                ? [
                    anyOption,
                    ...listesFromConfig.restriction
                  ]
                : listesFromConfig.restriction}
              name="restriction"/>
            {allowAnyOption
              ? (
              <SelectField
                label={labels.publie}
                options={publieSelectOptions}
                name="publie"
              />
                )
              : (
              <SwitchField
                className="center"
                label={labels.publie}
                name="publie"
              />
                )}

            <InputField
              label={labels.oid}
              name="oid"/>
            <InputField
              label={labels.origine}
              name="origine"/>
            <InputField
              label={labels.idOrigine}
              name="idOrigine"/>

            <InputField
              className="col-2"
              label={labels.auteurs}
              name="auteurs"/>
            <InputField
              label={labels.groupes}
              name="groupes"/>
          </div>
        </fieldset>
        <hr/>
        <Classification detailed/>
        <div className="buttons-area">
          <button type="submit" className="btn--primary">Rechercher</button>
        </div>
      </form>
    )
  }
  // sinon on rappelle juste les critères
  return (
    <div className="search-box">
      <div className="fr">
        <a href="#form">Modifier</a> les critères de recherche actuels
      </div>
      <ul className="tags">
        {Object.entries(query).map(([key, criteria]) => {
          if (!labels[key]) return '' // c'est un critère de tri ajouté dans l'url mais non géré par le form
          // les booléens et restriction doivent avoir une traduction plus parlante que leur valeur
          let criteriaLabel
          if (typeof criteria === 'boolean') {
            criteriaLabel = criteria ? 'oui' : 'non'
          } else if (listes[key]) {
            if (Array.isArray(criteria)) {
              // on ajoute `?? c` en fallback, au cas où on aurait pas le label (pour afficher au moins la clé plutôt que 'undefined')
              const criteriaMapper = (c) => listes[key][c] ?? c
              if (criteria[0] === '!') {
                // c'est une négation
                const realCriterias = criteria.slice(1)
                criteriaLabel = realCriterias.length === 1
                  ? `tout sauf "${realCriterias[0]}"`
                  : 'tout sauf "' + realCriterias.map(criteriaMapper).join('" et "') + '"'
              } else {
                criteriaLabel = '"' + criteria.map(criteriaMapper).join('" ou "') + '"'
              }
            } else if (typeof criteria === 'string' && criteria.startsWith('!')) {
              criteriaLabel = 'tout sauf "' + listes[key][criteria.substring(1)] + '"'
            } else {
              criteriaLabel = listes[key][criteria]
            }
          } else {
            // Champ libre (sans valeurs prédéfinies) qui n'est pas un booléen => on laisse les valeurs saisies.
            // On ne devrait pas les avoir ici résumé / description mais il reste auteurs et groupes.
            if (Array.isArray(criteria)) {
              if (criteria[0] === '!') {
                // c'est une négation
                if (criteria.length === 2) {
                  criteriaLabel = 'tout sauf "' + criteria[1] + '"'
                } else {
                  criteriaLabel = 'tout sauf "' + criteria.slice(1).join('" et "') + '"'
                }
              } else {
                criteriaLabel = '"' + criteria.join('" ou "') + '"'
              }
            } else if (typeof criteria === 'string' && criteria.startsWith('!')) {
              criteriaLabel = 'tout sauf "' + criteria.substring(1) + '"'
            } else {
              criteriaLabel = criteria
            }
          }

          return (
            <li key={key}><span className="tag--info">{labels[key]} : {criteriaLabel}</span></li>
          )
        })}
      </ul>
    </div>
  )
}

const queryToSearch = (query) => {
  const compactQuery = {}
  for (const [k, v] of Object.entries(query)) {
    if (v == null) continue // nullish viré
    if (Array.isArray(v) && !v.length) continue // tableau vide viré
    compactQuery[k] = v
  }
  return queryString.stringify(compactQuery)
}

ResourceSearchForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  query: PropTypes.object,
  allowAnyOption: PropTypes.bool
}

// pour utiliser Field faut être un redux-form
const reduxFormWrapper = reduxForm({
  form: 'searchForm',
  // cf https://redux-form.com/7.4.2/docs/api/reduxform.md/#-code-enablereinitialize-boolean-code-optional-
  // sinon le form garde son état interne entre clic sur "mes ressources" puis "rechercher"
  enableReinitialize: true,
  onSubmit: (query, dispatch) => {
    // on nettoie ici query des éventuels critères qui seraient ignorés
    if (query.oid) {
      query = { oid: query.oid }
    } else if (query.origine && query.idOrigine) {
      query = {
        origine: query.origine,
        idOrigine: query.idOrigine
      }
    }
    dispatch(push({
      pathname: '/ressource/rechercher',
      hash: '#results',
      search: queryToSearch(query)
    }))
  }
})

export default reduxFormWrapper(ResourceSearchForm)

// ce module est utilisé par nodeJs (pour sortir dans le html la correction chiffrée) ET par le browser (pour déchiffrer la correction)
// on utilise crypto-js en v3 car la v4 pose pb avec webpack5
const { encrypt, decrypt } = require('crypto-js/aes')
const enc = require('crypto-js/enc-utf8')

module.exports = {
  /**
   * Chiffre une string
   * @param {string} text Le texte à chiffrer
   * @param {string} key La clé de chiffrement
   * @return {string}
   */
  encrypt: (text, key) => encrypt(text, key).toString(),
  /**
   * Chiffre une string
   * @param {Object} obj L'Objet à chiffrer
   * @param {string} key La clé de chiffrement
   * @return {string}
   */
  encryptObj: (obj, key) => encrypt(JSON.stringify(obj), key).toString(),
  /**
   * Déchiffre une string
   * @param {string} encryptedText Le texte à déchiffrer
   * @param {string} key La clé de chiffrement
   * @return {string}
   */
  decrypt: (encryptedText, key) => decrypt(encryptedText, key).toString(enc),
  /**
   * Déchiffre un objet chiffré au préalable avec encryptObj
   * @param {string} encryptedText Le texte à déchiffrer, sortie de encryptObj(obj, key)
   * @param {string} key La clé de chiffrement
   * @return {Object|*}
   */
  decryptObj: (encryptedText, key) => JSON.parse(decrypt(encryptedText, key).toString(enc))
}

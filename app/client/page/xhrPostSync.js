// Module pour simuler de l'ajax synchrone
// si c'est dispo avec du sendBeacon vers /api/deferSync qui va reposter en async vers ailleurs,
// sinon en faisant un xhr.sync vers /api/deferSync qui fera suivre

import sjt from 'sesajstools'
import xhr from 'sesajstools/http/xhr'

/**
 * À n'utiliser que dans un event unload (où on peut pas faire de l'ajax async)
 * Utilisera sendBeacon si c'est dispo, ou ajax sync, pour envoyer à '/api/deferPost' qui fera suivre
 * @private
 * @param {string}   url url absolue vers un sesalab connu
 * @param {Object} data
 * @param {errorCallback} next appellé en synchrone
 */
export default function xhrPostSync (url, data, next) {
  // ici pas le choix, si on veut envoyer ça sur du unload faut du xhr sync, qui est deprecated
  // et que chrome ne fait plus…
  // on essaie quand même sendBeacon si c'est dispo…
  // cf https://developer.mozilla.org/en-US/docs/Web/API/Navigator/sendBeacon
  // sauf qu'il faudrait ajouter un middleware pour décoder une simple string
  // ou alors utiliser les web workers et FormData, on verra plus tard…
  if (typeof navigator !== 'undefined' && navigator.sendBeacon && typeof Blob !== 'undefined') {
    data.deferUrl = url
    url = '/api/deferPost'
    // faut préciser le type sinon y'a un pb de preflight avec chrome
    const blob = new Blob([sjt.stringify(data)], { type: 'text/plain; charset=UTF-8' })
    if (navigator.sendBeacon(url, blob)) next(null)
    else next(new Error('sendBeacon existe mais son appel a échoué'))
  } else {
    xhr.post(url, data, { sync: true }, error => {
      if (!error) return next(null)
      /* global bugsnagClient */
      if (typeof bugsnagClient !== 'undefined') bugsnagClient.notify(error)
      console.error(error)
      next(error)
    })
  }
}

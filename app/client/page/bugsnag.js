// ce fichier met un objet busgnagClient en global (soit le vrai soit un fake), il n'exporte rien

// On utilise ça pour mutualiser la conf bugsnag entre client & client-react
import getBugsnagClient from '../../client-react/utils/getBugsnagClient'

if (typeof window === 'undefined') {
  console.error(new Error('pas de busgnag hors d’un navigateur'))
} else if (!window.bugsnagClient) {
  const bugsnagClient = getBugsnagClient()
  if (bugsnagClient) {
    window.bugsnagClient = bugsnagClient
  } else {
    console.error('pas d’apiKey pour bugsnag, on crée un fake qui sortira les erreurs en console')
    // pour que ceux qui s'attendent à trouver ça ne plantent pas
    window.bugsnagClient = {
      addMetadata: function addMetadata (section, prop, value) {
        if (!window.bugsnagClient.metadata[section]) window.bugsnagClient.metadata[section] = {}
        if (typeof prop === 'object') window.bugsnagClient.metadata[section] = { ...window.bugsnagClient.metadata[section], ...prop }
        else if (typeof prop === 'string') window.bugsnagClient.metadata[section][prop] = value
        else console.error(Error('addMetadata appelée avec des arguments invalides'), arguments)
      },
      notify: function fakeNotify () {
        console.error('bugsnag n’a pas été instancié, mais il reçoit')
        console.error.apply(console, arguments)
      },
      metadata: {},
      user: {}
    }
  }
}

/**
 * @file Liste de typedef pour jsdoc
 * @private
 */

/**
 * Callback appellée avec une erreur ou sans argument
 * @callback errorCallback
 * @param {Error} [error] Une erreur éventuelle
 */

/**
 * @callback groupeCallback
 * @param {Error}  [error]  erreur éventuelle
 * @param {Groupe} [groupe] Entity ou Object
 */

/**
 * @callback groupeListCallback
 * @param {Error}    [error] erreur éventuelle
 * @param {Groupe[]} groupes tableau d'entity groupe
 */

/**
 * @callback personneCallback
 * @param {Error}    [error]    erreur éventuelle
 * @param {Personne} [personne] Entity ou Object
 */

/**
 * @callback entityPersonneCallback
 * @param {Error}          [error]    Erreur éventuelle
 * @param {EntityPersonne} [personne] Une Entity Personne
 */

/**
 * @callback ressourceCallback
 * @param {Error}     [error]
 * @param {Ressource} [ressource]
 */

/**
 * @callback entityRessourceCallback
 * @param {Error}           [error]
 * @param {EntityRessource} [ressource]
 */

/**
 * Callback appellée sans argument
 * @callback simpleCallback
 */

// from sesatheque-plugin-arbre:editArbre.js
/**
 * Un enfant d'un arbre
 * @typedef Enfant
 * @property {string} titre
 * @property {string} type
 * @property {boolean} public
 * @property {string} [aliasOf] Absent seulement pour les "dossiers" (ressources de type arbre sans aliasOf mais avec enfants)
 * @property {string} [resume]
 * @property {string} [commentaires]
 * @property {Enfant[]} [enfants]
 */
/**
 * Une des relations d'une ressource
 * @typedef Relation
 * @property {number} 0 Id du type de liaison (cf app/ressource/config:listes.relations)
 * @property {string} 1 rid de la ressource liée
 */

/**
 * Objet link à passer à la vue link.dust
 * @typedef Link
 * @type {Object}
 * @property {string} href    L'url du lien
 * @property {string} value   Le texte à mettre dans le lien
 * @property {string} [icon]  Le nom de l'icone
 * @property {NameValue[]} [attributes] Attributs supplémentaires à mettre dans le tag a
 */

/**
 * Paire name / value
 * @typedef NameValue
 * @type {Object}
 * @property {string} name
 * @property {string} value
 */

/**
 * Une liste de critères de recherche.
 *
 * Chaque propriété est l'index sur lequel faire la recherche,
 * Sa valeur doit toujours être un tableau. S'il est vide on match simplement l'index
 * (non null dans mongo), sinon les valeurs demandées.
 * ex {type: 'j3p', groupes: ['foo', 'bar']} var chercher les ressources de type j3p publiées dans les groupes foo OU bar
 * Si une valeur de type string contient % on fera du like dessus (à éviter car gourmand, préférer fulltext)
 *
 * La clé fulltext est particulière, ça lance un textSearch lassi, en concaténant toutes les valeurs
 * (donc ['foo', 'bar'] revient au même que ['foo bar'], il faut passer ['"foo bar"'] pour une recherche exacte sur plusieurs mots)
 * @typedef {Object} SearchQuery
 */

/**
 * Options de recherche (skip, limit & orderBy)
 * @typedef {Object} SearchQueryOptions
 * @param {number} [skip=0] Offset
 * @param {number} [limit=25] Le nombre max de ressources à remonter
 * @param {OrderByParam[]} [orderBy] La liste éventuelle des clés de tri
 */
/**
 * Indication de tri (on accepte une string pour un tri ascendant)
 * @typedef OrderByParam
 * @type {Array|string}
 * @property {string} 0 la clé sur laquelle trier
 * @property {string} [1=asc] passer 'desc' pour un tri inversé
 */

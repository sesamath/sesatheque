'use strict'

// inspiré du sesalab:_private.exemple/config.docker.minimal.js

module.exports = {
  application: {
    /* admin destinataire des notification d'anomalie de l'appli */
    mail: 'dockerSesalab@example.com',
    /* from de tous les mails envoyés */
    fromMail: 'dockerSesalab@example.com',
    /* destinataire des signalements utilisateur */
    contactMail: 'dockerSesalab@example.com',
    baseUrl: 'http://sesalab.local:3000/',
    sesatheques: [
      { baseId: 'biblilocal3001', baseUrl: 'http://bibliotheque.local:3001/' },
      { baseId: 'communlocal3002', baseUrl: 'http://commun.local:3002/' }
    ]
  },

  $cache: {
    redis: {
      host: 'redis',
      port: 6379,
      prefix: 'sesalab'
    }
  },

  $entities: {
    database: {
      host: 'mongo',
      port: 27017,
      name: 'sesalab'
    }
  },

  $crypto: {
    salt: '12s2#OXei8H34'
  },

  $rail: {
    cookie: {
      key: 'nbé32!ps2#OXei8Htd'
    },
    session: {
      secret: '2#OXei8Htdnbé32!p'
    }
  },

  $server: {
    host: 'sesalab.local',
    port: 3000
  },

  // le smtp docker
  smtp: {
    host: 'mailhog',
    port: 1025
  }
}
